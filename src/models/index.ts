import { IGDSFilter } from "@vaultspace/gdatastore-v1";
import { ICustomer, IContact, IQuotePolicy, IApplication, IApplicationFunnel, IEmployee } from "@vaultspace/neon-shared-models";

export enum EBinType {
    Day = "Day",
    Week = "Week",
    Month = "Month"
}


export interface IEmailContent {
    subject: string;
    body: string;
}


export interface IEntityStats {
    kind: string;
    count: number;
    histograms: IHistogramCollection;
}

export interface IDateBin {
    date: Date;
    count: number;
}

export interface IHistogramCollection {
    [binType: string]: {
        [dateFieldName: string]: IDateBin[]
    };
}

export interface IStatsJobType {
    kind: string;
    datePinField: string;
    wantedBins: EBinType[];
    dateFieldNames: string[];
    queries: {[friendlyName: string]: IGDSFilter[][]};
}

export interface IStatsJobType2 {
    kind: string;
    datePinField: string;
    wantedBins: EBinType[];
    dateFieldNames: string[];
    queries: IKindQuery;
}

export interface IKindQuery {
    kind: string;
    compoundQueryFilters: {[queryName: string]: IGDSFilter[][]};
    permissionFilter: IGDSFilter;
    // permission QuotePolicy.systemApplication == appName , Customer.applications.includes(appName)
}


export type availableAppName = "applicationName" | "imaliaFintech";

export const availableAppNameObj = Object.freeze({
    applicationName: "applicationName" as availableAppName,
    imaliaFintech: "imaliaFintech" as availableAppName,
});


export type availableEmailType = "paymentCancellationEmail" // stripe -> payments
                                | "failedPaymentEmailWithLittleStripeRetries" // stripe -> dsPayment.service
                                | "failedPaymentEmailWithManyStripeRetries" // stripe -> dsPayment.service
                                | "inviteEmail" // neon-api -> company
                                | "refundEmail"
                                | "refundEmail2"
                                | "failedRefundEmail"
                                | "firstInvoiceEmail" // + pdfs
                                | "invoiceEmail" // + stripe pdf url
                                | "finalInvoiceEmail" // + stripe pdf url
                                | "paymentDisputeEmail"
                                | "referralEmail"
                                | "referralEmail2"
                                | "paymentDisputeNotification"
                                | "paymentFailureNotification"
                                | "refundFailureNotification"
                                | "referralNotification"
                                | "paymentDiscrepancyNotification";

export const availableEmailTypeObj = Object.freeze({
    paymentCancellationEmail: "paymentCancellationEmail" as availableEmailType,
    failedPaymentEmailWithLittleStripeRetries: "failedPaymentEmailWithLittleStripeRetries" as availableEmailType,
    failedPaymentEmailWithManyStripeRetries: "failedPaymentEmailWithManyStripeRetries" as availableEmailType,
    inviteEmail: "inviteEmail" as availableEmailType,
    refundEmail: "refundEmail" as availableEmailType,
    refundEmail2: "refundEmail2" as availableEmailType,
    failedRefundEmail: "failedRefundEmail" as availableEmailType,
    firstInvoiceEmail: "firstInvoiceEmail" as availableEmailType,
    invoiceEmail: "invoiceEmail" as availableEmailType,
    finalInvoiceEmail: "finalInvoiceEmail" as availableEmailType,
    paymentDisputeEmail: "paymentDisputeEmail" as availableEmailType,
    referralEmail: "referralEmail" as availableEmailType,
    referralEmail2: "referralEmail2" as availableEmailType,
    paymentDisputeNotification: "paymentDisputeNotification" as availableEmailType,
    oaymentFailureNotification: "paymentFailureNotification" as availableEmailType,
    refundFailureNotification: "refundFailureNotification" as availableEmailType,
    referralNotification: "referralNotification" as availableEmailType,
    paymentDiscrepancyNotification: "paymentDiscrepancyNotification" as availableEmailType
});


export type availableNotificationType = "personal"
                                        |"broadcasting";



export interface IEmailContentBuilderArguments {
    emailType: availableEmailType;
    notificationType: availableNotificationType;
    Customer?: ICustomer;
    Employee?: IEmployee;
    Contact?: IContact;
    QuotePolicy: IQuotePolicy;
    Application: IApplication;
    ApplicationFunnel: IApplicationFunnel;
    options: Readonly<{[key: string]: any}>;
}

export interface IEmailContentOptionsBody {
    [key: string]: {
        [key: string]: any;
    };
}

export interface IEmailContentOptionsSubject {
    [key: string]: any;
}

export interface IEmailContentOptions {
    body: IEmailContentOptionsBody;
    subject: IEmailContentOptionsSubject;
}
