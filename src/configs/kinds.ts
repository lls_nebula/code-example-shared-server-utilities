export const applicationKind = "Application";
// export const applicationKind = process.env.NODE_ENV === "test" ? "ApplicationTest" : "Application";
export const applicationEmailContentKind: string = "ApplicationEmailContent";
export const quotePolicyKind = "QuotePolicy";