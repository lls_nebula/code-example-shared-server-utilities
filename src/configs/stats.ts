import { EApprovedStatus, IResponse } from "@vaultspace/neon-shared-models";
import { EBinType, IEntityStats, IStatsJobType, IStatsJobType2, IKindQuery } from "../models";
import { IGDSFilter } from "@vaultspace/gdatastore-v1";
import { addDaysToDate, addYearsToDate } from "../util/time-and-date";

function quotePolicyQueries(_now: number = null): { [queryName: string]: IGDSFilter[][] } {
    const now = _now == null ? new Date() : new Date(_now);
    return {
        "All": [[]],
        "ReferredQuotes":
            [ // OR FILTERS
                [ // AND FILTERS
                    { propertyName: "approvedStatus", comparator: "=", value: EApprovedStatus.inReferral }
                ]
            ],
        "AllBound": [
            [{ propertyName: "policy", comparator: "=", value: true }]
        ],
        "ActivePolicies": [ // OR FILTERS
            [ // AND FILTERS
                { propertyName: "policy", comparator: "=", value: true },
                { propertyName: "startDate", comparator: ">", value: <any>addYearsToDate(now, -1).getTime() },
                { propertyName: "startDate", comparator: "<", value: <any>now.getTime() },
                { propertyName: "cancelled", comparator: "=", value: false },
                { propertyName: "numberOfMonthUnitsForTerm", comparator: "=", value: 12 }
            ],
            /*[ // AND FILTERS
                { propertyName: "policy", comparator: "=", value: true },
                { propertyName: "startDate", comparator: ">", value: <any>addYearsToDate(now, -2).getTime() },
                { propertyName: "startDate", comparator: "<", value: <any>now.getTime() },
                { propertyName: "cancelled", comparator: "=", value: false },
                { propertyName: "numberOfMonthUnitsForTerm", comparator: "=", value: 24 }
            ]*/
        ],
        "RefundedAndCancelled": [ // use this for canceled
            [
                { propertyName: "policy", comparator: "=", value: true },
                { propertyName: "cancelled", comparator: "=", value: true }
            ]
        ],
        "ExpiredPolicies": [
            [
                { propertyName: "policy", comparator: "=", value: true },
                { propertyName: "endDate", comparator: "<", value: <any>now.getTime() },
                { propertyName: "cancelled", comparator: "=", value: false }
            ]
        ],
        "PaymentFinishedPolicies": [
            [
                { propertyName: "policy", comparator: "=", value: true },
                { propertyName: "paymentFinished", comparator: "=", value: true },
            ]
        ],
        "ActiveQuotes": [ // PROVISIONAL IN FRONT END
            // QUOTE - any current quote (i.e. less than 30 days since issuance).
            // This would include both quotes directly from the front end,
            //   but also quotes which are awaiting acceptance from the client that have previously been referred.
            [
                { propertyName: "approvedStatus", comparator: "=", value: EApprovedStatus.manuallyApproved },
                { propertyName: "policy", comparator: "=", value: false },
                { propertyName: "quoteValidUntil", comparator: ">", value: <any>now.getTime() },
            ],
            [
                { propertyName: "approvedStatus", comparator: "=", value: EApprovedStatus.autoApproved },
                { propertyName: "policy", comparator: "=", value: false },
                { propertyName: "quoteValidUntil", comparator: ">", value: <any>now.getTime() },
            ],
            [ // AND FILTERS
                { propertyName: "approvedStatus", comparator: "=", value: EApprovedStatus.provisional },
                { propertyName: "policy", comparator: "=", value: false },
                { propertyName: "quoteValidUntil", comparator: ">", value: <any>now.getTime() },
            ],


        ],
        "ExpiredQuotes": [
            [
                { propertyName: "policy", comparator: "=", value: false },
                { propertyName: "quoteValidUntil", comparator: "<", value: <any>now.getTime() },
                { propertyName: "approvedStatus", comparator: "=", value: EApprovedStatus.provisional },
            ]
        ],
        "NTUQuotes": [ // OR FILTERS
            [ // AND FILTERS
                { propertyName: "approvedStatus", comparator: "=", value: EApprovedStatus.ntu },
            ]
        ],
        "ProvisionalQuotes": [
            [
                { propertyName: "approvedStatus", comparator: "=", value: EApprovedStatus.provisional }
            ]
        ],
        "CancelledPolicies": [
            [
                { propertyName: "policy", comparator: "=", value: true },
                { propertyName: "cancelled", comparator: "=", value: true },
                { propertyName: "refunded", comparator: "=", value: false }
            ]
        ],
        "RefundedPolicies": [
            [
                { propertyName: "policy", comparator: "=", value: true },
                { propertyName: "refunded", comparator: "=", value: true }
            ]
        ],
        "PoliciesWithClaims": [
            [
                { propertyName: "policy", comparator: "=", value: true },
                { propertyName: "claim", comparator: "=", value: true }
            ]
        ],
        "RenewalLess7": [ // OR FILTERS
            [ // AND FILTERS
                { propertyName: "endDate", comparator: "<", value: <any>addDaysToDate(now, 7).getTime() },
                { propertyName: "policy", comparator: "=", value: true },
                { propertyName: "cancelled", comparator: "=", value: false }
            ]
        ],
        "RenewalLess15": [ // OR FILTERS
            [ // AND FILTERS
                { propertyName: "endDate", comparator: "<", value: <any>addDaysToDate(now, 15).getTime() },
                { propertyName: "policy", comparator: "=", value: true },
                { propertyName: "cancelled", comparator: "=", value: false }
            ]
        ],
        "RenewalLess30": [ // OR FILTERS
            [ // AND FILTERS
                { propertyName: "endDate", comparator: "<", value: <any>addDaysToDate(now, 30).getTime() },
                { propertyName: "policy", comparator: "=", value: true },
                { propertyName: "cancelled", comparator: "=", value: false }
            ]
        ],
        "RenewalLess45": [ // OR FILTERS
            [ // AND FILTERS
                { propertyName: "endDate", comparator: "<", value: <any>addDaysToDate(now, 45).getTime() },
                { propertyName: "policy", comparator: "=", value: true },
                { propertyName: "cancelled", comparator: "=", value: false }
            ]
        ],
        "RenewalLess60": [ // OR FILTERS
            [ // AND FILTERS
                { propertyName: "endDate", comparator: "<", value: <any>addDaysToDate(now, 60).getTime() },
                { propertyName: "policy", comparator: "=", value: true },
                { propertyName: "cancelled", comparator: "=", value: false }
            ]
        ],
        "RenewalLess90": [ // OR FILTERS
            [ // AND FILTERS
                { propertyName: "endDate", comparator: "<", value: <any>addDaysToDate(now, 90).getTime() },
                { propertyName: "policy", comparator: "=", value: true },
                { propertyName: "cancelled", comparator: "=", value: false }
            ]
        ]
    };
}

function customerQueries(_now: number = null): { [queryName: string]: IGDSFilter[][] } {
    return {
        "All": [[]]
    };
}


export function fetchKindQuery(kind: string, now: number = null): IKindQuery {
    if (kind == "QuotePolicy") {
        return {
            kind: "QuotePolicy",
            permissionFilter: { propertyName: "systemApplication", comparator: "=", value: undefined },
            compoundQueryFilters: quotePolicyQueries(now)
        };
    }
    else if (kind == "Customer") {
        return {
            kind: "Customer",
            permissionFilter: { propertyName: "applications", comparator: "includes", value: undefined },
            compoundQueryFilters: customerQueries(now)
        };
    }
    else {
        throw {
            error: true,
            message: `Kind ${kind} has not any named queries`,
            code: 82172623212
        };
    }
}

export function fetchQueries2(now: number = null): IStatsJobType2[] {
    const jobs: IStatsJobType2[] = [
        {
            kind: "QuotePolicy",
            datePinField: "quoteCreatedDate",
            wantedBins: [EBinType.Day, EBinType.Month, EBinType.Week],
            dateFieldNames: ["quoteCreatedDate", "approvedDate", "startDate", "endDate"],
            queries: <IKindQuery>fetchKindQuery("QuotePolicy", now)
        },
        {
            kind: "Customer",
            datePinField: "createdAt",
            wantedBins: [EBinType.Day, EBinType.Month, EBinType.Week],
            dateFieldNames: ["createdAt"],
            queries: <IKindQuery>fetchKindQuery("Customer", now)
        },
    ];
    return jobs;
}


export function fetchQueries(now: number = null) {
    return quotePolicyQueries(now);
}


export function fetchJobs() {
    const queries: any = fetchQueries();
    const jobs: IStatsJobType[] = [
        {
            kind: "QuotePolicy",
            datePinField: "quoteCreatedDate",
            wantedBins: [EBinType.Day, EBinType.Month, EBinType.Week],
            dateFieldNames: ["quoteCreatedDate", "approvedDate", "startDate", "endDate"],
            queries: queries
        },
        /*{       !item.policy && item.quoteValidUntil > Date.now() && (item.approvedStatus === EApprovedStatus.provisional ||
        item.approvedStatus === EApprovedStatus.autoApproved || item.approvedStatus === EApprovedStatus.manuallyApproved)

            kind: "Customer",
            datePinField: "createdAt",
            wantedBins: [EBinType.Day, EBinType.Month, EBinType.Week],
            dateFieldNames: ["createdAt"],
            queries: {
                "All": [[]]
            }
        }*/
    ];
    return jobs;
}