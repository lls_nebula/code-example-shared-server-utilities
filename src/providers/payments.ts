import { IPaymentInstallmentPlan, paymentPlanInstallmentsKind } from "@vaultspace/neon-shared-models";
import { gcdsInstance, IGDSFilter } from "@vaultspace/gdatastore-v1";
const quotePolicyKind: string = "QuotePolicy";
export async function getPaymentPlanInstallmentsForCustomer(customerId: string, quoteId: string): Promise<IPaymentInstallmentPlan[]> {
    const filters: IGDSFilter[] = [
        { propertyName: "customerId", comparator: "=", value: customerId },
        { propertyName: "quoteId", comparator: "=", value: quoteId}
    ];

    return gcdsInstance.queryMany(paymentPlanInstallmentsKind, filters, quotePolicyKind, quoteId);
}