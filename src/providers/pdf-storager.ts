import { createFileWithPath, getFilesListFromDirectory, fetchFilesFromDirectory } from "./storage";
import { gcdsInstance } from "../gcds-instance";
import { IGDSKey } from "@vaultspace/gdatastore-v1";
import { IQuotePolicy } from "@vaultspace/neon-shared-models";
import { availableAppName } from "../models";
import * as pdf from "@vaultspace/pdf";
import { quotePolicyKind } from "../configs/kinds";
import {
    ICreateFintechScheduleArguments,
    ICreateFdcScheduleArguments,
    ICreateCoCArguments,
        } from "@vaultspace/pdf/dist/src/models";
import {
    folderName,
    staticFolderName,
    personalFolderName,
        } from "../configs/pdf_storager_config";
import { IBufferAttachment } from "@vaultspace/emailer";
import { fetchQuote } from "./quote-policy";
import { link } from "fs";
import { firestore } from "@vaultspace/firebase-server";

export function generateCommonDocuments(): Promise<Array<string>> {

    return Promise.all([
        pdf.createFdcEducators().then(buf => storeCommonDocument("imaliaDaycare", "fdcEducatorsProgram", buf)),
        pdf.createFdcPersonalAccidentHtml().then(buf => storeCommonDocument("imaliaDaycare", "fdcPersonalAccident", buf)),
        pdf.createFdcCombinedLiabilityHtml().then(buf => storeCommonDocument("imaliaDaycare", "fdcCombinedLiability", buf)),
        pdf.createFintechPolicy().then(buf => storeCommonDocument("imaliaFintech", "fintechPolicy", buf)),
    ]);
}

export async function storeCommonDocument(appName: availableAppName, fileName: string, data: Buffer): Promise<string> {
    try {
        const path = `/${folderName}/${appName}/${staticFolderName}/${fileName}.pdf`;

        await createFileWithPath(path, data);
        return path;

    } catch (err) { throw err; }
}

export async function attachDocuments(
    quoteId: string,
    options: {
        schedule: ICreateFintechScheduleArguments | ICreateFdcScheduleArguments,
        coc?: ICreateCoCArguments
    }
): Promise<IBufferAttachment[]> {
    try {
        const key: IGDSKey = { kind: quotePolicyKind, name: quoteId };
        const quote: IQuotePolicy = await fetchQuote(quoteId);
        const links = await provideDocuments(quote.systemApplication as availableAppName, quoteId, quote.customerId, options);

        // fuck this line! no await!
        // gcdsInstance.updateOne(key, quote);
        await firestore.collection("QuotePolicy").doc(quoteId).set({storedDocuments: links}, {merge: true});

        const attachments = await Promise.all([
            fetchFilesFromDirectory(`${folderName}/${quote.systemApplication}/${staticFolderName}`),
            fetchFilesFromDirectory(`${folderName}/${quote.systemApplication}/${personalFolderName}/customers/${quote.customerId}/quotes/${quoteId}`),
        ]);

        return [...attachments[0], ...attachments[1]];

    } catch (err) {
        throw err;
    }
}

async function provideDocuments(appName: availableAppName, quoteId: string, customerId: string, options: {schedule: ICreateFintechScheduleArguments|ICreateFdcScheduleArguments, coc?: ICreateCoCArguments}): Promise<Array<string>> {
    try {
        switch (appName) {
            case ("imaliaDaycare"):
                if (!options.coc) {
                    throw new Error("there is no options for CoC");
                }

                if (!("policyNumber" in options.schedule)) {
                    throw new Error("incorrect options for fdc schedule");
                }

                const scheduleOptions1 = options.schedule;

                const result1 = await Promise.all([
                    getStaticDocPaths(appName),

                    pdf.createFdcSchedule(scheduleOptions1).then((buf) => {
                        return storePersonalDocument(quoteId, appName, customerId, "Imalia - FDC - Schedule - v1.0.0", buf);
                    }).then(link => link),

                    pdf.createCoC(options.coc).then((buf: any) => {
                        return storePersonalDocument(quoteId, appName, customerId, "Imalia - FDC - Certificate of Currency - v1.0.0", buf);
                    }).then((link: any) => link),

                    // pdf.createFdcPersonalAccidentHtml().then((buf) => {
                    //     return storePersonalDocument(quoteId, appName, customerId, "FdcPersonalAccidentPolicy", buf);
                    // }).then(link => link),

                    // pdf.createFdcCombinedLiabilityHtml().then((buf) => {
                    //     return storePersonalDocument(quoteId, appName, customerId, "FdcLiabilityPolicy", buf);
                    // }).then(link => link)
                ]);

                return [...result1[0], result1[1], result1[2]];


            case ("imaliaFintech"):
                if (!("schedule" in options.schedule)) {
                    throw new Error("incorrect options for fintech schedule");
                }
                const scheduleOptions2 = options.schedule;

                const result2 = await Promise.all([
                    getStaticDocPaths(appName),

                    pdf.createFintechSchedule(scheduleOptions2)
                        .then(buf => storePersonalDocument(quoteId, appName, customerId, "fdcSchedule", buf))
                        .then(link => link),
                ]);

                return [...result2[0], result2[1]];

            default:
                throw new Error("incorrect application");
        }
    } catch (err) { throw err; }
}





export function getStaticDocPaths(appName: availableAppName): Promise<Array<string>> {
    // I THINK THIS MIGHT RESOLVE WITHOUT A SLASH
    return getFilesListFromDirectory(
        `${folderName}/${appName}/${staticFolderName}`
    );
}



async function storePersonalDocument(quoteId: string, appName: availableAppName, customerId: string, fileName: string, data: Buffer): Promise<string> {
    try {
        const path = `/${folderName}/${appName}/${personalFolderName}/customers/${customerId}/quotes/${quoteId}/${fileName}.pdf`;
        await createFileWithPath(path, data);

        return path;

    } catch (err) { throw err; }
}



export const documentsBuilder = Object.freeze({
    attachForImaliaDaycare: (quoteId: string, options: {schedule: ICreateFdcScheduleArguments, coc: ICreateCoCArguments}) => attachDocuments(quoteId, options),
    attachForImaliaFintech: (quoteId: string, options: {schedule: ICreateFintechScheduleArguments}) => attachDocuments(quoteId, options),
});
