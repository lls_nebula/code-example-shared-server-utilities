
import { collectStatsForKind, saveKindQueryCounts, saveHistograms } from "./stats";
import { IStatsJobType } from "../models";
import { fetchJobs } from "../configs/stats";

/*
"QuotePolicy", "quoteCreatedDate", [EBinType.Day, EBinType.Month, EBinType.Week], ["quoteCreatedDate"]
*/



export async function collectAndSaveAllStatsAuto(): Promise<any> {
    const jobs = fetchJobs();
    return await Promise.all(jobs.map((job: IStatsJobType) => {
        return collectStatsForKind(job.kind, job.datePinField, job.wantedBins, job.dateFieldNames, job.queries).then(async (stats: any) => {
           await saveKindQueryCounts(stats);
           await saveHistograms(stats);
        });
    }));
}
