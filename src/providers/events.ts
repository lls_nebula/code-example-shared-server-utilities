import { firestore, firebaseFunctions, firebaseClient, firebaseAdmin } from "@vaultspace/firebase-server";
import { gcdsInstance, ICompoundResult, IGDSFilter } from "@vaultspace/gdatastore-v1";
import { IPaymentInstallmentPlan, IApplicationProduct, EPaymentStatus, IQuotePolicy, ETransactionType, ETransactionType2, IApplication, EProductType, ICustomer, IContact } from "@vaultspace/neon-shared-models";
import { v4 as uuid } from "uuid";
import { getPaymentPlanInstallmentsForCustomer } from "./payments";
import { updateAllStatsForKindEntity } from "./stats";
import * as vm from "vm";
import * as firebase from "firebase";
import { DeepDiff } from "../util/diff";
const diffMapper = new DeepDiff();

async function safeDeltaMapper(_new: any, _old: any, path: string, timestamp: number) {
    return;
    try {
        const old = _old || {};
        const new2 = _new || {};
        const diff = diffMapper.diff(new2, old);
        // create delta
        const fullPath = path + `/Delta/${timestamp}`;
        const pathSplit = fullPath.split("/");
        await gcdsInstance.createOne(pathSplit, {
            timestamp: timestamp,
            diff: JSON.parse(JSON.stringify(diff))
        });
    }
    catch (e) {
        console.error("ERROR IN DELTA CALCULATOR", e);
    }
}
export enum EEventType {
    Written = "Written",
    Paid = "Paid",
    Interaction = "Interaction"
}


/*
IEventComputedFieldsStream
IEventContextStream
*/

export const IEventComputedFieldsStreamKind = "EventStreamComputedFields";
export interface IEventComputedFieldsStream {
    _name?: string;
    eventType: EEventType;
    applicationName: string;
    timestamp: Date | number;
    primaryEntityKind: string;
    primaryEntityName: string;
    eventContext: any;
    [computedFieldName: string]: any;
}

export const IEventContextStreamKind = "EventContextStream";
export interface IEventContextStream {
    _name?: string;
    eventType: EEventType;
    applicationName: string;
    timestamp: Date | number;
    primaryEntityKind: string;
    primaryEntityName: string;
    eventContext: any;
    data: { [contextEntityName: string]: any; };
    [wildCard: string]: any;
}
/*

const amountRecieved = data.QuotePolicySnapshot.LatestPaymentPlanInstallment.amount_recieved;
const amountDue = data.QuotePolicySnapshot.LatestPaymentPlanInstallment.amount_due;


const productTotalTax = Object.keys(charge.personalAccident.tax).reduce((acc, taxName) => {
acc += parseFloat(charge.personalAccident.tax[taxName])
return acc;
}, 0);

const productTotalParty = Object.keys(charge.personalAccident.party).reduce((acc, partyName) => {
acc += parseFloat(charge.personalAccident.party[partyName])
return acc;
}, 0);


const productionFraction = (productTotalTax + productTotalParty) / amountDue


const amountPaidForAGivenProduct = productionFraction * amountRecieved;


PaidThisTime_DXEvolution_BrokerFee
PaidThisTime_GST_GL



what i have done is PaidThisTime_BrokerFee
what fraction of this is a tax for instance
*/
export enum EPaymentPlanProductSubType {
    Party = "party",
    Tax = "tax"
}
function paidThisTimeForAGivenProductPartyOrTax(latestPaymentPlanInstallment: IPaymentInstallmentPlan, paymentPlan: IPaymentPlanOutput, taxOrParty: EPaymentPlanProductSubType, taxOrPartyName: string, productName: string) {
    if (!latestPaymentPlanInstallment) return 0;
    if (!paymentPlan) return 0;
    if (!taxOrParty) return 0;
    if (!taxOrPartyName) return 0;
    if (!productName) return 0;
    if (!paymentPlan.charge[productName]) return 0;
    if (!paymentPlan.charge[productName][taxOrParty]) return 0;
    if (!paymentPlan.charge[productName][taxOrParty][taxOrPartyName]) return 0;
    const amountDue = parseFloat(latestPaymentPlanInstallment.amountDue);
    const amountReceived = parseFloat(latestPaymentPlanInstallment.amountReceived);
    // get product/party sum
    const productTotalTax = Object.keys(paymentPlan.charge[productName].tax).reduce((acc, taxName) => {
        acc += parseFloat(paymentPlan.charge[productName].tax[taxName]);
        return acc;
    }, 0);

    const productTotalParty = Object.keys(paymentPlan.charge[productName].party).reduce((acc, partyName) => {
        acc += parseFloat(paymentPlan.charge[productName].party[partyName]);
        return acc;
    }, 0);

    const totalTaxPartySumForAProduct = productTotalTax + productTotalParty;
    const productionFraction = (totalTaxPartySumForAProduct) / amountDue;

    // what is the fraction of a given tax/party name for a given product
    const productTaxOrPartyAmount = parseFloat(paymentPlan.charge[productName][taxOrParty][taxOrPartyName]);
    const partyOrTaxFractionForProductFraction = productTaxOrPartyAmount / totalTaxPartySumForAProduct;


    const amountPaidForAGivenProduct = partyOrTaxFractionForProductFraction * productionFraction * amountReceived;
    if (latestPaymentPlanInstallment.installmentIndex === 0 && (paymentPlan.preTaxGWP.total != paymentPlan.preTaxGWPWritten.total)) {
        return amountPaidForAGivenProduct * 2;
    }
    return amountPaidForAGivenProduct;
}

export function contextComputedField_computer(code: string, context: any): any {
    const contextClone = JSON.parse(JSON.stringify(context));
    contextClone._ = {
        amountPaidDueFraction: function (paymentPlanInstallmentsSnapshot: Array<IPaymentInstallmentPlan>) {
            if (!paymentPlanInstallmentsSnapshot) return 0;
            const paidToDate = paymentPlanInstallmentsSnapshot.reduce((acc: number, installation: IPaymentInstallmentPlan) => {
                acc += installation.amountReceived ? parseFloat(installation.amountReceived) : 0;
                return acc;
            }, 0);
            const due = paymentPlanInstallmentsSnapshot.reduce((acc: number, installation: IPaymentInstallmentPlan) => {
                acc += installation.amountDue ? parseFloat(installation.amountDue) : 0;
                return acc;
            }, 0);
            return paidToDate / due;
        },
        installmentsAmountPaidToDate: function (paymentPlanInstallmentsSnapshot: Array<IPaymentInstallmentPlan>) {
            return paymentPlanInstallmentsSnapshot.reduce((acc: number, installation: IPaymentInstallmentPlan) => {
                acc += installation.amountReceived ? parseFloat(installation.amountReceived) : 0;
                return acc;
            }, 0);
        },
        installmentsAmountTotal: function (paymentPlanInstallmentsSnapshot: Array<IPaymentInstallmentPlan>) {
            return paymentPlanInstallmentsSnapshot.reduce((acc: number, installation: IPaymentInstallmentPlan) => {
                acc += installation.amountDue ? parseFloat(installation.amountDue) : 0;
                return acc;
            }, 0);
        },
        paidThisTimeForAGivenProductPartyOrTax: paidThisTimeForAGivenProductPartyOrTax
    };
    // functional extension
    let ret: any = null;
    try {
        ret = vm.runInNewContext(code, contextClone);
    }
    catch (e) {
        ret = `ERROR: ${JSON.stringify(e.stack).slice(0, 100)}...`;
    }
    if (ret === undefined) return `UNDEFINED RESULT`;
    return ret;
}

// eventData: IEventContextStream
export const createEventStreamEntity = async (appName: string, eventType: EEventType, timestamp: number, context: any, otherTopLevel: any, primaryEntityKind: string, primaryEntityName: string, eventContext: any) => {
    const eventContextData: IEventContextStream = {
        timestamp: timestamp,
        data: context,
        eventType: eventType,
        applicationName: appName,
        primaryEntityKind: primaryEntityKind,
        primaryEntityName: primaryEntityName,
        eventContext: eventContext
    };



    console.error("IN createEventStreamEntity", eventContextData);
    // retrieve product information
    const applicationInfo: IApplication = await gcdsInstance.getOne(["Application", appName]);
    console.error("applicationInfo", applicationInfo);

    // prepare to collect computedFields
    const computedFields: any = {};

    // get field computer
    const fieldComputerInfo = applicationInfo.streamVariables;
    Object.keys(fieldComputerInfo).forEach((fieldNameToCompute: string) => {
        const code = fieldComputerInfo[fieldNameToCompute];
        computedFields[fieldNameToCompute] = contextComputedField_computer(code, eventContextData);
    });

    console.error("computedFields", computedFields);

    const eventComputedFieldsData: IEventComputedFieldsStream = {
        eventType: eventType,
        applicationName: appName,
        timestamp: timestamp,
        primaryEntityKind: primaryEntityKind,
        primaryEntityName: primaryEntityName,
        eventContext: eventContext
    };

    const computedFieldsKeys = Object.keys(computedFields);
    const protectedKeys = ["eventType", "applicationName", "timestamp", "_name", "primaryEntityKind", "primaryEntityName", "eventContext"];
    const safeComputedFieldKeys = computedFieldsKeys.filter(it => !protectedKeys.includes(it));
    safeComputedFieldKeys.forEach((computedFieldName: string) => {
        eventComputedFieldsData[computedFieldName] = computedFields[computedFieldName];
    });

    if (typeof otherTopLevel === "object") {
        Object.keys(otherTopLevel).filter(it => !protectedKeys.concat(computedFieldsKeys).includes(it))
            .forEach((topLevelPropertyName: string) => {
                eventComputedFieldsData[topLevelPropertyName] = otherTopLevel[topLevelPropertyName];
            });
    }

    // attach primary info

    //         computedFields: computedFields


    console.error("eventComputedFieldsData", eventComputedFieldsData);
    console.error("saving.....");

    const randomUUID: string = uuid();
    return Promise.all(
        [
            gcdsInstance.createOne([IEventContextStreamKind, randomUUID], rebuild(eventContextData)),
            gcdsInstance.createOne([IEventComputedFieldsStreamKind, randomUUID], rebuild(eventComputedFieldsData))
        ]
    );
};


/*
        const relevantReportingProducts = Object.keys(application.products).filter((productKey) => {
            const productType: EProductType = application.products[productKey];
            if (productType == EProductType.insurance) return true;
        });

*/

export async function fetchApplicationProductsInformation(appName: string): Promise<IApplicationProduct[]> {
    return await gcdsInstance.queryMany("Product", [], "Application", appName);
}

export async function computeRefresh(appName: string) {
    console.error("appName", appName);
    const newData: IApplication = await gcdsInstance.getOne(["Application", appName]);
    console.error("IN UPDATE APPILCATION COMPUTER", newData);

    // see if compute fields have changed
    const reComputeFields: { [fieldName: string]: string } = {};
    const fieldsToRemove: Array<string> = [];
    Object.keys(newData.streamVariables).forEach((fieldComputeName: string) => {
        const newValue = newData.streamVariables[fieldComputeName];
        reComputeFields[fieldComputeName] = newValue;
        console.log("fieldComputeName", fieldComputeName);
    });

    console.error("FETCHING RELEVANT CONTEXT STREAMS");

    // query all items in context stream
    const filters: IGDSFilter[] = [
        { propertyName: "applicationName", comparator: "=", value: appName },
    ];
    const relevantContextStreamsBatch = await gcdsInstance.queryMany(IEventContextStreamKind, filters); // productLoopBatchSize
    let counts = 0;

    const contextStreams: IEventContextStream[] = relevantContextStreamsBatch;
    await Promise.all(contextStreams.map(async (contextStream: IEventContextStream) => {
        counts += 1;
        const reComputedValues: any = {};
        const id = contextStream._name;
        console.log("resolving", id);

        Object.keys(reComputeFields).forEach((fieldName: string) => {
            const code = reComputeFields[fieldName];
            reComputedValues[fieldName] = contextComputedField_computer(code, contextStream);
            if (id === "feabe0d2-b32e-4e24-a855-4bca0f3cf8ee") {
                console.log("000000000000000000000000");
                console.log(fieldName, reComputedValues[fieldName]);
            }
        });
        // console.log("[IEventComputedFieldsStreamKind, id]", [IEventComputedFieldsStreamKind, id]);
        // fetch old computedStream
        const computedStream: IEventComputedFieldsStream = await gcdsInstance.getOne([IEventComputedFieldsStreamKind, id]);
        console.log("computedStream", computedStream);
        if (!computedStream) {
            console.error("COULD NOT FIND COMPUTED STREAM WITH ID", id);
            return;
        }
        // console.log("computedStream", computedStream);
        fieldsToRemove.forEach((fieldToRemove: string) => {
            delete computedStream[fieldToRemove];
        });
        // console.log(reComputedValues);
        Object.keys(reComputedValues).forEach((fieldName: string) => {
            computedStream[fieldName] = reComputedValues[fieldName]; // computedFields
        });

        const computeKeys = Object.keys(reComputedValues);

        Object.keys(computedStream).forEach((fieldName: string) => {
            if (!computeKeys.includes(fieldName)) {
                delete computedStream[fieldName];
            }
        });

        // computedStream
        computedStream.primaryEntityKind = contextStream.primaryEntityKind;
        computedStream.primaryEntityName = contextStream.primaryEntityName;
        computedStream.eventContext = contextStream.eventContext;
        if (contextStream.hasOwnProperty("transactionType2")) {
            computedStream.transactionType2 = contextStream.transactionType2;
        }
        if (contextStream.hasOwnProperty("transactionType")) {
            computedStream.transactionType = contextStream.transactionType;
        }


        console.log("FINAL", rebuild(computedStream));

        // console.log("gah computedStream", computedStream);
        await gcdsInstance.updateOne({ kind: IEventComputedFieldsStreamKind, name: id }, rebuild(computedStream));
    }));

    console.error("GOT TO COMPUTE WITH CONTEXT STREAMS - ALL DONE", counts);
}

/// TRIGGERS

/*
events

paid (installment of X)
last payment made in period (installation)
REAL refund of X

written (sold for total price)
payment arranged (policy = true)
policy cancelled (not refund)
MTA's (additional price)

*/
const productLoopBatchSize: number = 100;
export async function applicationUpdatedInner(appName: string, newData: IApplication, oldData: IApplication) {
    console.error("appName", appName);
    console.error("IN UPDATE APPILCATION COMPUTER", newData, oldData);
    // see if compute fields have changed
    const reComputeFields: { [fieldName: string]: string } = {};
    const fieldsToRemove: Array<string> = [];
    Object.keys(newData.streamVariables).forEach((fieldComputeName: string) => {
        const newValue = newData.streamVariables[fieldComputeName];
        const oldValue = oldData.streamVariables[fieldComputeName];
        if (newValue === undefined) {
            fieldsToRemove.push(fieldComputeName);
        }
        else if (newValue != oldValue) {
            const code: string = newValue;
            reComputeFields[fieldComputeName] = code;
        }
    });

    console.error("FETCHING RELEVANT CONTEXT STREAMS");

    // query all items in context stream
    const filters: IGDSFilter[] = [
        { propertyName: "applicationName", comparator: "=", value: appName },
    ];

    let cursor = null;
    let allFinished = false;
    let count = 0;
    let results = [];
    while (!allFinished) {
        const result: ICompoundResult = await gcdsInstance.queryManyLimited(IEventContextStreamKind, filters, productLoopBatchSize, cursor);
        cursor = result.cursor;
        allFinished = result.allFinished;
        results = result.results;
        console.log("repeat result", results.length, result);

        console.log("loop");
        const contextStreams: IEventContextStream[] = results;
        await Promise.all(contextStreams.map(async (contextStream: IEventContextStream) => {
            count += 1;
            const reComputedValues: any = {};
            const id = contextStream._name;
            Object.keys(reComputeFields).forEach((fieldName: string) => {
                const code = reComputeFields[fieldName];
                reComputedValues[fieldName] = contextComputedField_computer(code, contextStream);
            });
            // console.log("[IEventComputedFieldsStreamKind, id]", [IEventComputedFieldsStreamKind, id]);
            // fetch old computedStream
            const computedStream: IEventComputedFieldsStream = await gcdsInstance.getOne([IEventComputedFieldsStreamKind, id]);
            if (!computedStream) {
                console.error("COULD NOT FIND COMPUTED STREAM WITH ID", id);
                return;
            }
            // console.log("computedStream", computedStream);
            fieldsToRemove.forEach((fieldToRemove: string) => {
                delete computedStream[fieldToRemove];
            });
            // console.log(reComputedValues);
            Object.keys(reComputedValues).forEach((fieldName: string) => {
                computedStream[fieldName] = reComputedValues[fieldName]; // computedFields
            });

            // top level!!
            computedStream.primaryEntityKind = contextStream.primaryEntityKind;
            computedStream.primaryEntityName = contextStream.primaryEntityName;
            computedStream.eventContext = contextStream.eventContext;
            if (contextStream.hasOwnProperty("transactionType2")) {
                computedStream.transactionType2 = contextStream.transactionType2;
            }
            if (contextStream.hasOwnProperty("transactionType")) {
                computedStream.transactionType = contextStream.transactionType;
            }

            console.log("FINAL", computedStream);

            // console.log("gah computedStream", computedStream);
            await gcdsInstance.updateOne({ kind: IEventComputedFieldsStreamKind, name: id }, rebuild(computedStream));
        }));


    }
    console.error("GOT TO COMPUTE WITH CONTEXT STREAMS - ALL DONE", "COUNT", count);
}

function rebuild(obj: any) {
    return JSON.parse(JSON.stringify(obj));
}
import * as functions from "firebase-functions";
import { type } from "os";
// product re-compute
export const applicationUpdated = functions.runWith({
    timeoutSeconds: 530,
    memory: "1GB"
}).region("us-central1")
    .firestore
    .document((`Application/{applicationName}`))
    .onUpdate(async (change: firebaseFunctions.Change<FirebaseFirestore.DocumentSnapshot>, context: firebaseFunctions.EventContext) => {
        const appName: string = context.params.applicationName;
        const newData: IApplication = <any>change.after.data();
        const oldData: IApplication = <any>change.before.data();
        const path = `Application/${appName}`;

        await applicationUpdatedInner(appName, newData, oldData);

        const timestamp: number = new Date(context.timestamp).getTime();
        await safeDeltaMapper(newData, oldData, path, timestamp);


    });
/*
TypeError: Cannot read property 'yearOfAccount' of undefined             const code = newValue.variables[fieldComputeName];
    at Object.keys.forEach (/srv/node_modules/@vaultspace/shared-server-utilities/dist/providers/events.js:131:44)
    at Array.forEach (<anonymous>)
    at Object.<anonymous> (/srv/node_modules/@vaultspace/shared-server-utilities/dist/providers/events.js:124:36)
    at Generator.next (<anonymous>)
    at /srv/node_modules/@vaultspace/shared-server-utilities/dist/providers/events.js:7:71
    at new Promise (<anonymous>)
    at __awaiter (/srv/node_modules/@vaultspace/shared-server-utilities/dist/providers/events.js:3:12)
    at exports.productUpdated.firebase_server_1.firebaseFunctions.firestore.document.onUpdate (/srv/node_modules/@vaultspace/shared-server-utilities/dist/providers/events.js:116:36)
    at cloudFunctionNewSignature (/srv/node_modules/firebase-functions/lib/cloud-functions.js:105:23)
    at /worker/worker.js:825:24
*/
export async function createPaidRecord(latestPaymentPlanInstallment: IPaymentInstallmentPlan, quotePolicy: IQuotePolicy) {
    const now = Date.now();
    const application: IApplication = await gcdsInstance.getOne({ kind: "Application", name: quotePolicy.systemApplication });
    const customer: ICustomer = await gcdsInstance.getOne({ kind: "Customer", name: quotePolicy.customerId });
    const contact: IContact = await gcdsInstance.getOne({ kind: "Contact", name: customer.primaryContactId });
    // we hav a new installment
    const installments: IPaymentInstallmentPlan[] = await getPaymentPlanInstallmentsForCustomer(quotePolicy.customerId, quotePolicy._name);
    await createEventStreamEntity(application.name, EEventType.Paid, <any>latestPaymentPlanInstallment.dateTimeOfPaymentMade || now,
        {
            QuotePolicySnapshot: quotePolicy,
            transactionType2: ETransactionType2.originalPremium,
            LatestPaymentPlanInstallmentSnapshot: latestPaymentPlanInstallment,
            PaymentPlanInstallmentsSnapshot: installments,
            CustomerSnapshot: customer,
            ContactSnapshot: contact,
        },
        {
            transactionType2: ETransactionType2.originalPremium,
        },
        "PaymentPlanInstallments",
        latestPaymentPlanInstallment._name,
        { timestamp: now }
    );
}

export async function createPaidRefundRecord(refundedPaymentInstallation: IPaymentInstallmentPlan, quotePolicy: IQuotePolicy) {
    const now = Date.now();
    // is it a real stripe refund
    const application: IApplication = await gcdsInstance.getOne({kind: "Application", name: quotePolicy.systemApplication});
    const customer: ICustomer = await gcdsInstance.getOne({ kind: "Customer", name: quotePolicy.customerId });
    const contact: IContact = await gcdsInstance.getOne({ kind: "Contact", name: customer.primaryContactId });
    if (refundedPaymentInstallation.stripeRefundAttempted == true) {
        const installments: IPaymentInstallmentPlan[] = await getPaymentPlanInstallmentsForCustomer(quotePolicy.customerId, quotePolicy._name);
        // refundAmount could be "0"
        //     this._quote.cancelledWhen = (new Date(_now || Date.now())).getTime();
        // need to add to paid
        await createEventStreamEntity(application.name, EEventType.Paid, quotePolicy.cancelledWhen,
            {
                QuotePolicySnapshot: quotePolicy,
                transactionType2: ETransactionType2.returnPremium,
                LatestPaymentPlanInstallmentSnapshot: refundedPaymentInstallation,
                PaymentPlanInstallmentsSnapshot: installments,
                CustomerSnapshot: customer,
                ContactSnapshot: contact,
            },
            {
                transactionType2: ETransactionType2.returnPremium,
            },
            "PaymentPlanInstallments",
            refundedPaymentInstallation._name,
            {
                timestamp: now
            }
        );
    }
}
// payment installations update
export const installmentPaid = firebaseFunctions
    .firestore
    .document((`QuotePolicy/{quotePolicyId}/PaymentPlanInstallments/{installmentIndex}`))
    .onUpdate(async (change: firebaseFunctions.Change<FirebaseFirestore.DocumentSnapshot>, context: firebaseFunctions.EventContext) => {
        const quotePolicyId: string = context.params.quotePolicyId;
        const installmentIndex: string = context.params.installmentIndex;
        const newData: IPaymentInstallmentPlan = <any>change.after.data();
        const oldData: IPaymentInstallmentPlan = <any>change.before.data();
        const quotePolicy: IQuotePolicy = await gcdsInstance.getOne(["QuotePolicy", quotePolicyId]);
        const application: IApplication = await gcdsInstance.getOne(["Application", quotePolicy.systemApplication]);
        const customer: ICustomer = await gcdsInstance.getOne(["Customer", quotePolicy.customerId]);
        const contact: IContact = customer ? customer.primaryContactId ? await gcdsInstance.getOne(["Contact", customer.primaryContactId]) : null : null;

        if (newData) {
            // is this event a valid refund?
            /*if (newData.attemptedRefund == true && (!oldData || oldData.attemptedRefund == false)) {
                // is it a real stripe refund
                if (newData.stripeRefundAttempted == true) {
                    const installments: IPaymentInstallmentPlan[] = await getPaymentPlanInstallmentsForCustomer(quotePolicy.customerId, quotePolicy._name);
                    // refundAmount could be "0"
                    const refundAmount: string = newData.refundAmount;
                    //     this._quote.cancelledWhen = (new Date(_now || Date.now())).getTime();
                    // need to add to paid
                    await createEventStreamEntity(application.name, EEventType.Paid, quotePolicy.cancelledWhen,
                        {
                            QuotePolicySnapshot: quotePolicy,
                            transactionType2: ETransactionType2.returnPremium,
                            LatestPaymentPlanInstallmentSnapshot: newData,
                            PaymentPlanInstallmentsSnapshot: installments,
                            CustomerSnapshot: customer,
                            ContactSnapshot: contact,
                        },
                        {
                            transactionType2: ETransactionType2.returnPremium,
                        },
                        "PaymentPlanInstallments",
                        (oldData || newData)._name,
                        context
                    );
                }
            }*/

            // is this event a new paid installment
            /*if ((!oldData || oldData.status == EPaymentStatus.pending) && newData.status == EPaymentStatus.received) {
                // we hav a new installment
                const amountPaid: string = newData.amountReceived;
                const installments: IPaymentInstallmentPlan[] = await getPaymentPlanInstallmentsForCustomer(quotePolicy.customerId, quotePolicy._name);
                await createEventStreamEntity(application.name, EEventType.Paid, <any>newData.dateTimeOfPaymentMade || Date.now(),
                    {
                        QuotePolicySnapshot: quotePolicy,
                        transactionType2: ETransactionType2.originalPremium,
                        LatestPaymentPlanInstallmentSnapshot: newData,
                        PaymentPlanInstallmentsSnapshot: installments,
                        CustomerSnapshot: customer,
                        ContactSnapshot: contact,
                    },
                    {
                        transactionType2: ETransactionType2.originalPremium,
                    },
                    "PaymentPlanInstallments",
                    (oldData || newData)._name,
                    context
                );
            }*/
        }

        const path = `QuotePolicy/${quotePolicyId}/PaymentPlanInstallments/${installmentIndex}`;
        const timestamp: number = new Date(context.timestamp).getTime();
        await safeDeltaMapper(newData, oldData, path, timestamp);

    });

// quote policy conversion
export async function createWrittenRecord(quotePolicy: IQuotePolicy) {
    const now = Date.now();
    const customer: ICustomer = await gcdsInstance.getOne({ kind: "Customer", name: quotePolicy.customerId });
    const contact: IContact = await gcdsInstance.getOne({ kind: "Contact", name: customer.primaryContactId });
    // detect if we have an old policy
    const policies: IQuotePolicy[] = await gcdsInstance.queryMany("QuotePolicy", [
        { propertyName: "policy", comparator: "=", value: true },
        { propertyName: "customerId", comparator: "=", value: quotePolicy.customerId },
        { propertyName: "systemApplication", comparator: "=", value: quotePolicy.systemApplication } // filter via sys app? not really generic
    ]);
    //  ETransactionType.Endorsement ETransactionType.Ammendment
    let transactionType: ETransactionType;
    if (!policies) {
        transactionType = ETransactionType.New;
    }
    else {
        transactionType = policies.length === 1 ? ETransactionType.New : ETransactionType.Renewal;
    }
    await createEventStreamEntity(quotePolicy.systemApplication, EEventType.Written, now,
        {
            QuotePolicySnapshot: quotePolicy,
            transactionType: transactionType || ETransactionType.New,
            CustomerSnapshot: customer || {},
            ContactSnapshot: contact || {},
        },
        {
            transactionType: transactionType || ETransactionType.New,
        },
        "QuotePolicy",
        quotePolicy._name,
        {
            timestamp: now
        }
    );
}

function changeStringNumberPolarity(numberString: string) {
    if (numberString[0] != "-") {
        return `-${numberString}`;
    }
    else {
        return numberString.slice(1, numberString.length - 1);
    }
}
/*
personalAccident
(map)
total
tax
GST
"52.50"
NRT
"14.24"
StampDuty
"0.00"
*/
export async function createWrittenCancelledRecord(_quotePolicy: IQuotePolicy) {
    const now = Date.now();
    const quotePolicy: IQuotePolicy = JSON.parse(JSON.stringify(_quotePolicy)); // clone
    // modify polarity of written and charge and preTaxGWPWritten and preTaxGWP
    const a: IPaymentPlanOutput = null;
    // a.written.
    Object.keys(quotePolicy.paymentPlan.written).forEach((category: string) => {
        // category e.g. total personalLiability etc
        // iterate each category
        Object.keys(quotePolicy.paymentPlan.written[category]).forEach((categoryKey: string) => {
            // tax / party
            Object.keys(quotePolicy.paymentPlan.written[category][categoryKey]).forEach((valueKey: string) => {
                quotePolicy.paymentPlan.written[category][categoryKey][valueKey] = changeStringNumberPolarity(quotePolicy.paymentPlan.written[category][categoryKey][valueKey]);
            });
        });
    });
    // for charge

    Object.keys(quotePolicy.paymentPlan.charge).forEach((category: string) => {
        // category e.g. total personalLiability etc
        // iterate each category
        Object.keys(quotePolicy.paymentPlan.charge[category]).forEach((categoryKey: string) => {
            // tax / party
            Object.keys(quotePolicy.paymentPlan.charge[category][categoryKey]).forEach((valueKey: string) => {
                quotePolicy.paymentPlan.charge[category][categoryKey][valueKey] = changeStringNumberPolarity(quotePolicy.paymentPlan.charge[category][categoryKey][valueKey]);
            });
        });
    });

    // for preTaxGWP

    Object.keys(quotePolicy.paymentPlan.preTaxGWP).forEach((preTaxKey: string) => {
        quotePolicy.paymentPlan.preTaxGWP[preTaxKey] = changeStringNumberPolarity(quotePolicy.paymentPlan.preTaxGWP[preTaxKey]);
    });

    // for preTaxGWP written

    Object.keys(quotePolicy.paymentPlan.preTaxGWPWritten).forEach((preTaxKey: string) => {
        quotePolicy.paymentPlan.preTaxGWPWritten[preTaxKey] = changeStringNumberPolarity(quotePolicy.paymentPlan.preTaxGWPWritten[preTaxKey]);
    });
    const application: IApplication = await gcdsInstance.getOne({ kind: "Application", name: quotePolicy.systemApplication });
    const customer: ICustomer = await gcdsInstance.getOne({ kind: "Customer", name: quotePolicy.customerId });
    const contact: IContact = await gcdsInstance.getOne({ kind: "Contact", name: customer.primaryContactId });
    await createEventStreamEntity(application.name, EEventType.Written, now,
        {
            QuotePolicySnapshot: quotePolicy,
            transactionType: ETransactionType.Cancellation,
            CustomerSnapshot: customer,
            ContactSnapshot: contact,
        },
        {
            transactionType: ETransactionType.Cancellation,
        },
        "QuotePolicy",
        quotePolicy._name,
        {
            timestamp: now
        }
    );
}

export const quotePolicyConversion = firebaseFunctions
    .firestore
    .document(`QuotePolicy/{quotePolicyId}`)
    .onWrite(async (change: firebaseFunctions.Change<FirebaseFirestore.DocumentSnapshot>, context: firebaseFunctions.EventContext) => {
        const quotePolicyId: string = context.params.quotePolicyId;
        const quotePolicy: IQuotePolicy = await gcdsInstance.getOne(["QuotePolicy", quotePolicyId]);
        const application: IApplication = await gcdsInstance.getOne(["Application", quotePolicy.systemApplication]);
        const newData: IQuotePolicy = change.after.exists ? <any>change.after.data() : null;
        const oldData: IQuotePolicy = change.before.exists ? <any>change.before.data() : null;
        const customer: ICustomer = await gcdsInstance.getOne(["Customer", quotePolicy.customerId]);
        const contact: IContact = customer ? customer.primaryContactId ? await gcdsInstance.getOne(["Contact", customer.primaryContactId]) : null : null;
        const path = `QuotePolicy/${quotePolicyId}`;
        const timestamp: number = new Date(context.timestamp).getTime();


        if (newData) {
            // if policy was false and now is true then we have detected a conversion
            // need to add to written

            // written
            // is the policy NEW?
            /*if (newData.policySetupAllFinished == true && (!oldData || !oldData.policySetupAllFinished)) {
                // detect if we have an old policy
                const policies: IQuotePolicy[] = await gcdsInstance.queryMany("QuotePolicy", [
                    { propertyName: "policy", comparator: "=", value: true },
                    { propertyName: "customerId", comparator: "=", value: quotePolicy.customerId },
                    { propertyName: "systemApplication", comparator: "=", value: quotePolicy.systemApplication } // filter via sys app? not really generic
                ]);
                //  ETransactionType.Endorsement ETransactionType.Ammendment
                let transactionType: ETransactionType;
                if (!policies) {
                    transactionType = ETransactionType.New;
                }
                else {
                    transactionType = policies.length === 1 ? ETransactionType.New : ETransactionType.Renewal;
                }
                await createEventStreamEntity(quotePolicy.systemApplication, EEventType.Written, <any>newData.policyConversionDate || Date.now(),
                    {
                        QuotePolicySnapshot: newData,
                        transactionType: transactionType || ETransactionType.New,
                        CustomerSnapshot: customer || {},
                        ContactSnapshot: contact || {},
                    },
                    {
                        transactionType: transactionType || ETransactionType.New,
                    },
                    "QuotePolicy",
                    (newData || oldData)._name,
                    context || {}
                );
            }*/

            // is the policy Cancelled?
            // cancelled
            /*if (newData.cancelled == true && (!oldData || oldData.cancelled == false)) {
                await createEventStreamEntity(application.name, EEventType.Written, <any>newData.policyConversionDate || Date.now(),
                    {
                        QuotePolicySnapshot: newData,
                        transactionType: ETransactionType.Cancellation,
                        CustomerSnapshot: customer,
                        ContactSnapshot: contact,
                    },
                    {
                        transactionType: ETransactionType.Cancellation,
                    },
                    "QuotePolicy",
                    (oldData || newData)._name,
                    context
                );
            }*/

            // DETECT MTA
            // oldData.policy == true && (!oldData || !oldData.paymentFinished)
            if (oldData && (oldData.policy = true)) {
                // && newData.gwpPretax != oldData.gwpPreTax
                // we have had an adjustment to the premium
                //  ETransactionType.Endorsement
                // @Vlad todo
            }
            else {
                // ETransactionType.Ammendment change with no change of premium
                // @Vlad todo
            }
        }

        // eeeeek!
        /*if (oldData && oldData.policy == true && newData.policy == false) {
            await firestore.collection("QuotePolicy").doc(oldData._name).set({policy: true}, {merge: true});
        }*/






        await updateAllStatsForKindEntity("QuotePolicy", newData).catch((err: any) => {
            console.error("ERROR CALCULATING STATS", err);
            console.error(err.stack);
        });

        await safeDeltaMapper(newData, oldData, path, timestamp);

    });

export const customerChange = firebaseFunctions
    .firestore
    .document(`Customer/{customerId}`)
    .onWrite(async (change: firebaseFunctions.Change<FirebaseFirestore.DocumentSnapshot>, context: firebaseFunctions.EventContext) => {
        const customerId: string = context.params.customerId;
        const newData: IQuotePolicy = change.after.exists ? <any>change.after.data() : null;
        const oldData: IQuotePolicy = change.before.exists ? <any>change.before.data() : null;
        const timestamp: number = new Date(context.timestamp).getTime();
        const path = `Customer/${customerId}`;

        if (newData) {
            // region
            if ((!oldData || oldData.region) != newData.region) {
                // possible change of premium to all policies
                // fetch all policies which might be affected
                const policies: IQuotePolicy[] = await gcdsInstance.queryMany("QuotePolicy", [
                    { propertyName: "policy", comparator: "=", value: true },
                    { propertyName: "customerId", comparator: "=", value: customerId },
                    { propertyName: "cancelled", comparator: "=", value: false },
                    { propertyName: "paymentFinished", comparator: "=", value: false }

                ]);

                // @Vlad todo
            }
        }

        /*
,
                    "Customer",
                    (oldData || newData)._name
        */


        await updateAllStatsForKindEntity("Customer", newData).catch((err: any) => {
            console.error("ERROR CALCULATING STATS", err);
            console.error(err.stack);
        });


        await safeDeltaMapper(newData, oldData, path, timestamp);

    });
export async function createWrittenContactAmmendment(customer: ICustomer, contact: IContact) {
    const now = Date.now();
    const customerId = customer._name;
    const policies: IQuotePolicy[] = await gcdsInstance.queryMany("QuotePolicy", [
        { propertyName: "policy", comparator: "=", value: true },
        { propertyName: "customerId", comparator: "=", value: customerId },
        { propertyName: "cancelled", comparator: "=", value: false },
        { propertyName: "paymentFinished", comparator: "=", value: false }

    ]);

    const seenApplications: Array<string> = [];
    const applications: IApplication[] = (await Promise.all(policies.map((policy: IQuotePolicy) => {
        return gcdsInstance.getOne(["Application", policy.systemApplication]);
    }))).filter((application: IApplication) => {
        if (!seenApplications.includes(application.name)) {
            seenApplications.push(application.name);
            return true;
        }
    }, {});

    await Promise.all(policies.map(async (policy: IQuotePolicy) => {
        const application: IApplication = applications.find((it: IApplication) => it.name == policy.systemApplication);
        const customer: ICustomer = await gcdsInstance.getOne(["Customer", policy.customerId]);
        // for each product
        // application reporting products
        await createEventStreamEntity(application.name, EEventType.Written, now,
            {
                QuotePolicySnapshot: policy,
                transactionType: ETransactionType.Ammendment,
                CustomerSnapshot: customer,
                ContactSnapshot: contact,
            },
            {
                transactionType: ETransactionType.Ammendment,
            },
            "Contact",
            contact._name,
            {
                timestamp: now
            }
        );
    }));

}
export const contactChange = firebaseFunctions
    .firestore
    .document(`Contact/{contactId}`)
    .onUpdate(async (change: firebaseFunctions.Change<FirebaseFirestore.DocumentSnapshot>, context: firebaseFunctions.EventContext) => {
        const contactId: string = context.params.contactId;
        const newData: IContact = <any>change.after.data();
        const oldData: IContact = <any>change.before.data();
        const timestamp: number = new Date(context.timestamp).getTime();
        const path = `Contact/${contactId}`;

        /*
            customerId: string;
    name: string;
    phone?: string;
    email: string;
    dob?: number | Date;
    [key: string]: any;
    */
        // fetch all policies for this customer
        /*const customerId: string = newData.customerId;

        const policies: IQuotePolicy[] = await gcdsInstance.queryMany("QuotePolicy", [
            { propertyName: "policy", comparator: "=", value: true },
            { propertyName: "customerId", comparator: "=", value: customerId },
            { propertyName: "cancelled", comparator: "=", value: false },
            { propertyName: "paymentFinished", comparator: "=", value: false }

        ]);

        const seenApplications: Array<string> = [];
        const applications: IApplication[] = (await Promise.all(policies.map((policy: IQuotePolicy) => {
            return gcdsInstance.getOne(["Application", policy.systemApplication]);
        }))).filter((application: IApplication) => {
            if (!seenApplications.includes(application.name)) {
                seenApplications.push(application.name);
                return true;
            }
        }, {});

        await Promise.all(policies.map(async (policy: IQuotePolicy) => {
            const application: IApplication = applications.find((it: IApplication) => it.name == policy.systemApplication);
            const customer: ICustomer = await gcdsInstance.getOne(["Customer", policy.customerId]);
            // for each product
            // application reporting products
            await createEventStreamEntity(application.name, EEventType.Written, Date.now(),
                {
                    QuotePolicySnapshot: policy,
                    transactionType: ETransactionType.Ammendment,
                    CustomerSnapshot: customer,
                    ContactSnapshot: newData,
                },
                {
                    transactionType: ETransactionType.Ammendment,
                },
                "Contact",
                (oldData || newData)._name,
                context
            );
        }));*/

        await safeDeltaMapper(newData, oldData, path, timestamp);

    });


// stats2
// firebase.firestore.FieldValue.increment;
import { helperMethods } from "@vaultspace/firebase-server";
import { IPaymentPlan } from "@vaultspace/pdf/src/models";
import { IPaymentPlanOutput } from "./price-engine";
export const newStatsAddition = firebaseFunctions
    .firestore
    .document(`/Statistics2/{appName}/kind/{kind}/{namedQuery}/{quotePolicyId}`)
    .onCreate(async (snap, context) => {
        const appName = context.params.appName;
        const kind = context.params.kind;
        const namedQuery = context.params.namedQuery;
        const quotePolicyId = context.params.quotePolicyId;
        // get ref
        await firestore.collection("StatisticsRead").doc(appName).collection("Kind").doc(kind).set(
            {
                [namedQuery]: helperMethods.increment
            }, { merge: true });

    });
// col/doc/col/doc/col/doc/col/doc
// /Statistics2/imaliaDaycare/kind/QuotePolicy/ActivePolicies/STRIPETESTING_Etn9zALc1xXUzvi2xDJYGUXCoDxIPztFrAtvbTko1IjF2YFEDS
export const newStatsSubtraction = firebaseFunctions
    .firestore
    .document(`/Statistics2/{appName}/kind/{kind}/{namedQuery}/{quotePolicyId}`)
    .onDelete(async (snap, context) => {
        const appName = context.params.appName;
        const kind = context.params.kind;
        const namedQuery = context.params.namedQuery;
        const quotePolicyId = context.params.quotePolicyId;
        // get ref
        await firestore.collection("StatisticsRead").doc(appName).collection("Kind").doc(kind).set(
            {
                [namedQuery]: helperMethods.decrement
            }, { merge: true });

    });
