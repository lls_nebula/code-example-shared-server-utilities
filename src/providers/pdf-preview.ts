import * as pdf from "@vaultspace/pdf";
import {
    ICreateFintechScheduleArguments,
    ICreateFdcScheduleArguments,
    ICreateCoCArguments,
        } from "@vaultspace/pdf/dist/src/models";
import { IQuotePolicy, ICustomer, IContact } from "@vaultspace/neon-shared-models";
import { gcdsInstance } from "@vaultspace/gdatastore-v1";
import { cocBuilder } from "../util/companyName/coc-builder"; // util/imaliaDaycare/coc-builder
import { fdcScheduleBuilder } from "../util/companyName/fdcSchedule-builder";
import { updateEntityCollectionWithDelta } from "./price-engine";
export interface ICreateDocArguments {
    QuotePolicy: IQuotePolicy;
    Contact: IContact;
    Customer: ICustomer;
}

export enum EDocumentType {
    CoC = "CoC",
    Schedule = "Schedule"
}

export interface IDocumentChangeDetectionOrGenerate {
    documentationModified: boolean;
    buffer?: Buffer;
}

function diffArgs(before: any, after: any) {
    const diff: any = {};
    Object.keys(after).forEach((afterKey: string) => {
        const beforeValue = before[afterKey];
        if (beforeValue != after[afterKey]) {
            diff[afterKey] = {changed: true, before: beforeValue, after: after[afterKey]};
        }
    });
    return diff;
}

function diffExists(diff: any) {
    return Object.keys(diff).some((it: any) => {
        if (diff[it].changed === true) return true;
    });
}

export async function generateDocumentPreview(quotePolicyId: string, collectionDelta: any) {
    const quotePolicy: IQuotePolicy = await gcdsInstance.getOne({kind: "QuotePolicy", name: quotePolicyId});
    const customer: ICustomer = quotePolicy.customerId ? await gcdsInstance.getOne({kind: "Customer", name: quotePolicy.customerId}) : null;
    const contact: IContact = customer.primaryContactId ? await gcdsInstance.getOne({kind: "Contact", name: customer.primaryContactId}) : null;
    const oldArguments: ICreateDocArguments = JSON.parse(JSON.stringify( {
        Customer: customer,
        QuotePolicy: quotePolicy,
        Contact: contact
    }));
    const oldCocArguments = cocBuilder(oldArguments.Customer, oldArguments.QuotePolicy);
    const oldScheduleArguments = fdcScheduleBuilder(oldArguments.Customer, oldArguments.QuotePolicy, oldArguments.Contact);
    // apply collection delta
    const modified = updateEntityCollectionWithDelta(oldArguments.QuotePolicy, oldArguments.Customer, oldArguments.Contact, collectionDelta);
    const newCocArguments = cocBuilder(modified.Customer, modified.QuotePolicy);
    const newScheduleArguments = fdcScheduleBuilder(modified.Customer, modified.QuotePolicy, modified.Contact);

    const cocHasChange = diffExists(diffArgs(oldCocArguments, newCocArguments));
    const scheduleHasChange = diffExists(diffArgs(oldScheduleArguments, newScheduleArguments));

    const deltaCoC: IDocumentChangeDetectionOrGenerate = {
        documentationModified: cocHasChange ? true : false,
        buffer: cocHasChange ? await pdf.createCoC(newCocArguments) : null
    };

    const deltaSchedule: IDocumentChangeDetectionOrGenerate = {
        documentationModified: scheduleHasChange ? true : false,
        buffer: scheduleHasChange ? await pdf.createFdcSchedule(newScheduleArguments) : null
    };

    return {
        CoC: deltaCoC,
        schedule: deltaSchedule
    };
}

/*
pdf.createFdcSchedule(scheduleOptions1).then((buf) => {
                        return storePersonalDocument(quoteId, appName, customerId, "Imalia - FDC - Schedule - v1.0.0", buf);
                    }).then(link => link),

                    pdf.createCoC(options.coc).then((buf: any) => {
                        return storePersonalDocument(quoteId, appName, customerId, "Imalia - FDC - Certificate of Currency - v1.0.0", buf);
                    }).then((link: any) => link),

                                const coc: ICreateCoCArguments = cocBuilder(args.Customer, args.QuotePolicy);
            const fdcSchedule: ICreateFdcScheduleArguments = fdcScheduleBuilder(args.Customer, args.QuotePolicy, args.Contact);


*/
