import { bucket } from "@vaultspace/firebase-server";
import { IBufferAttachment } from "@vaultspace/emailer";

export async function createFileWithPath(path: string, data: Buffer) {
    console.log(`NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN`);
    console.log(path);
    console.log(`NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN`);


    return new Promise(async (resolve, reject) => {
        const stream = bucket.file(path).createWriteStream();

        stream.on("error", (err) => {
            return reject(err);
        }).on("finish", () => {
            return resolve();
        }).end(data); // Buffer.from(data)
    });
}

export async function fetchFileFromPath(path: string): Promise<Buffer[]> {
    return new Promise(async (resolve, reject) => {
        try {
            const file = await bucket.file(path).download();
            resolve(file);
        }
        catch (e) {
            reject(e);
        }
    });
}

export async function fetchFilesFromDirectory(directory: string): Promise<Array<IBufferAttachment>> {
    return new Promise(async (resolve, reject) => {
        try {
            const files = await bucket.getFiles({directory});

            const results = await Promise.all(
                files[0].map(file => file.download())
            );

            resolve( results.map((item, i) => {
                const fileNameFull = files[0][i].name;
                const fileNameFullSplit = fileNameFull.split("/");
                return ({
                    filename: fileNameFullSplit[fileNameFullSplit.length - 1],
                    content: item[0],
                });
            }) );
        }
        catch (e) {
            reject(e);
        }
    });
}

export async function createJsonFileWithPath(path: string, data: any) {
    const buffer: Buffer = Buffer.from(JSON.stringify(data));
    return await createFileWithPath(path, buffer);
}

export async function fetchJsonFileFromPath(path: string) {
    const data = await fetchFileFromPath(path);
    const stringData: string = data[0].toString("utf8");
    return JSON.parse(stringData);
}

export async function getFilesListFromDirectory(directory: string): Promise<Array<string>> {
    try {
        const files = await bucket.getFiles({directory});
        return files[0].map(file => `/${file.name}`);

    } catch (err) { throw err; }
}
