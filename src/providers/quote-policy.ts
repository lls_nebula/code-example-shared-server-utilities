import { IGDSKey } from "@vaultspace/gdatastore-v1";
import { gcdsInstance } from "../gcds-instance";
import { IQuotePolicy, EApprovedStatus, IApplicationFunnel } from "@vaultspace/neon-shared-models";
import { v4 as uuid } from "uuid";
import { fetchCustomer } from "./customer";
import { fetchApplicationFunnel } from "./application";

export const quotePolicyKind = "QuotePolicy";

export async function initDefaultQuote(customerId: string, applicationName: string, sourceClass?: string): Promise<IQuotePolicy> {
    const newQuoteId: string = uuid();
    const now = new Date();
    const customer = await fetchCustomer(customerId);
    const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(applicationName, sourceClass || null);

    const defaultQuotePolicy: IQuotePolicy = {
        quoteId: newQuoteId,
        customerId: customerId,
        systemApplication: applicationName,
        quoteCreatedDate: now.getTime(),
        customerDetailsSnapshot: <any> JSON.stringify(customer),
        approvedStatus: EApprovedStatus.provisional,
        backDated: false,
        quoteNeedsApproval: true, // depends on answers
        quoteValidUntil: <any> now.getTime(), // its a number now
        policy: false, // its not a policy until payment
        agreed: false,
        quoteExpired: false,
        signed: false,
        PRICE_ALGORITHM_VERSION: "v7.0.0",
        funnelName: applicationFunnel,
        paymentPlan: null,
        claim: false,
        numberOfMonthUnitsForTerm: 12
    };

    return defaultQuotePolicy;
}

export async function fetchQuote(quoteID: string): Promise<IQuotePolicy> {
    const key: IGDSKey = { kind: quotePolicyKind, name: quoteID };

    return await gcdsInstance.getOne(key);
}

export async function patchQuote(quoteId: string, data: any): Promise<any> {
    const key: IGDSKey = { kind: quotePolicyKind, name: quoteId };
    return await gcdsInstance.createOne(key, data, { merge: true });
}