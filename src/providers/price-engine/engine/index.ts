import * as xlsx from "xlsx";
import { contextComputedField_computer } from "../../events";
import { recompute } from "./sheet-calc";
import { EFieldType, ISpreadSheetInput, ISpreadSheetOutput, EPaymentPlan } from "@vaultspace/neon-shared-models";

export function eval_model(modelData: Buffer, inputs: ISpreadSheetInput[], outputs: ISpreadSheetOutput[], context: any) {
    const model = xlsx.read(modelData);
    // change forumulas with cell lookups
    return eval_model_internal(model, inputs, outputs, context);
}

export function forceLookup(model: xlsx.WorkBook) {
    model.SheetNames.forEach((sheetName) => {
        Object.keys(model.Sheets[sheetName])
            .filter((i) => {
                // console.log("i", i, typeof i);
                return !i || !i.includes("!");
            })
            .forEach((cellName) => {
                const cell: xlsx.CellObject = model.Sheets[sheetName][cellName];
                // console.log("cellName", cellName, cell, typeof cell);
                if (cell.f && cell.f.includes("!")) {
                    const cellExclamationSplit = cell.f.split("!");
                    const [lhs, cellName] = cellExclamationSplit;
                    const sheetName = lhs.replace(/'/g, "");
                    cell.v = model.Sheets[sheetName][cellName].v;
                    cell.f = undefined;
                    console.log("][][][][][][][][][][][][][", sheetName, cellName, cell);

                }
            });
    });
}
// "preTaxGWP", "written", "calculatedInputs", "commission", "charge", "excess", "preTaxGWPWritten", "limit"
export interface IPaymentPlanOutput {
    "calculatedInputs": { [inputName: string]: number };
    "charge": {
        [productName: string]: {
            tax: {
                [taxName: string]: string;
            },
            party: {
                [partyName: string]: string;
            }
        },
        total: {
            tax: {
                [taxName: string]: string;
            },
            party: {
                [partyName: string]: string;
            }
        }
    };
    written: {
        [productName: string]: {
            tax: {
                [taxName: string]: string;
            },
            party: {
                [partyName: string]: string;
            }
        },
        total: {
            tax: {
                [taxName: string]: string;
            },
            party: {
                [partyName: string]: string;
            }
        }
    };
    "excess": {
        [productName: string]: string;
    };
    "commission": {
        [productName: string]: string;
    };
    "limit": {
        [productName: string]: string;
    };
    "preTaxGWP": {
        [productName: string]: string;
        total: string;
    };
    "preTaxGWPWritten": {
        [productName: string]: string;
        total: string;
    };
}
export function eval_model_internal(model: xlsx.WorkBook, inputs: ISpreadSheetInput[], outputs: ISpreadSheetOutput[], context: any) {
    const returnData: any = { calculatedInputs: {} };


    // convert inputs
    inputs.forEach((input: ISpreadSheetInput) => {
        const inputFromContext = contextComputedField_computer(input.fromInput, context);
        let calculatedInput = null;
        if (input.fieldMap) {
            if (input.fieldMap.type == EFieldType.numberEnum) {
                calculatedInput = input.fieldMap.numberEnumMap[inputFromContext];
            }
        }
        else {
            calculatedInput = inputFromContext;
        }
        returnData.calculatedInputs[input.fromInput] = calculatedInput;
        // console.log(input.sheet, input.cell, calculatedInput);
        model.Sheets[input.sheet][input.cell].v = calculatedInput;
        model.Sheets[input.sheet][input.cell].f = undefined;
    });

    // console.log("------------------------------------------------------------------------");

    // recompute workbook
    recompute(model);
    // console.log(JSON.stringify(model));

    // extract output
    outputs.forEach((output: ISpreadSheetOutput) => {
        // to output split
        const toOutPutSplit: string[] = output.toOutput.split(".");
        let cellOutput: any = model.Sheets[output.sheet][output.cell].v;
        if (!isNaN(cellOutput)) {
            if (output.toOutput.includes("commission")) {
                cellOutput = cellOutput.toString();
            }
            else if (output.toOutput.includes("excess")) {
                cellOutput = cellOutput.toString();
            }
            else if (output.toOutput.includes("limit")) {
                cellOutput = cellOutput.toString();
            }
            else {
                cellOutput = cellOutput.toFixed(2);
            }
        }

        if (toOutPutSplit.length == 2) {
            const first: string = toOutPutSplit[0];
            const second: string = toOutPutSplit[1];
            if (!returnData.hasOwnProperty(first)) {
                returnData[first] = {};
            }
            returnData[first][second] = cellOutput;
        }
        else if (toOutPutSplit.length == 3) {
            const first: string = toOutPutSplit[0];
            const second: string = toOutPutSplit[1];
            const third: string = toOutPutSplit[2];
            if (!returnData.hasOwnProperty(first)) {
                returnData[first] = {};
            }
            if (!returnData[first].hasOwnProperty(second)) {
                returnData[first][second] = {};
            }
            returnData[first][second][third] = cellOutput;

        }
        else if (toOutPutSplit.length == 4) {
            const first: string = toOutPutSplit[0];
            const second: string = toOutPutSplit[1];
            const third: string = toOutPutSplit[2];
            const forth: string = toOutPutSplit[3];

            if (!returnData.hasOwnProperty(first)) {
                returnData[first] = {};
            }
            if (!returnData[first].hasOwnProperty(second)) {
                returnData[first][second] = {};
            }
            if (!returnData[first][second].hasOwnProperty(third)) {
                returnData[first][second][third] = {};
            }
            returnData[first][second][third][forth] = cellOutput;

        }

        // returnData[first][second] = model.Sheets[output.sheet][output.cell].v;
    });

    // calculate written
    // console.log("00000", JSON.stringify(returnData, null, 4));
    const paymentPlan: EPaymentPlan = context.QuotePolicy.paymentPlanType;
    returnData["written"] = {};
    // console.log("paymentPlan", paymentPlan, context);

    //  console.log("-----------------------------------------------------------------------------");

    Object.keys(returnData.charge).forEach((productName: string) => {
        console.log("iter charge product name", productName);
        Object.keys(returnData.charge[productName].tax).forEach((taxName: string) => {
            console.log("iter tax name", taxName);
            if (!returnData.written[productName]) {
                returnData.written[productName] = { tax: {}, party: {} };
            }
            if (paymentPlan == EPaymentPlan.Monthly) {
                returnData.written[productName].tax[taxName] = (parseFloat(returnData.charge[productName].tax[taxName]) * 12).toFixed(2);
            }
            else if (paymentPlan == EPaymentPlan.Singular) {
                returnData.written[productName].tax[taxName] = returnData.charge[productName].tax[taxName];
            }
        });

    });

    // fix me deal with other terms

    Object.keys(returnData.charge).forEach((productName: string) => {
        Object.keys(returnData.charge[productName].party).forEach((partyName: string) => {
            if (!returnData.written[productName]) {
                returnData.written[productName] = { tax: {}, party: {} };
            }
            if (paymentPlan == EPaymentPlan.Monthly) {
                returnData.written[productName].party[partyName] = (parseFloat(returnData.charge[productName].party[partyName]) * 12).toFixed(2);
            }
            else if (paymentPlan == EPaymentPlan.Singular) {
                returnData.written[productName].party[partyName] = returnData.charge[productName].party[partyName];
            }
        });

    });

    // deal with written gwp
    returnData.preTaxGWPWritten = {};
    Object.keys(returnData.preTaxGWP).forEach((thing: string) => {
        if (paymentPlan == EPaymentPlan.Monthly) {
            returnData.preTaxGWPWritten[thing] = (parseFloat(returnData.preTaxGWP[thing]) * 12).toFixed(2);
        }
        else if (paymentPlan == EPaymentPlan.Singular) {
            returnData.preTaxGWPWritten[thing] = returnData.preTaxGWP[thing];

        }
    });

    // console.log("00009999999999999999999999999999999999999999999");
    // console.log(JSON.stringify(returnData, null, 4));







    // F32 { t: 'n', v: NaN, f: 'SUM(F30:F31)*$C$16', w: '0.00000000' }



    // G61-SUM(D61:F61)'


    return returnData;
}


////// types:






/*
    console.log("###############################################################");
    console.log("G35", model.Sheets["v0.0.14"]["G35"]);
    console.log("D35", model.Sheets["v0.0.14"]["D35"]);
    console.log("E35", model.Sheets["v0.0.14"]["E35"]);
    console.log("F35", model.Sheets["v0.0.14"]["F35"]);

    // C35 { t: 'n', v: NaN, f: 'G35-SUM(D35:F35)', w: '1.11000000' }


    console.log("C35", model.Sheets["v0.0.14"]["C35"]);

    console.log("C38", model.Sheets["v0.0.14"]["C38"]);
    console.log("C39", model.Sheets["v0.0.14"]["C39"]);
    console.log("C40", model.Sheets["v0.0.14"]["C40"]);
    console.log("C41", model.Sheets["v0.0.14"]["C41"]);
    console.log("C42", model.Sheets["v0.0.14"]["C42"]);

    console.log("C44", model.Sheets["v0.0.14"]["C44"]);


    console.log("C44", model.Sheets["v0.0.14"]["C44"]);
    console.log("G45", model.Sheets["v0.0.14"]["G45"]);
    console.log("G58", model.Sheets["v0.0.14"]["G58"]);
    console.log("G35", model.Sheets["v0.0.14"]["G35"]);
    console.log("D35", model.Sheets["v0.0.14"]["D35"]);
    console.log("E35", model.Sheets["v0.0.14"]["E35"]);
    console.log("F35", model.Sheets["v0.0.14"]["F35"]);

    // C35 { t: 'n', v: NaN, f: 'G35-SUM(D35:F35)', w: '1.11000000' }


    console.log("C35", model.Sheets["v0.0.14"]["C35"]);

    console.log("C38", model.Sheets["v0.0.14"]["C38"]);
    console.log("C39", model.Sheets["v0.0.14"]["C39"]);
    console.log("C40", model.Sheets["v0.0.14"]["C40"]);
    console.log("C41", model.Sheets["v0.0.14"]["C41"]);
    console.log("C42", model.Sheets["v0.0.14"]["C42"]);

    console.log("C44", model.Sheets["v0.0.14"]["C44"]);


    console.log("C44", model.Sheets["v0.0.14"]["C44"]);
    console.log("G45", model.Sheets["v0.0.14"]["G45"]);
    console.log("G58", model.Sheets["v0.0.14"]["G58"]);
    console.log("------------------------------------------------");
*/