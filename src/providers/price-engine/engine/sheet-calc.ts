const XLSX_CALC = require("@vaultspace/xlsx-calc");
const  formulajs = require("formulajs");

import * as xlsx from "xlsx";
// import formula fixes
import { MATCH, ROUND, IF } from "../formula-fixes";

// S5SCalc.XLSXLib = xlsx;

// import and overide
formulajs.MATCH = MATCH;
formulajs.ROUND = ROUND;
formulajs.IF = IF;

XLSX_CALC.import_functions(formulajs, {override: true});

export function recompute(a: xlsx.WorkBook) {
    XLSX_CALC(a);
    // S5SCalc.recalculate(a);
    return a;
}