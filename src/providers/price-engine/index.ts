import { eval_model } from "./engine/index";
import { bucket } from "@vaultspace/firebase-server";
import { fetchFileFromPath } from "../storage";
import { gcdsInstance } from "@vaultspace/gdatastore-v1";
import { IQuotePolicy, ICustomer, IPricingModelOutputs, IResponse, IApplication,
  IPricingModelSanitizedPostTax, IPricingModelSanitizedPreTax, IApplicationPricingModelDetails, IContact
 } from "@vaultspace/neon-shared-models";
import { IPaymentPlanOutput } from "./engine";
export * from "./engine/index";

export async function eval_application_model(appName: string, modelName: string, version: string, context: any): Promise<IPricingModelOutputs | IResponse> {
    // get model data
    const modelData: any = await downloadModelFile(modelName, appName);
    // get mapping data
    const modelMappingData = await gcdsInstance.getOne(["Application", appName, "ApplicationPriceEngineModel", modelName]);

    if (!modelData) {
        throw {error: true, message: `Could not find model data for model with applicationName: ${appName} fileName: ${modelName}`, code: 11922232};
    }
    if (!modelMappingData) {
        throw {error: true, message: `Could not find model mapping data for model with applicationName: ${appName} fileName: ${modelName} version: ${version}`, code: 11922233};

    }

    const modelVersion = modelMappingData.versions;
    if (!modelVersion) {
        throw {error: true, message: `Could not find model versions for model with applicationName: ${appName} fileName: ${modelName}`, code: 11922234};
    }
    const versionData = modelMappingData.versions[parseInt(version)];
    if (!versionData) {
        throw {error: true, message: `Could not find model version for model with applicationName: ${appName} fileName: ${modelName} version: ${version}`, code: 11922233};
    }
    const inputs = versionData.inputs;
    const outputs = versionData.outputs;

    if (!inputs) {
        throw {error: true, message: `Could not find model inputs for model with applicationName: ${appName} fileName: ${modelName} version: ${version}`, code: 11922233};
    }

    if (!outputs) {
        throw {error: true, message: `Could not find model outputs for model with applicationName: ${appName} fileName: ${modelName} version: ${version}`, code: 11922233};
    }

    try {
        const result = eval_model(modelData[0], inputs, outputs, context);
        return result;
    }
    catch (e) {
        return {error: true, message: "Error occured when evaulating a model", code: 222422, data: e.stack};
    }
}


/*
        quote.PRICE_ALGORITHM_MODEL_NAME = appPriceDetails.modelName;
        quote.PRICE_ALGORITHM_MODEL_VERSION = appPriceDetails.modelVersion;
        quote.PRICE_ALGORITHM_SYSTEM_VERSION = appPriceDetails.systemVersion;
*/
export async function eval_quote_model_charge(quoteId: string): Promise<IPricingModelOutputs | IResponse>  {
    const quote: IQuotePolicy = await gcdsInstance.getOne(["QuotePolicy", quoteId]);
    const customer: ICustomer = await gcdsInstance.getOne(["Customer", quote.customerId]);
    const appName = quote.systemApplication;
    const modelName = quote.PRICE_ALGORITHM_MODEL_NAME;
    const modelVersion = quote.PRICE_ALGORITHM_MODEL_VERSION;
    return eval_application_model(appName, modelName, modelVersion,
        {
            Customer: customer,
            QuotePolicy: quote,
            isManualRating: 0,
            daysOfRisk: 0,
            monthsOfRisk: 0,
        }
    );
}

/// BESPOKE

function tryFloat(val: string) {
  const ret = parseFloat(val);
  if (isNaN(ret)) return val;
  return ret;
}
// store this in the db later
const configForManualForImaliaDayCare: any = {
  "paymentPlan.preTaxGWPWritten.generalLiability": "manualPremiumLiability", // editable
  "paymentPlan.preTaxGWPWritten.personalAccident": "manualPremiumPersonalAccident", // editable
  "paymentPlan.preTaxGWPWritten.brokerFee": "manualPremiumBrokerFee", // editable
  "paymentPlan.preTaxGWPWritten.debtCollectorFeeAndTelephoneLegalAdvice": "manualPremiumLegalAddOn", // editable
  "paymentPlan.limit.generalLiability": "manualLimitLiability", // editable
  "paymentPlan.limit.personalAccident": "manualLimitPersonalAccident", // editable
  "paymentPlan.excess.generalLiability": "manualExcessLiability", // editable
  "paymentPlan.excess.personalAccident": "manualExcessPersonalAccident",
  "paymentPlan.commission.personalAccident": "manualCommissionPersonalAccident",
  "paymentPlan.commission.generalLiability": "manualCommissionLiability"
};

/*
util for extracting the config
*/
export function calculateManualInputsFromModel(paymentPlan: IPaymentPlanOutput) {
  const outputs: any = {};
  Object.keys(configForManualForImaliaDayCare).forEach((contextKey: string) => {
    const outputName = configForManualForImaliaDayCare[contextKey];
    // extract value from context key
    const value = tryFloat(contextComputedField_computer(contextKey, {paymentPlan: paymentPlan}));
    outputs[outputName] = value;
  });
  return outputs;
}

import { contextComputedField_computer } from "../events";

/*
function calculateRestOfPaymentPlan(inputs: any, paymentPlan: IPaymentPlanOutput) {
  Object.keys(configForManualForImaliaDayCare).forEach((fieldPath: string) => {
      // compute fieldPath value from paymentPan
      const value = tryFloat(contextComputedField_computer(fieldPath, { paymentPlan: paymentPlan }));
      const key = configForManualForImaliaDayCare[fieldPath];
      console.log("fieldPath", key, value);
      inputs[key] = value;
  });
  return inputs;
}

function calculateRestOfPaymentPlan_clone(results: IPaymentPlanOutput, originalContext: IPriceContext) {
  const cloneResults = JSON.parse(JSON.stringify(results));
  const cloneContext: IPriceContext = JSON.parse(JSON.stringify(originalContext));
  const modifiedPaymentPlan = calculateRestOfPaymentPlan(cloneContext, cloneResults.QuotePolicy.paymentPlan);
  return outputs;
}*/

function calculateRestOfPaymentPlan_clone(standardRatingPriceOutput: IPricingModelOutputs, bespokeRatingContext: IPriceContext): IPriceContext {
  const cloneBespokeContext: IPriceContext = JSON.parse(JSON.stringify(bespokeRatingContext));
  // get updated manual inputs
  Object.keys(configForManualForImaliaDayCare).forEach((fieldPath: string) => {
    const outKey = configForManualForImaliaDayCare[fieldPath]; // outkey e.g. manualPremiumLiability
    const inputKey = fieldPath; // e.g. paymentPlan.preTaxGWPWritten.generalLiability
    const value = tryFloat(contextComputedField_computer(fieldPath, { paymentPlan: standardRatingPriceOutput }));
    cloneBespokeContext[outKey] = value;
  });
  return cloneBespokeContext;
}

function typeCodeWrap(value: any) {
  if (typeof value === "string") {
    return `\`${value}\``;
  }
  if (typeof value === "object") {
    return JSON.stringify(value);
  }
  return value;
}
export interface IEntityCollectionModifiedByDelta {
  QuotePolicy: IQuotePolicy;
  Customer: ICustomer;
  Contact: IContact;
}
export function updateEntityCollectionWithDelta(quotePolicy: IQuotePolicy, customer: ICustomer, contact: IContact, delta: any): IEntityCollectionModifiedByDelta {
  /* delta
    {"Customer.providesOutOfHomeCare": true/false}
    {"Customer.formalName": "hello"}
  */
  const context = {
    context: {
      QuotePolicy: quotePolicy,
      Customer: customer,
      Contact: contact
    }
  };
  let entityChangeCode = ``;
  Object.keys(delta).forEach((entityPath: string) => {
    const value: any = delta[entityPath];
    entityChangeCode += `context.${entityPath} = ${typeCodeWrap(value)};`;
  });
  const code = `${entityChangeCode};context;`;
  return contextComputedField_computer(code, context);
}
export interface IPriceContext {
  QuotePolicy: IQuotePolicy;
  Customer: ICustomer;
  isManualRating: number;
  daysOfRisk: number;
  monthsOfRisk: number;
  [wildCard: string]: any;
}
export async function eval_quote_model_bespoke(quoteId: string, entityDeltas: any, manualArgs: any): Promise<IPricingModelOutputs | IResponse>  {
  let quote: IQuotePolicy = await gcdsInstance.getOne(["QuotePolicy", quoteId]);
  let customer: ICustomer = await gcdsInstance.getOne(["Customer", quote.customerId]);
  let contact: IContact = await gcdsInstance.getOne(["Contact", customer.primaryContactId]);

  const app: IApplication = await gcdsInstance.getOne(["Application", quote.systemApplication]);
  const livePEDetails = await getLiveModelDetailsForApplication(app);
  const peQuoteDetails = getPricingModelDetailsForQuote(quote);

  const appName = quote.systemApplication;
  const modelName = peQuoteDetails.modelName || livePEDetails.modelName; // quote.PRICE_ALGORITHM_MODEL_NAME;
  const modelVersion = peQuoteDetails.modelVersion || livePEDetails.modelVersion; // quote.PRICE_ALGORITHM_MODEL_VERSION;
  /*
    {"Customer.providesOutOfHomeCare": true/false}
  */
 // update entity collection with delta
  const modifiedEntityCollection = updateEntityCollectionWithDelta(quote, customer, contact, entityDeltas);
  quote = modifiedEntityCollection.QuotePolicy;
  customer = modifiedEntityCollection.Customer;
  contact = modifiedEntityCollection.Contact;
  const standardRating: IPricingModelOutputs = <IPricingModelOutputs> await eval_application_model(appName, modelName, modelVersion,
      {
          Customer: customer,
          QuotePolicy: quote,
          isManualRating: 0,
          daysOfRisk: 0,
          monthsOfRisk: 0,
      }
  );
  const bespokeDefaultContext: any = {
    QuotePolicy: quote,
    Customer: customer,
    isManualRating: 1,
    daysOfRisk: 0,
    monthsOfRisk: 0,
  };
  const bespokeCorrectedContext =  await calculateRestOfPaymentPlan_clone(<any> standardRating, bespokeDefaultContext);
  Object.keys(manualArgs).forEach((argName: string) => {
    bespokeCorrectedContext[argName] = manualArgs[argName];
  });
  console.log("#########################################");
  console.log("bespokeCorrectedContext", bespokeCorrectedContext);
  return eval_application_model(appName, modelName, modelVersion, bespokeCorrectedContext);
}

export async function eval_quote_model_standard(quoteId: string, entityDeltas: any): Promise<IPricingModelOutputs | IResponse>  {
  let quote: IQuotePolicy = await gcdsInstance.getOne(["QuotePolicy", quoteId]);
  let customer: ICustomer = await gcdsInstance.getOne(["Customer", quote.customerId]);
  let contact: IContact = await gcdsInstance.getOne(["Contact", customer.primaryContactId]);
  const app: IApplication = await gcdsInstance.getOne(["Application", quote.systemApplication]);
  const livePEDetails = await getLiveModelDetailsForApplication(app);
  const peQuoteDetails = getPricingModelDetailsForQuote(quote);

  const appName = quote.systemApplication;
  const modelName = peQuoteDetails.modelName || livePEDetails.modelName; // quote.PRICE_ALGORITHM_MODEL_NAME;
  const modelVersion = peQuoteDetails.modelVersion || livePEDetails.modelVersion; // quote.PRICE_ALGORITHM_MODEL_VERSION;
  /*
    {"Customer.providesOutOfHomeCare": true/false}
  */
 // update entity collection with delta
  const modifiedEntityCollection = updateEntityCollectionWithDelta(quote, customer, contact, entityDeltas);
  quote = modifiedEntityCollection.QuotePolicy;
  customer = modifiedEntityCollection.Customer;
  contact = modifiedEntityCollection.Contact;
  const standardRating: IPricingModelOutputs = <IPricingModelOutputs> await eval_application_model(appName, modelName, modelVersion,
      {
          Customer: customer,
          QuotePolicy: quote,
          isManualRating: 0,
          daysOfRisk: 0,
          monthsOfRisk: 0,
      }
  );
  return standardRating;
}

// sanitize_price_model_output_for_consumer_tax
export function sanitize_price_model_output_for_consumer_pretax(modelOutput: IPricingModelOutputs): IPricingModelSanitizedPreTax {
  return <any> modelOutput.preTaxGWP;
}


export function sanitize_price_model_output_for_consumer_tax(modelOutput: IPricingModelOutputs): IPricingModelSanitizedPostTax {
  const chargeInfo = modelOutput.charge;
  const output: any = {
    total: {
      tax: {}
    }
  };
  Object.keys(chargeInfo).filter((key: string) => key != "total").forEach((key: string) => {
    output[key] = {};
    const tax = chargeInfo[key].tax;
    output[key].tax = tax;
    const parties = chargeInfo[key].party;
    let partySum = 0;
    Object.keys(parties).forEach((party) => {
      const strSum = parties[party];
      const numSum = parseFloat(strSum);
      partySum += numSum;
    });
    const partySumString = partySum.toFixed(2);
    output[key].premium = partySumString;
  });
  output.total.tax = chargeInfo.total.tax;
  output.total.premium = 0;
  Object.keys(chargeInfo.total.party).forEach((partyName: string) => {
    const strSum = chargeInfo.total.party[partyName];
    const numSum = parseFloat(strSum);
    output.total.premium += numSum;
  });
  output.total.premium = output.total.premium.toFixed(2);
  return output;
}

// change for bespoke
export async function eval_quote_model_refund(quoteId: string, daysOfRisk: number, monthsOfRisk: number): Promise<IPricingModelOutputs | IResponse>  {
    const quote: IQuotePolicy = await gcdsInstance.getOne(["QuotePolicy", quoteId]);
    const customer: ICustomer = await gcdsInstance.getOne(["Customer", quote.customerId]);
    const appName = quote.systemApplication;
    const modelName = quote.PRICE_ALGORITHM_MODEL_NAME;
    const modelVersion = quote.PRICE_ALGORITHM_MODEL_VERSION;
    const systemVersion = quote.PRICE_ALGORITHM_SYSTEM_VERSION;
    // if (systemVersion == 1)
    return eval_application_model(appName, modelName, modelVersion,
        {
            Customer: customer,
            QuotePolicy: quote,
            daysOfRisk: daysOfRisk,
            monthsOfRisk: monthsOfRisk,
            isManualRating: 0
        }
    );
}

export function compute_final_revenue(modelOutput: IPricingModelOutputs) {
  // modelOutput.charge.total

  /*
        "total": {
            "tax": {
                "NRT": "8.55",
                "GST": "33.54",
                "StampDuty": "1.65"
            },
            "party": {
                "Imalia": "99.16",
                "DX-Evolution": "8.39",
                "Stripe": "6.79",
                "JLT": "7.13",
                "Neon": "195.65",
                "MS-Amlin": "9.73"
            }
        },

  */
 console.log("modelOutputmodelOutputmodelOutput", modelOutput);
  let taxTotal: number = 0;
  let partyTotal: number = 0;
  Object.keys(modelOutput.charge.total.tax).forEach((taxName: string) => {
    const taxStr: string = modelOutput.charge.total.tax[taxName];
    taxTotal += parseFloat(taxStr);
  });
  Object.keys(modelOutput.charge.total.party).forEach((taxName: string) => {
    const taxStr: string = modelOutput.charge.total.party[taxName];
    partyTotal += parseFloat(taxStr);
  });
  return (partyTotal + taxTotal).toFixed(2);
}

export async function getLiveModelDetailsForApplication (app: IApplication): Promise<IApplicationPricingModelDetails> {
  if (
    !app.hasOwnProperty("currentPricingEngineModelName") ||
    !app.hasOwnProperty("currentPricingEngineVersion") ||
    !app.hasOwnProperty("currentPricingEngineSystemVersion")
    ) {
    throw {error: true, message: `Cannot find pricing model version information for application: ${app.name}`, code: 12299922};
  }
  return {
    modelName: app.currentPricingEngineModelName,
    modelVersion: app.currentPricingEngineVersion,
    systemVersion: app.currentPricingEngineSystemVersion
  };
}

export function getPricingModelDetailsForQuote(quotePolicy: IQuotePolicy): IApplicationPricingModelDetails {
  return {
    modelName: quotePolicy.PRICE_ALGORITHM_MODEL_NAME,
    modelVersion: quotePolicy.PRICE_ALGORITHM_MODEL_VERSION,
    systemVersion: quotePolicy.PRICE_ALGORITHM_SYSTEM_VERSION
  };
}


export function downloadModelFile(fileName: string, applicationName: string) {
    // const ref = bucket.
    return fetchFileFromPath(`/pricingModels/${applicationName}/${fileName}`);
}