

const error = {
nil : new Error("#NULL!"),
div0 : new Error("#DIV/0!"),
value : new Error("#VALUE?"),
ref : new Error("#REF!"),
name : new Error("#NAME?"),
num : new Error("#NUM!"),
na : new Error("#N/A"),
error : new Error("#ERROR!"),
data : new Error("#GETTING_DATA"),
};

const parseNumber = function(string: string) {
    if (string === undefined || string === "") {
      return error.value;
    }
    if (!isNaN(<any> string)) {
      return parseFloat(string);
    }
    return error.value;
  };

  const anyIsError = function(number: number, digits: string) {
    let n = arguments.length;
    while (n--) {
      if (arguments[n] instanceof Error) {
        return true;
      }
    }
    return false;
  };

export const ROUND = function(number: any, digits: any) {
    number = parseNumber(number) || 0;
    // console.log(Math.round(number * Math.pow(10, digits)) / Math.pow(10, digits));
    /*if (anyIsError(number, digits)) {
      return error.value;
    }*/
    let result;
    if (digits) {
      digits = parseNumber(digits);
      result = Math.round(parseFloat((number * Math.pow(10, digits)).toFixed(digits))) / Math.pow(10, digits);
      // result = Math.round(number * Math.pow(10, digits)) / Math.pow(10, digits);
    }
    else {

      result = Math.round(number);
    }
    return result;
  };

  /*
ROUND(C7*C2+C3,)
  */