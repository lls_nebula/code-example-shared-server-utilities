const error = {
    na: new Error("#N/A")
};

export const MATCH = function(lookupValue: any, lookupArray: any, matchType: any) {
    // in match
    if (!lookupValue && !lookupArray) {
      return error.na;
    }
    if (arguments.length === 2) {
      matchType = 1;
    }

    if (arguments.length == 3) {
        if (arguments[2] === undefined) {
            matchType = 1;
        }
    }

    if (!(lookupArray instanceof Array)) {
      return error.na;
    }

    if (matchType !== -1 && matchType !== 0 && matchType !== 1) {
      return error.na;
    }
    let index;
    let indexValue;
    for (let idx = 0; idx < lookupArray.length; idx++) {
      if (matchType === 1) {
        if (lookupArray[idx] === lookupValue) {
          return idx + 1;
        } else if (lookupArray[idx] < lookupValue) {
          if (!indexValue) {
            index = idx + 1;
            indexValue = lookupArray[idx];
          } else if (lookupArray[idx] > indexValue) {

            index = idx + 1;
            indexValue = lookupArray[idx];

          }
        }
      } else if (matchType === 0) {
        if (typeof lookupValue === "string") {
          lookupValue = lookupValue.replace(/\?/g, ".");
          if (lookupArray[idx].toLowerCase().match(lookupValue.toLowerCase())) {
            return idx + 1;
          }
        } else {
          if (lookupArray[idx] === lookupValue) {
            return idx + 1;
          }
        }
      } else if (matchType === -1) {
        if (lookupArray[idx] === lookupValue) {
          return idx + 1;
        } else if (lookupArray[idx] > lookupValue) {
          if (!indexValue) {
            index = idx + 1;
            indexValue = lookupArray[idx];
          } else if (lookupArray[idx] < indexValue) {
            index = idx + 1;
            indexValue = lookupArray[idx];
          }
        }
      }
    }
   return index ? index + 1 : error.na;
};