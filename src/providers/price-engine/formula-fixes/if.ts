export const IF = function(test: any, then_value: any, otherwise_value: any) {
    return test ? then_value : otherwise_value;
};