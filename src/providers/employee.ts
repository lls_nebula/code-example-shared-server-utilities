import { ICustomer, IResponse, IContact, IEmployee } from "@vaultspace/neon-shared-models";
import { gcdsInstance } from "../gcds-instance";
import { IGDSKey } from "@vaultspace/gdatastore-v1";

export const employeeKind = "Employee";

export async function fetchEmployee(employeeId: string): Promise<IEmployee> {
    const key: IGDSKey = { kind: employeeKind, name: employeeId };
    return gcdsInstance.getOne(key);
}

export async function patchEmployee(employeeId: string, data: IEmployee): Promise<any> {
    const key: IGDSKey = { kind: employeeKind, name: employeeId };
    return await gcdsInstance.updateOne(key, data);
}