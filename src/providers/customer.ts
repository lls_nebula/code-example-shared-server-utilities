import { ICustomer, IResponse, IContact, EConsumerType, EConsumerStatus } from "@vaultspace/neon-shared-models";
import { gcdsInstance } from "../gcds-instance";
import { IGDSKey } from "@vaultspace/gdatastore-v1";
import { v4 as uuid } from "uuid";

export const customerKind = "Customer";
export const contactKind = "Contact";

export async function initDefaultCustomer(): Promise<ICustomer> {
    const defaultCustomer: ICustomer = {
        id: uuid(),
        type: EConsumerType.Consumer,
        exactEntityType: null,
        status: EConsumerStatus.Inactive,
        createdAt: new Date().getTime()
    };

    return defaultCustomer;
}

export async function fetchCustomer(customerId: string): Promise<ICustomer> {
    const key: IGDSKey = { kind: customerKind, name: customerId };
    return gcdsInstance.getOne(key);
}

export async function fetchContact(contactId: string): Promise<IContact> {
    const key: IGDSKey = { kind: contactKind, name: contactId };
    return gcdsInstance.getOne(key);
}

export async function patchCustomer(customerId: string, data: ICustomer): Promise<any> {
    const key: IGDSKey = { kind: customerKind, name: customerId };
    return await gcdsInstance.updateOne(key, data);
}