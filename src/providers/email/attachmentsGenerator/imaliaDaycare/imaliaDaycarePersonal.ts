import { IBufferAttachment } from "@vaultspace/emailer";
import { documentsBuilder } from "../../../pdf-storager";
import { IEmailContentBuilderArguments } from "../../../../models";
import { cocBuilder } from "../../../../util/applicationName/coc-builder";
import { ICreateCoCArguments, ICreateFdcScheduleArguments } from "@vaultspace/pdf/dist/src/models";
import { fdcScheduleBuilder } from "../../../../util/applicationName/fdcSchedule-builder";
import { firestore } from "@vaultspace/firebase-server";
import { IQuotePolicy } from "@vaultspace/neon-shared-models";
import { gcdsInstance } from "@vaultspace/emailer/dist/src/util/gcdatastore-service";
import { fetchFilesFromDirectory } from "../../../storage";
import {
    folderName,
    staticFolderName,
    personalFolderName,
        } from "../../../../configs/pdf_storager_config"; // ../../configs/pdf_storager_config
// function for you code that does not generate the pdf it assumes it is already there
// function which generates

/*

    await firestore.collection("QuotePolicy").doc(quoteId).set({storedDocuments: links}, {merge: true});

    const attachments = await Promise.all([
        fetchFilesFromDirectory(`${folderName}/${quote.systemApplication}/${staticFolderName}`),
        fetchFilesFromDirectory(`${folderName}/${quote.systemApplication}/${personalFolderName}/customers/${quote.customerId}/quotes/${quoteId}`),
    ]);

    return [...attachments[0], ...attachments[1]];

*/

export async function fetchAttachmentsForPolicy(policy: IQuotePolicy) {
    const attachments = await Promise.all([
        fetchFilesFromDirectory(`${folderName}/${policy.systemApplication}/${staticFolderName}`),
        fetchFilesFromDirectory(`${folderName}/${policy.systemApplication}/${personalFolderName}/customers/${policy.customerId}/quotes/${policy._name}`),
    ]);
    return [...attachments[0], ...attachments[1]];
}


export const generateAttachmentsForFdcPersonal = async (args: IEmailContentBuilderArguments): Promise<IBufferAttachment[]> => {
    try {
        let attachments: IBufferAttachment[];

        if (args.emailType == "firstInvoiceEmail") {
            const coc: ICreateCoCArguments = cocBuilder(args.Customer, args.QuotePolicy);
            const fdcSchedule: ICreateFdcScheduleArguments = fdcScheduleBuilder(args.Customer, args.QuotePolicy, args.Contact);

            console.log(`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`);
            console.log(coc);
            console.log(fdcSchedule);
            console.log(`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`);

            attachments = await documentsBuilder.attachForapplicationName(
                args.QuotePolicy.quoteId,
                {
                    coc: coc,
                    schedule: fdcSchedule,
                }
            );

            const now = Date.now();
            const quotePolicyId = args.QuotePolicy.quoteId;

            await firestore.collection("QuotePolicy").doc(quotePolicyId).set({policyIssuanceDate: now}, {merge: true});

        }

        return attachments;

    } catch (err) {
        throw err;
    }
};
