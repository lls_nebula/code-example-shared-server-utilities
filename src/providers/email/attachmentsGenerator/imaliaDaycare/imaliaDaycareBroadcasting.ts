import { IBufferAttachment } from "@vaultspace/emailer";
/*import { documentsBuilder } from "../../../pdf-storager";*/
import { IEmailContentBuilderArguments } from "../../../../models";

export const generateAttachmentsForFdcBroadcasting = async (args: IEmailContentBuilderArguments): Promise<IBufferAttachment[]> => {
    try {
        const attachments: IBufferAttachment[] = [];

        if (args.emailType == "paymentDisputeNotification") {
            if (args.options.chargeDispute) {
                attachments.push({
                    filename: "chargeDispute.json",
                    content: Buffer.from(JSON.stringify(args.options.chargeDispute))
                });
            }
        } else if (args.emailType == "refundFailureNotification") {
            if (args.options.failedRefund) {
                attachments.push({
                    filename: "failedRefund.json",
                    content: Buffer.from(JSON.stringify(args.options.failedRefund))
                });
            }
        } else if (args.emailType == "paymentFailureNotification") {

        } else if (args.emailType == "referralNotification") {

        }

        return attachments;

    } catch (err) {
        throw err;
    }
};