import { ISendMail } from "@vaultspace/emailer";
import { availableAppName, IEmailContentBuilderArguments, IEmailContent, availableEmailType } from "../../models";
import { gcdsInstance, TGFSKey } from "@vaultspace/gdatastore-v1";
import { applicationKind } from "../../configs/kinds";
import { buildContentForFdcPersonalEmail, buildContentForFdcBroadcastingEmail } from "./contentBuilder";
import { generateAttachmentsForFdcPersonal, generateAttachmentsForFdcBroadcasting, fetchAttachmentsForPolicy } from "./attachmentsGenerator";
import { getEmailsForBroadcastingByApp } from "../../util/email-broadcasting";

const fdcPersonalEmailContentBuilder = async (
    args: IEmailContentBuilderArguments,
    cc: Array<string> = [],
    bcc: Array<string> = [],
    to: Array<string> = []
): Promise<ISendMail|null> => {
    const key: TGFSKey = [applicationKind, args.Application.name, "EmailContent", args.emailType];
    const template = await gcdsInstance.getOne(key) as IEmailContent;

    console.log(`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`);
    console.log(key);
    console.log(`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`);

    if (!template || !template.subject || !template.body) {
        console.error("MISSING EMAIL KEY", key, template);
        return null;
    }

    const content: ISendMail = {
        to: [args.Contact.email],
        subject: template.subject,
        body: buildContentForFdcPersonalEmail(template.body, args),
        attachments: args.emailType == "firstInvoiceEmail" ? await fetchAttachmentsForPolicy(args.QuotePolicy) : [],
        cc,
        bcc,
    };

    return content;
};

const fdcBroadcastingEmailContentBuilder = async (args: IEmailContentBuilderArguments): Promise<ISendMail|null> => {
    const key: TGFSKey = [applicationKind, args.Application.name, "EmailContent", args.emailType];
    const template = await gcdsInstance.getOne(key) as IEmailContent;
    const bcc: string[] = await getEmailsForBroadcastingByApp(args.Application.name);

    if (!template || !template.subject || !template.body) { return null; }

    const emailDontent: IEmailContent = buildContentForFdcBroadcastingEmail(template, args);

    const content: ISendMail = {
        to: [],
        subject: emailDontent.subject,
        body: emailDontent.body,
        attachments:  await generateAttachmentsForFdcBroadcasting(args),
        cc: [],
        bcc: bcc,
    };

    return content;
};


const fintechEmailContentBuilder = async (args: IEmailContentBuilderArguments, cc: Array<string> = [], bcc: Array<string> = [], to: Array<string> = []): Promise<ISendMail|null> => {
    return null;
};

export const emailContentBuilder = async (args: IEmailContentBuilderArguments, cc: Array<string> = [], bcc: Array<string> = [], to: Array<string> = []): Promise<ISendMail|null> => {
    try {
        switch (args.Application.name as availableAppName) {
            case("applicationName"):
                if (args.notificationType === "personal") {
                    return fdcPersonalEmailContentBuilder(args, cc, bcc);
                }

                if (args.notificationType === "broadcasting") {
                    return fdcBroadcastingEmailContentBuilder(args);
                }

                break;

            case("imaliaFintech"):
                return fintechEmailContentBuilder(args, cc, bcc);

                break; // it is useless but I left it here for consistency

            default: return null;
        }

    } catch (err) { throw err; }
};
