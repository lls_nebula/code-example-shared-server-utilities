import { VSMailer } from "@vaultspace/emailer";
import { mailerResponseType } from "@vaultspace/emailer/src/models";
import { emailContentBuilder } from "./emailContentBuilder";
import { IEmailContentBuilderArguments } from "../../models";

export async function buildEmailAndSendToUser(
    from: string,
    entities: IEmailContentBuilderArguments,
    cc: Array<string> = [],
    bcc: Array<string> = [],
    to: Array<string> = [],
): Promise<mailerResponseType[]> {
            const mailer = new VSMailer(from, entities.Application.name);
            const content = await emailContentBuilder(entities, cc, bcc, to);

            console.log(`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`);
            console.log(entities, cc, bcc, to);
            console.log(`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`);

            console.log(`~~~~~~~~~~~~~~~~~~~~~~~~++++~~~~~~~~~~~~~~~~~~~~~~~~`);
            console.log(`~~~~~~~~~~~~~~~~~~~~~~~~++++~~~~~~~~~~~~~~~~~~~~~~~~`);
            console.error(`the size of the subject is ${Buffer.byteLength(content.subject)} bytes`);
            console.error(`the size of the body is ${Buffer.byteLength(content.body)} bytes`);
            if (content.attachments) {
                content.attachments.forEach(attachment => {
                    console.error(`[${attachment.filename}] the size of the attachment is ${Buffer.byteLength(attachment.content)} bytes`);
                });
            }
            console.log(`~~~~~~~~~~~~~~~~~~~~~~~~++++~~~~~~~~~~~~~~~~~~~~~~~~`);
            console.log(`~~~~~~~~~~~~~~~~~~~~~~~~++++~~~~~~~~~~~~~~~~~~~~~~~~`);

            return mailer.sendMail(content);
}
