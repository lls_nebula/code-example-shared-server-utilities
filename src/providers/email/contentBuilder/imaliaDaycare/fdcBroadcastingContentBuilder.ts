import { IEmailContentBuilderArguments } from "../../../../models";
import { replaceFields } from "../../../../util/replaceFields";
import { IEmailContentOptionsBody, IEmailContentOptionsSubject, IEmailContent } from "../../../../models/index";

export const buildContentForFdcBroadcastingEmail = (emailTemplate: IEmailContent, args: IEmailContentBuilderArguments): IEmailContent => {

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "paymentDisputeNotification") {
        return replaceFields(emailTemplate, {
            body: {
                QuotePolicy: {
                    quoteId: args.QuotePolicy.quoteId
                }
            },
            subject: {
                quoteId: args.QuotePolicy.quoteId
            }
        });

//         return `Hi,
// <br><br>
// A payment has been disputed by the customer. The customer should be contacted ASAP to remediate the situation.
// <br><br>
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "refundFailureNotification") {
        return replaceFields(emailTemplate, {
            body: {
                QuotePolicy: {
                    quoteId: args.QuotePolicy.quoteId
                }
            },
            subject: {
                quoteId: args.QuotePolicy.quoteId
            }
        });

//         return `Hi,
// <br><br>
// Stripe was unable to process the requested refund. The policyholder has been notified and should be contacted ASAP.
// <br><br>
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "paymentFailureNotification") {
        return replaceFields(emailTemplate, {
            body: {
                QuotePolicy: {
                    quoteId: args.QuotePolicy.quoteId
                }
            },
            subject: {
                quoteId: args.QuotePolicy.quoteId
            }
        });

//         return `Hi,
// <br><br>
// Stripe was unable to process a payment for this policy. The policyholder has been sent an automated email with a link to update their payment details.
// <br><br>
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "referralNotification") {
        return replaceFields(emailTemplate, {
            body: {
                QuotePolicy: {
                    quoteId: args.QuotePolicy.quoteId
                }
            },
            subject: {
                quoteId: args.QuotePolicy.quoteId
            }
        });

//         return `Hi,
// <br><br>
// This quote has been referred and requires your action. Please review the quote ASAP.
// <br><br>
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "paymentDiscrepancyNotification") {
        return replaceFields(emailTemplate, {
            body: {
                QuotePolicy: {
                    quoteId: args.QuotePolicy.quoteId
                }
            },
            subject: {
                quoteId: args.QuotePolicy.quoteId
            }
        });

//         return `Hi,
// <br><br>
// A payment discrepancy is where the amount received from the customer is different from the amount due.
// <br><br>
// The issue should be investigated immediately.
// <br><br>
    }
};
