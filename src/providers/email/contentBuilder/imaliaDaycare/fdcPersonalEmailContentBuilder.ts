import { IEmailContentBuilderArguments } from "../../../../models";
import { replaceFieldsBody } from "../../../../util/replaceFields";
import { formatDate_1, dateDiffInDays, formatDate_2 } from "../../../../util/date-time";




export const buildContentForFdcPersonalEmail = (bodyTemplate: string, args: IEmailContentBuilderArguments): string => {

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "inviteEmail") {
        return replaceFieldsBody(bodyTemplate, {
            Contact: {
                firstName: args.Contact.name,
            },
            ApplicationFunnel: {
                customer_url: args.ApplicationFunnel.customerEndPointUrl,
            },
            Customer: {
                firebaseUserId: args.Customer.firebaseUserId,
            }
        },
        {
            password: args.options.password,
        });
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "firstInvoiceEmail") {
        const startDate = (args.QuotePolicy.approvedDate || args.QuotePolicy.referredWhen || args.QuotePolicy.startDate);

        return replaceFieldsBody(bodyTemplate, {
            Contact: {
                firstName: args.Contact.name,
            },
            ApplicationFunnel: {
                customer_url: args.ApplicationFunnel.customerEndPointUrl,
            },
            Customer: {
                firebaseUserId: args.Customer.firebaseUserId,
            },
            QuotePolicy: {
                startDate: formatDate_2(new Date(startDate))
            }
        },
        {
            invoice_url: args.options.invoice_url
        });
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "invoiceEmail") {

        return replaceFieldsBody(bodyTemplate, {
            Contact: {
                firstName: args.Contact.name,
            },
            ApplicationFunnel: {
                customer_url: args.ApplicationFunnel.customerEndPointUrl,
            },
            Customer: {
                firebaseUserId: args.Customer.firebaseUserId,
            }
        },
        {
            invoice_url: args.options.invoice_url
        });
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "finalInvoiceEmail") {
        return replaceFieldsBody(bodyTemplate, {
            Contact: {
                firstName: args.Contact.name,
            },
            QuotePolicy: {
                endDate: formatDate_1(new Date(args.QuotePolicy.endDate))
            }
        },
        {
            invoice_url: args.options.invoice_url,
            policyExpiryDate: formatDate_1(new Date(args.QuotePolicy.endDate))
        });
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "refundEmail") {
        return replaceFieldsBody(bodyTemplate, {
            Contact: {
                firstName: args.Contact.name,
            },
            ApplicationFunnel: {
                customer_url: args.ApplicationFunnel.customerEndPointUrl,
            },
            Customer: {
                firebaseUserId: args.Customer.firebaseUserId,
            }
        },
        {
            daysLeft: dateDiffInDays(new Date(), new Date(args.QuotePolicy.endDate)),
            amountRefound: args.options.refundAmount || "0.00", // @Vlad use this in stripe
        });
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "refundEmail2") {
        return replaceFieldsBody(bodyTemplate, {
            Contact: {
                firstName: args.Contact.name,
            }
        },
        {
            amountRefound: Array.isArray(args.options.failedRefund) ? args.options.failedRefund[3].refundAmount : "0.00", // @Vlad use this in stripe
            invoice_url: args.options.invoice_url
        });
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "failedRefundEmail") {
        return replaceFieldsBody(bodyTemplate, {
            Contact: {
                firstName: args.Contact.name,
            },
            ApplicationFunnel: {
                customer_url: args.ApplicationFunnel.customerEndPointUrl,
            },
            Customer: {
                firebaseUserId: args.Customer.firebaseUserId,
            }
        },
        {
            amountRefound: Array.isArray(args.options.failedRefund) ? args.options.failedRefund[3].find((it: any) => { if (it.attemptedRefund == true) return true; }).refundAmount : "0.00" // @Vlad use this in stripe
        });
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "referralEmail") {
        const startDate = (args.QuotePolicy.approvedDate || args.QuotePolicy.referredWhen || args.QuotePolicy.startDate || Date.now());

        return replaceFieldsBody(bodyTemplate, {
            Contact: {
                firstName: args.Contact.name,
            },
            ApplicationFunnel: {
                customer_url: args.ApplicationFunnel.customerEndPointUrl,
            },
            Customer: {
                firebaseUserId: args.Customer.firebaseUserId,
            },
            QuotePolicy: {
                startDate: formatDate_2(new Date(startDate))
            }
        },
        {
            // invoice_url: args.options.invoice_url
        });
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "referralEmail2") {
        return replaceFieldsBody(bodyTemplate, {
            Contact: {
                firstName: args.Contact.name,
            },
        });
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "failedPaymentEmailWithManyStripeRetries") {
        const startDate = (args.QuotePolicy.approvedDate || args.QuotePolicy.referredWhen || args.QuotePolicy.startDate);

        return replaceFieldsBody(bodyTemplate, {
            Contact: {
                firstName: args.Contact.name,
            },
            ApplicationFunnel: {
                customer_url: args.ApplicationFunnel.customerEndPointUrl,
            },
            Customer: {
                firebaseUserId: args.Customer.firebaseUserId,
            },
            QuotePolicy: {
                markedForCancellationDate: formatDate_1(new Date(args.QuotePolicy.markedForCancellationDate)),
                startDate: formatDate_2(new Date(startDate))
            }
        }, {
            invoice_url: args.options.invoice_url
        });
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "failedPaymentEmailWithLittleStripeRetries") {
        const startDate = (args.QuotePolicy.approvedDate || args.QuotePolicy.referredWhen || args.QuotePolicy.startDate);

        return replaceFieldsBody(bodyTemplate, {
            Contact: {
                firstName: args.Contact.name,
            },
            QuotePolicy: {
                markedForCancellationDate: formatDate_1(new Date(args.QuotePolicy.markedForCancellationDate)),
                startDate: formatDate_2(new Date(startDate)),
            }
        }, {
            invoice_url: args.options.invoice_url
        });
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "paymentCancellationEmail") {
        return replaceFieldsBody(bodyTemplate, {
            Contact: {
                firstName: args.Contact.name,
            },
        },
        {
            policyExpiryDate: formatDate_1(new Date(args.QuotePolicy.endDate))
        });
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (args.emailType == "paymentDisputeEmail") {
        return replaceFieldsBody(bodyTemplate, {
            Contact: {
                firstName: args.Contact.name,
            }
        });
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
};
