import { IApplicationProduct, EProductType } from "@vaultspace/neon-shared-models";

export const applicationNameDebtCollectorFeeAndTelephoneLegalAdviceProduct: IApplicationProduct = {
  productType: EProductType.addOn,
  productName: "debtCollectorFeeAndTelephoneLegalAdvice",
  revenueShare: {
      imalia: 0.9625, // * imaliaShareReduction? // need to store this! // share 0.14 0.13 // validation provide not allowed 0.15
      jlt: 0,
      dxEvo: 0.0225,
      stripe: 0.015,
      neon: 0, // steals from neons share if approve
      nrt: 0,
      msAmlin: 0
  }

};
