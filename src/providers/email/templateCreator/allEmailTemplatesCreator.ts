import { createImaliaDaycareEmailContent } from "./imaliaDaycare/fdcEmailTemplatesCreator";
import { createImaliaDaycareNotificationContent } from "./imaliaDaycare/fdcBroadcastingTemplatesCreator";

export function createAllEmailContent() {
    return Promise.all([
        createImaliaDaycareNotificationContent(),
        createImaliaDaycareEmailContent()
    ]);
}
