import { createEmailContentEntity } from "../../../../util";

export function createImaliaDaycareNotificationContent() {
    return Promise.all([
        createContentFor_ImaliaDaycare_ReferralNotification(),
        createContentFor_ImaliaDaycare_RefundFailureNotification(),
        createContentFor_ImaliaDaycare_PaymentFailureNotification(),
        createContentFor_ImaliaDaycare_PaymentDisputeNotification(),
        createContentFor_ImaliaDaycare_PaymentDiscrepancyNotification()
    ]);
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// https://admin-imalia-neon-test.web.app/#/quotes-panel/quotes/f26247f1-d0c8-40f9-b253-c51d3be3ef17
function createContentFor_ImaliaDaycare_ReferralNotification() {
    return createEmailContentEntity("imaliaDaycare", "referralNotification", {
        subject: "Imalia FDC - referral - :::quoteId:::", // @lee [dynamic data]
        body: `Hi,
<br><br>
This quote has been referred and requires your action. Please review the quote ASAP.
<br><br>
<a href="https://admin-imalia-neon-test.web.app/#/quotes-panel/quotes/:::QuotePolicy.quoteId:::">https://admin-imalia-neon-test.web.app/#/quotes-panel/quotes/:::QuotePolicy.quoteId:::</a>`, // @lee [dynamic data]
    });
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function createContentFor_ImaliaDaycare_RefundFailureNotification() {
    return createEmailContentEntity("imaliaDaycare", "refundFailureNotification", {
        subject: "Imalia FDC - refund failed - :::quoteId:::", // @lee [dynamic data]
        body: `Hi,
<br><br>
Stripe was unable to process the requested refund. The policyholder has been notified and should be contacted ASAP.
<br><br>
<a href="https://admin-imalia-neon-test.web.app/#/quotes-panel/quotes/:::QuotePolicy.quoteId:::">https://admin-imalia-neon-test.web.app/#/quotes-panel/quotes/:::QuotePolicy.quoteId:::</a>`, // @lee [dynamic data]
    });
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function createContentFor_ImaliaDaycare_PaymentFailureNotification() {
    return createEmailContentEntity("imaliaDaycare", "paymentFailureNotification", {
        subject: "Imalia FDC - payment failed - :::quoteId:::", // @lee [dynamic data]
        body: `Hi,
<br><br>
Stripe was unable to process a payment for this policy. The policyholder has been sent an automated email with a link to update their payment details.
<br><br>
<a href="https://admin-imalia-neon-test.web.app/#/quotes-panel/quotes/:::QuotePolicy.quoteId:::">https://admin-imalia-neon-test.web.app/#/quotes-panel/quotes/:::QuotePolicy.quoteId:::</a>`, // @lee [dynamic data]
    });
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function createContentFor_ImaliaDaycare_PaymentDisputeNotification() {
    return createEmailContentEntity("imaliaDaycare", "paymentDisputeNotification", {
        subject: "Imalia FDC - payment disputed - :::quoteId:::", // @lee [dynamic data]
        body: `Hi,
<br><br>
A payment has been disputed by the customer. The customer should be contacted ASAP to remediate the situation.
<br><br>
<a href="https://admin-imalia-neon-test.web.app/#/quotes-panel/quotes/:::QuotePolicy.quoteId:::">https://admin-imalia-neon-test.web.app/#/quotes-panel/quotes/:::QuotePolicy.quoteId:::</a>`, // @lee [dynamic data]
    });
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function createContentFor_ImaliaDaycare_PaymentDiscrepancyNotification() {
    return createEmailContentEntity("imaliaDaycare", "paymentDiscrepancyNotification", {
        subject: "Imalia FDC - payment discrepancy - :::quoteId:::", // @lee [dynamic data]
        body: `Hi,
<br><br>
A payment discrepancy is where the amount received from the customer is different from the amount due.
<br><br>
The issue should be investigated immediately.
<br><br>
<a href="https://admin-imalia-neon-test.web.app/#/quotes-panel/quotes/:::QuotePolicy.quoteId:::">https://admin-imalia-neon-test.web.app/#/quotes-panel/quotes/:::QuotePolicy.quoteId:::</a>`, // @lee [dynamic data]
    });
}