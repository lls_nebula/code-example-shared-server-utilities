import { createEmailContentEntity } from "../../../../util";
import { bottomImage } from "./bottom-image";

export function createImaliaDaycareEmailContent() {
    return Promise.all([
        createContentFor_ImaliaDaycare_InviteEmail(),
        createContentFor_ImaliaDaycare_FirstInvoiceEmail(),
        createContentFor_ImaliaDaycare_InvoiceEmail(),
        createContentFor_ImaliaDaycare_FinalInvoiceEmail(),
        createContentFor_ImaliaDaycare_FailedPaymentEmailWithManyStripeRetries(),
        createContentFor_ImaliaDaycare_FailedPaymentEmailWithLittleStripeRetries(),
        createContentFor_ImaliaDaycare_PaymentCancellationEmail(),
        createContentFor_ImaliaDaycare_PaymentDisputeEmail(),
        createContentFor_ImaliaDaycare_RefundEmail(),
        createContentFor_ImaliaDaycare_RefundEmail2(),
        createContentFor_ImaliaDaycare_FailedRefundEmail(),
        createContentFor_ImaliaDaycare_ReferralEmail(),
        createContentFor_ImaliaDaycare_ReferralEmail2(),
    ]);
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// hesitate
function createContentFor_ImaliaDaycare_InviteEmail() {
    return createEmailContentEntity("imaliaDaycare", "inviteEmail", {
        subject: "Imalia FDC: profile created!",
        body: `:::Contact.firstName:::!
<br><br>
Welcome to the Wolfpack!
<br><br>
We have created a profile for you.
<br><br>
Your password is: :::Options.password:::
<br><br>
Please keep this password safe; you will need it to make changes to your policy in the future.
<br><br>
If you require any help or support, do not hesitate to contact us at <a href="mailto:help@imalia.com.au">help@imalia.com.au</a>. Please note that if you reply to this email (noreply@…) it will be deleted.
<br><br>
${bottomImage}`
    });
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/*
Hi Luke!
A payment has been successfully made. You can view your invoice here: https://pay.stripe.com/invoice/invst_tdzYLnWbZu34xD8dxksGaUKVxG
We have attached your policy documentation to this email - please read and check the information enclosed.
*/
function createContentFor_ImaliaDaycare_FirstInvoiceEmail() {
    return createEmailContentEntity("imaliaDaycare", "firstInvoiceEmail", {
        subject: "Imalia FDC: Your Insurance",
        body: `Hi :::Contact.firstName:::!
<br><br>
A payment has been successfully made. You can view your invoice here: <a href=":::Options.invoice_url:::">:::Options.invoice_url:::</a>
<br><br>
We have attached your policy documentation to this email, please read and check the information enclosed.
<br><br>
If you require any help or support, do not hesitate to contact us at <a href="mailto:help@imalia.com.au">help@imalia.com.au</a>. Please note that if you reply to this email (noreply@…) it will be deleted.
<br><br>
${bottomImage}`
    });
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function createContentFor_ImaliaDaycare_InvoiceEmail() {
    return createEmailContentEntity("imaliaDaycare", "invoiceEmail", {
        subject: "Imalia FDC: payment received",
        body: `:::Contact.firstName:::!
<br><br>
This is an automated email to confirm that we have received your latest payment. A copy of the invoice is available here: <a href=":::Options.invoice_url:::">:::Options.invoice_url:::</a>.
<br><br>
If you require any help or support, do not hesitate to contact us at <a href="mailto:help@imalia.com.au">help@imalia.com.au</a>. Please note that if you reply to this email (noreply@…) it will be deleted.
<br><br>
${bottomImage}`
    });
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function createContentFor_ImaliaDaycare_FinalInvoiceEmail() {
    return createEmailContentEntity("imaliaDaycare", "finalInvoiceEmail", {
        subject: "Imalia FDC: final payment received",
        body: `:::Contact.firstName:::!
<br><br>
Your final payment has been received. Don’t forget that your insurance expires on :::Options.policyExpiryDate:::
<br><br>
We would love to insure you again next year.
<br><br>
We will be in touch to check whether you would like to renew your policy. However, please get in touch if you would like to arrange this sooner.
<br><br>
A copy of the invoice is available here: <a href=":::Options.invoice_url:::">:::Options.invoice_url:::</a>
<br><br>
If you require any help or support, do not hesitate to contact us at <a href="mailto:help@imalia.com.au">help@imalia.com.au</a>. Please note that if you reply to this email (noreply@…) it will be deleted.
<br><br>
${bottomImage}`, // @lee [dynamic data]
    });
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function createContentFor_ImaliaDaycare_FailedPaymentEmailWithManyStripeRetries() {
    // HARD CODED DATE!!!
    return createEmailContentEntity("imaliaDaycare", "failedPaymentEmailWithManyStripeRetries", {
        subject: "Imalia FDC: failed payment - RISK OF CANCELLATION",
        body: `Hi :::Contact.firstName:::,
<br><br>
Unfortunately we were still unable to collect your latest payment.
<br><br>
Have your card details changed?
<br><br>
If this is the case, you can provide updated details here: <a href=":::Options.invoice_url:::">:::Options.invoice_url:::</a>
<br><br>
You will need your password to access this link. This was sent to you by email on :::QuotePolicy.startDate::: titled, <i>“Imalia FDC: profile created”</i>.
<br><br>
<span style="border-bottom: 1px solid black;">Don’t forget, if we haven’t received your payment by :::QuotePolicy.markedForCancellationDate:::, your policy will be cancelled and you will no longer be insured.</span>
<br><br>
If you require any help or support, do not hesitate to contact us at <a href="mailto:help@imalia.com.au">help@imalia.com.au</a>. Please note that if you reply to this email (noreply@…) it will be deleted.
<br><br>
${bottomImage}`, // @lee [dynamic data]
    });
}
// We emailed previously to remind you on June 23rd.

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
function createContentFor_ImaliaDaycare_FailedPaymentEmailWithLittleStripeRetries() {
    return createEmailContentEntity("imaliaDaycare", "failedPaymentEmailWithLittleStripeRetries", {
        subject: "Imalia FDC: failed payment",
        body: `Hi :::Contact.firstName:::,
<br><br>
Unfortunately we were unable to collect your latest payment.
<br><br>
Have your card details changed?
<br><br>
If this is the case, you can provide updated details here: <a href=":::Options.invoice_url:::">:::Options.invoice_url:::</a>
<br><br>
You will need your password to access this link. This was sent to you by email on :::QuotePolicy.startDate::: titled, <i>“Imalia FDC: profile created”</i>.
<br><br>
Don’t forget, if we haven’t received your payment by :::QuotePolicy.markedForCancellationDate:::, your policy will be cancelled and you will no longer be insured.
<br><br>
If you require any help or support, do not hesitate to contact us at <a href="mailto:help@imalia.com.au">help@imalia.com.au</a>. Please note that if you reply to this email (noreply@…) it will be deleted.
<br><br>
${bottomImage}`,
    });
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function createContentFor_ImaliaDaycare_PaymentCancellationEmail() {
    return createEmailContentEntity("imaliaDaycare", "paymentCancellationEmail", {
        subject: "Imalia FDC: Insurance cancelled",
        body: `Hi :::Contact.firstName:::,
<br><br>
Your policy has been cancelled. Your insurance protection ends on :::Options.policyExpiryDate::: at 4pm.
<br><br>
If you require any help or support, do not hesitate to contact us at <a href="mailto:help@imalia.com.au">help@imalia.com.au</a>. Please note that if you reply to this email (noreply@…) it will be deleted.
<br><br>
${bottomImage}`, // @lee [dynamic data]
    });
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function createContentFor_ImaliaDaycare_PaymentDisputeEmail() {
    return createEmailContentEntity("imaliaDaycare", "paymentDisputeEmail", {
        subject: "Imalia FDC: payment dispute",
        body: `Hi :::Contact.firstName:::,
<br><br>
We have registered your payment dispute. We will investigate the matter and be in contact.
<br><br>
If you require any help or support, do not hesitate to contact us at <a href="mailto:help@imalia.com.au">help@imalia.com.au</a>. Please note that if you reply to this email (noreply@…) it will be deleted.
<br><br>
${bottomImage}`,
    });
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function createContentFor_ImaliaDaycare_RefundEmail() {
    return createEmailContentEntity("imaliaDaycare", "refundEmail", {
        subject: "Imalia FDC: your refund",
        body: `Hi :::Contact.firstName:::,
<br><br>
Your refund of $:::Options.amountRefound::: has been processed. You should expect to see the funds in your account within :::Options.daysLeft::: days.
<br><br>
If you require any help or support, do not hesitate to contact us at <a href="mailto:help@imalia.com.au">help@imalia.com.au</a>. Please note that if you reply to this email (noreply@…) it will be deleted.
<br><br>
${bottomImage}`, // @lee [dynamic data]
    });
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function createContentFor_ImaliaDaycare_RefundEmail2() {
    return createEmailContentEntity("imaliaDaycare", "refundEmail2", {
        subject: "Imalia FDC: your refund",
        body: `Hi :::Contact.firstName:::,
<br><br>
Your refund of $:::Options.amountRefound::: has been made by our payments processor. A copy of your receipt can be found here: <a href=":::Options.invoice_url:::">:::Options.invoice_url:::</a>
<br><br>
If you require any help or support, do not hesitate to contact us at <a href="mailto:help@imalia.com.au">help@imalia.com.au</a>. Please note that if you reply to this email (noreply@…) it will be deleted.
<br><br>
${bottomImage}`, // @lee [dynamic data]
    });
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function createContentFor_ImaliaDaycare_FailedRefundEmail() {
    return createEmailContentEntity("imaliaDaycare", "failedRefundEmail", {
        subject: "Imalia FDC: refund failed",
        body: `Hi :::Contact.firstName:::,
<br><br>
We were unable to complete your refund of $:::Options.amountRefound:::. Please contact us to arrange an alternate means of transaction.
<br><br>
If you require any help or support, do not hesitate to contact us at <a href="mailto:help@imalia.com.au">help@imalia.com.au</a>. Please note that if you reply to this email (noreply@…) it will be deleted.
<br><br>
${bottomImage}`, // @lee [dynamic data]
    });
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function createContentFor_ImaliaDaycare_ReferralEmail() {
    return createEmailContentEntity("imaliaDaycare", "referralEmail", {
        subject: "Imalia FDC: your quote",
        body: `Hi :::Contact.firstName:::,
<br><br>
Thank you for taking the time to provide your details.
<br><br>
Your information has been reviewed by the insurer, and your quote is now available online. Please see the link below for details <a href=":::ApplicationFunnel.customer_url:::/#/quotes/:::Customer.firebaseUserId:::">:::ApplicationFunnel.customer_url:::/#/quotes/:::Customer.firebaseUserId:::</a>.
<br><br>
You will need your password to access this link. This was sent to you by email on :::QuotePolicy.startDate::: titled, <i>“Imalia FDC: profile created”</i>
<br><br>
If you require any help or support, do not hesitate to contact us at <a href="mailto:help@imalia.com.au">help@imalia.com.au</a>. Please note that if you reply to this email (noreply@…) it will be deleted.
<br><br>
${bottomImage}`, // @lee [dynamic data]
    });
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function createContentFor_ImaliaDaycare_ReferralEmail2() {
    return createEmailContentEntity("imaliaDaycare", "referralEmail2", {
        subject: "Imalia FDC: your quote",
        body: `Hi :::Contact.firstName:::,
<br><br>
Thank you for taking the time to provide your details.
<br><br>
We will review your information and get back to you with any queries.
<br><br>
If you require any help or support, do not hesitate to contact us at <a href="mailto:help@imalia.com.au">help@imalia.com.au</a>. Please note that if you reply to this email (noreply@…) it will be deleted.
<br><br>
${bottomImage}`,
    });
}
