import { gcdsInstance } from "../gcds-instance";
import { EBinType, IEntityStats, IDateBin, IHistogramCollection, IStatsJobType2 } from "../models/index";
import { IGDSKey, IGDSFilter } from "@vaultspace/gdatastore-v1";
import { IStatsEntityCount, IStatsHistogram, IQuotePolicy, ICustomer, EApprovedStatus, IApplication } from "@vaultspace/neon-shared-models";
import { createJsonFileWithPath, fetchJsonFileFromPath } from "./storage";
import { fetchJobs, fetchKindQuery } from "../configs/stats";
import { addYearsToDate } from "../util/time-and-date";
import { firestore } from "@vaultspace/firebase-server";
import { addDaysToDate } from "../util/time-and-date";

import * as firebase from "firebase";
const batchSize: number = 100;

function daysInThisMonth(): number {
    const now = new Date();
    return new Date(now.getFullYear(), now.getMonth() + 1, 0).getDate();
}

function daysInMonth(month: number, year: number): number {
    const cloneDate: Date = new Date(year, month + 1, 0);
    const daysInMonth: number = cloneDate.getDate();

    return daysInMonth;
}

function incrementDateByBinUnit(date: Date, binType: EBinType) {
    const cloneDate: Date = new Date(date.toISOString());

    if (binType == EBinType.Day) {
        const currentDate = date.getDate();
        cloneDate.setDate(currentDate + 1);
        return cloneDate;
    } else if (binType == EBinType.Month) {
        cloneDate.setMonth(cloneDate.getMonth() + 1);
        return cloneDate;
    } else if (binType == EBinType.Week) {
        const currentDate: number = date.getDate();
        const currentMonth: number = date.getMonth();
        const currentYear: number = date.getFullYear();
        cloneDate.setDate(currentDate + 7);
        return cloneDate;
    }

    return null;
}
/*




export interface IHistogramCollection {
    [binType: string]: {
        [dateFieldName: string]: IDateBin[]
    };
}
*/


export interface IEntityStatsApplication {
    kind: string;
    count: { [queryName: string]: { [appName: string]: Set<string> } };
    histograms: IHistogramCollectionApplication;
    calculatedAt: Date | number;
}

export interface IHistogramCollectionApplication {
    [dateFieldName: string]: {
        [binType: string]: IDateBinApplication[]
    };
}
export interface IDateBinApplication {
    date: Date;
    count: { [applicationName: string]: Set<string> };
}


/*
export interface IEntityStatsOutput {
    kind: string;
    count: { [queryName: string]: number };
    histograms: IHistogramCollectionOutput;
}
*/

export interface IHistogramCollectionOutput {
    [queryName: string]: {
        [dateFieldName: string]: {
            [binType: string]: IDateBinOutput[]
        };
    };
}
export interface IDateBinOutput {
    date: Date;
    count: number;
}

function isValidDate(d: Date) {
    return d instanceof Date && !isNaN(<any>d);
}

export function localIGDSFilterComputer(filters: IGDSFilter[], entityToCheck: any): boolean {
    if (!filters.length) return true;
    // should be AND'd together
    let result: boolean = null;
    filters.some((filter: IGDSFilter) => {
        const fieldValue = entityToCheck[filter.propertyName];
        if (filter.comparator == "=") {
            if (fieldValue == filter.value) {
                result = true;
            }
            else {
                result = false;
                return true;
            }
        }
        else if (filter.comparator == "<") {
            console.log("<", fieldValue, filter.value);
            if (fieldValue < filter.value) {
                result = true;
            }
            else {
                result = false;
                return true;
            }
        }
        else if (filter.comparator == "<=") {
            if (fieldValue <= filter.value) {
                result = true;
            }
            else {
                result = false;
                return true;
            }
        }
        else if (filter.comparator == ">") {
            console.log(">", fieldValue, filter.value);

            if (fieldValue > filter.value) {
                result = true;
            }
            else {
                result = false;
                return true;
            }
        }
        else if (filter.comparator == ">=") {
            if (fieldValue >= filter.value) {
                result = true;
            }
            else {
                result = false;
                return true;
            }
        }
        return false;
    });
    return result;
}

export enum EQueryMembershipAction {
    INCREMENT = "INCREMENT",
    NOTHING = "NOTHING",
    DECREMENT = "DECREMENT"
}


/// updates
const pageLimit = 10;
export async function detectActivePolicyConversion() {
    const now = new Date();
    /*
                    {propertyName: "policy", comparator: "=", value: true},
                {propertyName: "startDate", comparator: ">", value: <any> addYearsToDate(now, -1).getTime()},
                {propertyName: "startDate", comparator: "<", value: <any> now.getTime()},
                {propertyName: "cancelled", comparator: "=", value: false}
                */
    const filters: IGDSFilter[] = [
        { propertyName: "policy", comparator: "=", value: true },
        { propertyName: "startDate", comparator: ">", value: <any>addYearsToDate(now, -1).getTime() },
        { propertyName: "startDate", comparator: "<", value: <any>now.getTime() },
        { propertyName: "cancelled", comparator: "=", value: false },
        { propertyName: "STATS_checkedForActivePolicy", comparator: "=", value: false },
        { propertyName: "numberOfMonthUnitsForTerm", comparator: "=", value: 12 }
    ];

    let finished: boolean = false;
    let cursor: string = null;

    // while we have no still paged over all the data.... continue paging
    while (finished == false) {
        console.log("detectActivePolicyConversion loop");
        // get page
        const pageOfData = await gcdsInstance.queryManyLimited("QuotePolicy", filters, batchSize, cursor);
        console.log("pageOfData", pageOfData);
        // update finished for next iteration
        finished = pageOfData.allFinished;
        // update cursor for next page
        cursor = pageOfData.cursor;
        await Promise.all(pageOfData.results.map(async (result: IQuotePolicy, index) => {
            console.log("processing result", result, index);
            const name = result._name;
            const docRef = firestore.collection("QuotePolicy").doc(name);
            return docRef.set({
                STATS_checkedForActivePolicy: true
            });
        }));
    }
    console.log("all finished");
}
export async function detectExpiredPolicyConversion() {
    const now = new Date();
    const filters: IGDSFilter[] = [
        {propertyName: "policy", comparator: "=", value: true},
        {propertyName: "endDate", comparator: "<", value: <any> now.getTime()},
        { propertyName: "STATS_checkedForExpiredPolicy", comparator: "=", value: false },
    ];

    let finished: boolean = false;
    let cursor: string = null;

    // while we have no still paged over all the data.... continue paging
    while (finished == false) {
        console.log("detectActivePolicyConversion loop");
        // get page
        const pageOfData = await gcdsInstance.queryManyLimited("QuotePolicy", filters, batchSize, cursor);
        console.log("pageOfData", pageOfData);
        // update finished for next iteration
        finished = pageOfData.allFinished;
        // update cursor for next page
        cursor = pageOfData.cursor;
        await Promise.all(pageOfData.results.map(async (result: IQuotePolicy, index) => {
            console.log("processing result", result, index);
            const name = result._name;
            const docRef = firestore.collection("QuotePolicy").doc(name);
            return docRef.set({
                STATS_checkedForExpiredPolicy: true
            });
        }));
    }
    console.log("all finished");
}
export async function detectExpiredQuotesConversion() {
    const now = new Date();
    const filters: IGDSFilter[] = [
        {propertyName: "policy", comparator: "=", value: false},
        {propertyName: "quoteValidUntil", comparator: "<", value: <any> now.getTime()},
        {propertyName: "approvedStatus", comparator: "=", value: EApprovedStatus.provisional},
        { propertyName: "STATS_checkedForExpiredQuote", comparator: "=", value: false },
    ];

    let finished: boolean = false;
    let cursor: string = null;

    // while we have no still paged over all the data.... continue paging
    while (finished == false) {
        console.log("detectActivePolicyConversion loop");
        // get page
        const pageOfData = await gcdsInstance.queryManyLimited("QuotePolicy", filters, batchSize, cursor);
        console.log("pageOfData", pageOfData);
        // update finished for next iteration
        finished = pageOfData.allFinished;
        // update cursor for next page
        cursor = pageOfData.cursor;
        await Promise.all(pageOfData.results.map(async (result: IQuotePolicy, index) => {
            console.log("processing result", result, index);
            const name = result._name;
            const docRef = firestore.collection("QuotePolicy").doc(name);
            return docRef.set({
                STATS_checkedForExpiredQuote: true
            });
        }));
    }
    console.log("all finished");
}

// RenewalLess7
export async function detectRenewalLess7Conversion() {
    const now = new Date();
    const filters: IGDSFilter[] = [
        {propertyName: "endDate", comparator: "<", value: <any> addDaysToDate(now, 7).getTime()},
        {propertyName: "policy", comparator: "=", value: true},
        { propertyName: "STATS_checkedForRenewalLess7", comparator: "=", value: false },
    ];

    let finished: boolean = false;
    let cursor: string = null;

    // while we have no still paged over all the data.... continue paging
    while (finished == false) {
        console.log("detectActivePolicyConversion loop");
        // get page
        const pageOfData = await gcdsInstance.queryManyLimited("QuotePolicy", filters, batchSize, cursor);
        console.log("pageOfData", pageOfData);
        // update finished for next iteration
        finished = pageOfData.allFinished;
        // update cursor for next page
        cursor = pageOfData.cursor;
        await Promise.all(pageOfData.results.map(async (result: IQuotePolicy, index) => {
            console.log("processing result", result, index);
            const name = result._name;
            const docRef = firestore.collection("QuotePolicy").doc(name);
            return docRef.set({
                STATS_checkedForRenewalLess7: true
            });
        }));
    }
    console.log("all finished");
}

// RenewalLess15

export async function detectRenewalLess15Conversion() {
    const now = new Date();
    const filters: IGDSFilter[] = [
        {propertyName: "endDate", comparator: "<", value: <any> addDaysToDate(now, 15).getTime()},
        {propertyName: "policy", comparator: "=", value: true},
        { propertyName: "STATS_checkedForRenewalLess15", comparator: "=", value: false },
    ];

    let finished: boolean = false;
    let cursor: string = null;

    // while we have no still paged over all the data.... continue paging
    while (finished == false) {
        console.log("detectActivePolicyConversion loop");
        // get page
        const pageOfData = await gcdsInstance.queryManyLimited("QuotePolicy", filters, batchSize, cursor);
        console.log("pageOfData", pageOfData);
        // update finished for next iteration
        finished = pageOfData.allFinished;
        // update cursor for next page
        cursor = pageOfData.cursor;
        await Promise.all(pageOfData.results.map(async (result: IQuotePolicy, index) => {
            console.log("processing result", result, index);
            const name = result._name;
            const docRef = firestore.collection("QuotePolicy").doc(name);
            return docRef.set({
                STATS_checkedForRenewalLess15: true
            });
        }));
    }
    console.log("all finished");
}

// RenewalLess30

export async function detectRenewalLess30Conversion() {
    const now = new Date();
    const filters: IGDSFilter[] = [
        {propertyName: "endDate", comparator: "<", value: <any> addDaysToDate(now, 30).getTime()},
        {propertyName: "policy", comparator: "=", value: true},
        { propertyName: "STATS_checkedForRenewalLess30", comparator: "=", value: false },
    ];

    let finished: boolean = false;
    let cursor: string = null;

    // while we have no still paged over all the data.... continue paging
    while (finished == false) {
        console.log("detectActivePolicyConversion loop");
        // get page
        const pageOfData = await gcdsInstance.queryManyLimited("QuotePolicy", filters, batchSize, cursor);
        console.log("pageOfData", pageOfData);
        // update finished for next iteration
        finished = pageOfData.allFinished;
        // update cursor for next page
        cursor = pageOfData.cursor;
        await Promise.all(pageOfData.results.map(async (result: IQuotePolicy, index) => {
            console.log("processing result", result, index);
            const name = result._name;
            const docRef = firestore.collection("QuotePolicy").doc(name);
            return docRef.set({
                STATS_checkedForRenewalLess30: true
            });
        }));
    }
    console.log("all finished");
}
// RenewalLess45

export async function detectRenewalLess45Conversion() {
    const now = new Date();
    const filters: IGDSFilter[] = [
        {propertyName: "endDate", comparator: "<", value: <any> addDaysToDate(now, 45).getTime()},
        {propertyName: "policy", comparator: "=", value: true},
        { propertyName: "STATS_checkedForRenewalLess45", comparator: "=", value: false },
    ];

    let finished: boolean = false;
    let cursor: string = null;

    // while we have no still paged over all the data.... continue paging
    while (finished == false) {
        console.log("detectActivePolicyConversion loop");
        // get page
        const pageOfData = await gcdsInstance.queryManyLimited("QuotePolicy", filters, batchSize, cursor);
        console.log("pageOfData", pageOfData);
        // update finished for next iteration
        finished = pageOfData.allFinished;
        // update cursor for next page
        cursor = pageOfData.cursor;
        await Promise.all(pageOfData.results.map(async (result: IQuotePolicy, index) => {
            console.log("processing result", result, index);
            const name = result._name;
            const docRef = firestore.collection("QuotePolicy").doc(name);
            return docRef.set({
                STATS_checkedForRenewalLess45: true
            });
        }));
    }
    console.log("all finished");
}

// RenewalLess60

export async function detectRenewalLess60Conversion() {
    const now = new Date();
    const filters: IGDSFilter[] = [
        {propertyName: "endDate", comparator: "<", value: <any> addDaysToDate(now, 60).getTime()},
        {propertyName: "policy", comparator: "=", value: true},
        { propertyName: "STATS_checkedForRenewalLess60", comparator: "=", value: false },
    ];

    let finished: boolean = false;
    let cursor: string = null;

    // while we have no still paged over all the data.... continue paging
    while (finished == false) {
        console.log("detectActivePolicyConversion loop");
        // get page
        const pageOfData = await gcdsInstance.queryManyLimited("QuotePolicy", filters, batchSize, cursor);
        console.log("pageOfData", pageOfData);
        // update finished for next iteration
        finished = pageOfData.allFinished;
        // update cursor for next page
        cursor = pageOfData.cursor;
        await Promise.all(pageOfData.results.map(async (result: IQuotePolicy, index) => {
            console.log("processing result", result, index);
            const name = result._name;
            const docRef = firestore.collection("QuotePolicy").doc(name);
            return docRef.set({
                STATS_checkedForRenewalLess60: true
            });
        }));
    }
    console.log("all finished");
}

// RenewalLess90


export async function detectRenewalLess90Conversion() {
    const now = new Date();
    const filters: IGDSFilter[] = [
        {propertyName: "endDate", comparator: "<", value: <any> addDaysToDate(now, 90).getTime()},
        {propertyName: "policy", comparator: "=", value: true},
        { propertyName: "STATS_checkedForRenewalLess90", comparator: "=", value: false },
    ];

    let finished: boolean = false;
    let cursor: string = null;

    // while we have no still paged over all the data.... continue paging
    while (finished == false) {
        console.log("detectActivePolicyConversion loop");
        // get page
        const pageOfData = await gcdsInstance.queryManyLimited("QuotePolicy", filters, batchSize, cursor);
        console.log("pageOfData", pageOfData);
        // update finished for next iteration
        finished = pageOfData.allFinished;
        // update cursor for next page
        cursor = pageOfData.cursor;
        await Promise.all(pageOfData.results.map(async (result: IQuotePolicy, index) => {
            console.log("processing result", result, index);
            const name = result._name;
            const docRef = firestore.collection("QuotePolicy").doc(name);
            return docRef.set({
                STATS_checkedForRenewalLess90: true
            });
        }));
    }
    console.log("all finished");
}
/*
                queries[queryName].forEach((filters: IGDSFilter[]) => {
                    const satisfiesFilter = localIGDSFilterComputer(filters, result);
                    if (satisfiesFilter == true) {
                        satisfiesOne = true;
                    }
                });

                const db = firebase.firestore();
const increment = firebase.firestore.FieldValue.increment(1);

// Document reference
const storyRef = db.collection('stories').doc('hello-world');

// Update read count
storyRef.update({ reads: increment });

// Create a reference to the SF doc.
var sfDocRef = db.collection("cities").doc("SF");

// Uncomment to initialize the doc.
// sfDocRef.set({ population: 0 });

return db.runTransaction(function(transaction) {
    // This code may get re-run multiple times if there are conflicts.
    return transaction.get(sfDocRef).then(function(sfDoc) {
        if (!sfDoc.exists) {
            throw "Document does not exist!";
        }

        // Add one person to the city population.
        // Note: this could be done without a transaction
        //       by updating the population using FieldValue.increment()
        var newPopulation = sfDoc.data().population + 1;
        transaction.update(sfDocRef, { population: newPopulation });
    });
}).then(function() {
    console.log("Transaction successfully committed!");
}).catch(function(error) {
    console.log("Transaction failed: ", error);
});

*/
export function computeQueryMembership(queryFilters: IGDSFilter[][], newEntityData: IQuotePolicy | ICustomer | any) {
    /*let oldDataMembership: boolean = false; // ;localIGDSFilterComputer(queryFilters, oldEntityData);
    let newDataMembership: boolean = false; // localIGDSFilterComputer(queryFilters, newEntityData);
    console.error("new", JSON.stringify(Object.keys(newEntityData)));
    console.error("old", JSON.stringify(Object.keys(newEntityData)));

    queryFilters.forEach((filter: IGDSFilter[]) => {
        console.error(JSON.stringify(filter));
        console.error("oldDataMembership", oldDataMembership);
        console.error("newDataMembership", newDataMembership);
        oldDataMembership = oldDataMembership || localIGDSFilterComputer(filter, oldEntityData);
        newDataMembership = newDataMembership || localIGDSFilterComputer(filter, newEntityData);
    });

    console.error("---------------------");

    console.error("oldDataMembership", oldDataMembership, typeof oldDataMembership);
    console.error("newDataMembership", newDataMembership, typeof newDataMembership);

    if (oldDataMembership != false && oldDataMembership == true && newDataMembership == false) {
        return EQueryMembershipAction.DECREMENT;
    }
    else if (oldDataMembership == false && newDataMembership != false && newDataMembership == true) {
        return EQueryMembershipAction.INCREMENT;
    }
    return EQueryMembershipAction.NOTHING;*/
    console.log("00000000000000000000000000");
    let newDataMembership: boolean = false; // localIGDSFilterComputer(queryFilters, newEntityData);
    queryFilters.forEach((filter: IGDSFilter[]) => {
        console.error("filter", JSON.stringify(filter));
        console.error("result", localIGDSFilterComputer(filter, newEntityData));
        newDataMembership = newDataMembership || localIGDSFilterComputer(filter, newEntityData);
    });
    console.log("FINAL", newDataMembership);
    return newDataMembership;
}


interface IStatisticEntityLock { }
function fetchStatisticEntityTemporalLockRef(kind: string, name: string) {
    return firestore.collection("StatisticsTemporalEntityLock").doc(`${kind}_${name}`);
}

export async function initStatsForKind(kind: string) {
    const queriesForKind = fetchKindQuery(kind, Date.now());
    const queryNames: Array<string> = Object.keys(queriesForKind.compoundQueryFilters);
    const applications: IApplication[] = await gcdsInstance.queryMany("Application", []);
    const applicationNames = applications.map((app: IApplication) => app.name);
    await Promise.all(applicationNames.map((appName: string) => {
        // trasactions
        return firestore.runTransaction(async (transaction: any) => {
            const docRef = firestore.collection("StatisticsRead").doc(appName).collection("Kind").doc(kind);
            const doc = await transaction.get(docRef);
            console.error("Statistics", appName, "Kind", kind);
            // FirebaseFirestore.Transaction
            if (!doc.exists) {
                const statsObj: any = {};
                queryNames.forEach((queryName: string) => {
                    statsObj[queryName] = 0;
                });
                docRef.set(
                    statsObj
                );
            }
        });
    }));
}

// todo @Vlad need a method to check end dates have elapased and mark them as checked

export async function updateAllStatsForKindEntity(kind: string, _newEntityData: IQuotePolicy | ICustomer | any) {
    // should we skip this because it is out of order?
    /*const lastTimeDocumentSnapshot = await fetchStatisticEntityTemporalLockRef(kind, newEntityData._name).get();
    if (lastTimeDocumentSnapshot.exists) {
        const lastTimeStamp: number = lastTimeDocumentSnapshot.data().timestamp;
        if (lastTimeStamp > timestamp) {
            console.error("SKIPPING DUE TO TIME LOCK");
            return;
        }
    }
    // set lock
    await fetchStatisticEntityTemporalLockRef(kind, newEntityData._name).set({timestamp});*/
    const queriesForKind = fetchKindQuery(kind, Date.now());
    const queryNames: Array<string> = Object.keys(queriesForKind.compoundQueryFilters);
    let applicationNames: Array<string> = null;
    // in case we are out of order with newEntityData
    const newEntityData: IQuotePolicy | ICustomer | any = await gcdsInstance.getOne([kind, _newEntityData._name]);
    if (kind === "QuotePolicy") {
        applicationNames = [newEntityData.systemApplication];
    }
    else if (kind === "Customer") {
        applicationNames = newEntityData.applications;
    }
    /*await Promise.all(applicationNames.map((appName: string) => {
        // trasactions
        return firestore.runTransaction(async (transaction: any) => {
            const docRef = firestore.collection("Statistics").doc(appName).collection("Kind").doc(kind);
            const doc = await transaction.get(docRef);
            console.error("Statistics", appName, "Kind", kind);
            // FirebaseFirestore.Transaction
            if (!doc.exists) {
                const statsObj: any = {};
                queryNames.forEach((queryName: string) => {
                    statsObj[queryName] = {};
                });
                docRef.set(
                    statsObj
                );
            } else {
                const statsObj = doc.data();
                console.error("data", JSON.stringify(statsObj));
                queryNames.forEach((queryName: string) => {
                    statsObj[queryName] = statsObj[queryName] || {};
                });
                console.error("JUST BEFORE CREATE", JSON.stringify(statsObj));
                docRef.set(
                    statsObj,
                    { merge: true }
                );
            }
        });
    }));*/
    const simpleStatsDelta: {[name: string]: boolean} = {};

    return Promise.all(applicationNames.map(async (appName: string) => {
        const statsDeltas: any = {};
        const docRef = firestore.collection("Statistics").doc(appName).collection("Kind").doc(kind);
        const stats = firestore.collection("Statistics2").doc(appName).collection("kind").doc(kind);
        // const increment = firebase.firestore.FieldValue.increment(1);
        // const decrement = firebase.firestore.FieldValue.increment(-1);

        await Promise.all(Object.keys(queriesForKind.compoundQueryFilters).map( async (namedQuery: string) => {
            // get stats table ref
            // const ref = firestore.collection(`Statistics`).doc()
            // /Statistics/kind/applicationName
            const filters = queriesForKind.compoundQueryFilters[namedQuery];
            console.error("computing membership", namedQuery);
            console.error(JSON.stringify(filters));
            const member: boolean = computeQueryMembership(filters, newEntityData);
            simpleStatsDelta[namedQuery] = member;
            // stats 2
            const namedQueryCol = stats.collection(namedQuery);
            const newEntityNamedQueryDataPoint = await namedQueryCol.doc(newEntityData._name).get();
            if (member == true && !newEntityNamedQueryDataPoint.exists) {
                await namedQueryCol.doc(newEntityData._name).set({});
            } else if (member == false && newEntityNamedQueryDataPoint.exists) {
                await namedQueryCol.doc(newEntityData._name).delete();
            }
            console.error("member", member);
            const key: string = newEntityData._name;
            if (!statsDeltas[namedQuery]) {
                statsDeltas[namedQuery] = {};
            }
            if (!statsDeltas[namedQuery][key]) {
                statsDeltas[namedQuery][key] = member;
            }
        }));
        console.error("STATS DELTA", JSON.stringify(statsDeltas));
        /*return firestore.runTransaction(async (transaction: FirebaseFirestore.Transaction) => {
            const data = (await transaction.get(docRef)).data();
            console.error("DATA FOR STAT OBJ REF");
            console.error(JSON.stringify(data));
            const statsValue: any = {};
            Object.keys(data).forEach((namedQuery: string) => {
                console.error("data[namedQuery]", namedQuery, data[namedQuery]);
                statsValue[namedQuery] = data[namedQuery];
                const member = statsDeltas[namedQuery][newEntityData._name];
                console.log("mmmmmemmmmberrrrrrrrr", member, namedQuery);

                if (typeof member !== "undefined") {
                    if (member == true) {
                        statsValue[namedQuery][newEntityData._name] = true;
                    }
                    else if (member == false && statsValue[namedQuery][newEntityData._name]) {
                        delete statsValue[namedQuery][newEntityData._name];
                    }
                }

            });
            console.error("LAST VALUE FOR STATS OBJ");
            console.error(JSON.stringify(statsValue));
            return transaction.set(docRef, statsValue);
        });*/



        // docRef.set(statsDeltas, {})
    })).then((it) => {
        return simpleStatsDelta;
    });

}

export async function collectStatsForKind(kind: string, datePinField: string, wantedBins: EBinType[], dateFieldNames: string[], queries: { [queryName: string]: IGDSFilter[][] }): Promise<IEntityStatsApplication> {
    // get all system applications
    const appNames: string[] = (await gcdsInstance.queryMany("Application", [])).map((it: any) => it.name);
    console.log("ALL SYSTEM APPS");
    // need to find number of years min+max date
    const maxDateResults = (await gcdsInstance.queryManyLimited(kind, [], 1, undefined, undefined, undefined, { [datePinField]: { descending: false } }));
    const minDateResults = (await gcdsInstance.queryManyLimited(kind, [], 1, undefined, undefined, undefined, { [datePinField]: { descending: true } }));
    const minDate: Date = minDateResults.results[0] ? new Date(minDateResults.results[0][datePinField]) : addYearsToDate(new Date(), - 2);
    const maxDate: Date = maxDateResults.results[0] ? new Date(maxDateResults.results[0][datePinField]) : addYearsToDate(new Date(), 1);

    // normalise the dates to start at 0 hours
    const normalisedMinDate = minDate;
    normalisedMinDate.setHours(0);
    normalisedMinDate.setMinutes(0);
    normalisedMinDate.setSeconds(0);
    normalisedMinDate.setMilliseconds(0);
    normalisedMinDate.setDate(1);

    const normalisedMaxDate = maxDate;
    normalisedMaxDate.setHours(0);
    normalisedMaxDate.setMinutes(0);
    normalisedMaxDate.setSeconds(0);
    normalisedMaxDate.setMilliseconds(0);
    normalisedMaxDate.setFullYear(normalisedMaxDate.getFullYear() + 1);

    // get day at end of month
    // normalisedMaxDate.setMonth(12); // normalisedMaxDate.getMonth() + 1
    // normalisedMaxDate.setDate(1);  // -1 will result in the day before the last day of the previous month

    // build container for stats
    const histogramCollector: IHistogramCollectionApplication = {};
    dateFieldNames.forEach((dateFieldName: string) => {
        histogramCollector[dateFieldName] = {};
        wantedBins.forEach((bin: EBinType) => {
            // create bins
            const binsForBinType: IDateBinApplication[] = [{ date: normalisedMinDate, count: {} }];
            appNames.forEach((appName: string) => {
                binsForBinType[0].count[appName] = new Set();
            });
            // iterate in steps of BinSize
            let startDateClone = new Date(normalisedMinDate.toISOString());

            while (startDateClone <= normalisedMaxDate) {
                /*if (bin == EBinType.Week || bin == EBinType.Month) {
                    console.log("startDateClone", bin, startDateClone);
                }*/
                const nextDate: Date = incrementDateByBinUnit(startDateClone, bin);
                /*if (bin == EBinType.Week || bin == EBinType.Month) {
                    console.log("nextDate", bin, nextDate);
                }*/
                const countObj: { [appName: string]: Set<string> } = {};
                appNames.forEach((appName: string) => {
                    countObj[appName] = new Set();
                });
                binsForBinType.push({ date: nextDate, count: countObj });
                startDateClone = nextDate;
            }
            histogramCollector[dateFieldName][bin] = binsForBinType;

        });
    });



    // build return object
    const returnData: IEntityStatsApplication = {
        kind: kind,
        count: {},
        calculatedAt: new Date().getTime(),
        histograms: null
    };

    Object.keys(queries).forEach((queryName: string) => {
        returnData.count[queryName] = {};
        appNames.forEach((appName: string) => {
            returnData.count[queryName][appName] = new Set();
        });
    });


    Object.keys(queries).forEach((queryName: string) => {
        const filters: IGDSFilter[][] = queries[queryName];
    });
    // iterate the data only once
    let finished: boolean = false;
    let cursor: string = null;

    // while we have no still paged over all the data.... continue paging
    while (finished == false) {
        // get page
        const pageOfData = await gcdsInstance.queryManyLimited(kind, [], batchSize, cursor);
        // update finished for next iteration
        finished = pageOfData.allFinished;
        // update cursor for next page
        cursor = pageOfData.cursor;
        // iterate current batch
        await Promise.all(pageOfData.results.map(async (result: any) => {
            /*if (result.hasOwnProperty("PRICE_ALGORITHM_VERSION")) {
                if (result["PRICE_ALGORITHM_VERSION"] == "vX.X.X") {
                    return;
                }
            }*/
            let entityApplications: string[] = [];
            let entityId: string = null;
            if (kind == "QuotePolicy") {
                entityApplications = [result.systemApplication];
                entityId = result.quoteId;
            }
            else if (kind == "Customer") {
                // fetch quotes
                const quotes = await gcdsInstance.queryMany("QuotePolicy", []);
                const allApplicationsForQuote = quotes.map((quoteInfo: IQuotePolicy) => quoteInfo.systemApplication);
                entityApplications = allApplicationsForQuote;
                entityId = result.id;
            }

            // iterate the queries
            await Promise.all(Object.keys(queries).map(async (queryName: string) => { // here!
                // ignore if does not satisfy constraint
                let satisfiesOne: boolean = false;
                queries[queryName].forEach((filters: IGDSFilter[]) => {
                    const satisfiesFilter = localIGDSFilterComputer(filters, result);
                    if (satisfiesFilter == true) {
                        satisfiesOne = true;
                    }
                });

                if (!satisfiesOne) return; // skip

                // increment the count
                entityApplications.forEach((appName: string) => {
                    returnData.count[queryName][appName].add(entityId);
                });

            })); // here

            // iterate the wanted bins
            return Promise.all(wantedBins.map(async (binType: EBinType) => { // outer
                // iterate dateFieldNames
                return Promise.all(dateFieldNames.map(async (dateFieldName: string) => {
                    // deal with this dateField
                    const dateInQuestion: Date = new Date(result[dateFieldName]);

                    // ignore invalid dates
                    if (!isValidDate(dateInQuestion)) {
                        console.log("SKIPPING INVALID DATE", dateFieldName, binType);
                        return;
                    }


                    // reference to histogram collector
                    const relevantHistogramCollector = histogramCollector[dateFieldName][binType];
                    let indexHit: number = -1;
                    // scan relevant histogram collector for correct index
                    const someResult = relevantHistogramCollector.some((bin: IDateBinApplication, index: number) => {
                        const isLastIndex = (index + 1) == relevantHistogramCollector.length;
                        if (!isLastIndex) {
                            if (dateInQuestion < relevantHistogramCollector[index + 1].date && dateInQuestion > bin.date) {
                                indexHit = index;
                                return true;
                            }
                        } else {
                            if (dateInQuestion > bin.date) {
                                indexHit = index;
                                return true;
                            } // otherwise outside collectable date range might be invalid.
                        }
                        return false;
                    });
                    // increment the correct bin count
                    if (indexHit != -1) {
                        entityApplications.forEach((entityApplication: string) => {
                            histogramCollector[dateFieldName][binType][indexHit].count[entityApplication].add(entityId);
                        });
                    }
                    else {
                        console.log("SKIPPING NO MATCH", dateFieldName, binType);
                    }
                }));
            })); // outer

        }));
    }
    returnData.histograms = histogramCollector;
    return returnData;
}


/*

export interface IHistogramCollectionApplication {
    [dateFieldName: string]: {
        [binType: string]: IDateBinApplication[]
    };
}
export interface IDateBinApplication {
    date: Date;
    count: { [applicationName: string]: Set<string> };
}

*/

export interface IDateBinApplicationStore {
    date: Date;
    count: { [applicationName: string]: string[] };
}
export interface IHistogramStore {
    calculatedAt: Date | number;
    bins: IDateBinApplicationStore[];
}

export async function saveHistograms(stats: IEntityStatsApplication) {
    const kind: string = stats.kind;
    return Promise.all(Object.keys(stats.histograms).map((dateFieldName: string) => {
        return Promise.all(Object.keys(stats.histograms[dateFieldName]).map((binType: EBinType) => {
            const kindFieldHistogramPath: string = `${statsFolder}/${kind}/${dateFieldName}/${binType}.json`;
            const bins: IDateBinApplication[] = stats.histograms[dateFieldName][binType];
            // map for serialisation
            const mappedBins: IDateBinApplicationStore[] = bins.map((bin: IDateBinApplication) => {
                const storeBin: IDateBinApplicationStore = {
                    date: bin.date,
                    count: Object.keys(bin.count).reduce((acc: any, appName: string) => {
                        acc[appName] = Array.from(bin.count[appName]);
                        return acc;
                    }, {})
                };
                return storeBin;
            });
            const blob: IHistogramStore = { calculatedAt: new Date(stats.calculatedAt).getTime(), bins: mappedBins };
            return createJsonFileWithPath(kindFieldHistogramPath, blob);
        }));
    }));
}
/*


export interface IStatsHistogram {
    histogram: IDateBin[];
    calculatedAt: Date;
    kind: string;
    binType: EBinType;
    dateFieldName: string;
}

export interface IDateBin {
    date: Date;
    count: number;
}
export interface IDateBinApplication {
    date: Date;
    count: { [applicationName: string]: Set<string> };
}
*/

export interface IHistogramOutput {
    calculatedAt: Date | number;
    bins: IDateBin[];
}

export async function fetchHistogram(kind: string, dateFieldName: string, binType: EBinType, applications: string[]): Promise<IHistogramOutput> {
    const output: IStatsHistogram = {
        calculatedAt: null,
        histogram: [],
        kind,
        binType,
        dateFieldName
    };
    const kindFieldHistogramPath: string = `${statsFolder}/${kind}/${dateFieldName}/${binType}.json`;
    const data: IHistogramStore = await fetchJsonFileFromPath(kindFieldHistogramPath);
    output.calculatedAt = new Date(data.calculatedAt).getTime();
    const mappedBins: IDateBin[] = data.bins.map((bin: IDateBinApplicationStore) => {
        const counts: Set<string> = new Set();
        Object.keys(bin.count).forEach((appName: string) => {
            if (applications.includes(appName)) {
                bin.count[appName].forEach((id: string) => {
                    counts.add(id);
                });
            }
        });
        const binOut: IDateBin = {
            date: <any>new Date(bin.date).getTime(),
            count: counts.size
        };
        return binOut;
    });
    return { calculatedAt: new Date(data.calculatedAt).getTime(), bins: mappedBins };
}

export interface IKindQueryCounts {
    kind: string;
    queryName: string;
    counts: { [appName: string]: string[] };
}
const statsFolder: string = "/stats";
export async function saveKindQueryCounts(stats: IEntityStatsApplication) {
    // make sure stats folder exists
    // entityCountKind
    // create record for kind/query + count Obj
    const kind: string = stats.kind;
    return Promise.all(Object.keys(stats.count).map((queryName: string) => {
        const kindQueryCountFilePath: string = `${statsFolder}/${kind}/${queryName}.json`;
        const queryCounts: { [appName: string]: Set<string> } = stats.count[queryName];
        const mappedQueryCounts: { [appName: string]: string[] } = Object.keys(queryCounts).reduce((acc: any, appName: string) => {
            acc[appName] = Array.from(queryCounts[appName]);
            return acc;
        }, {});
        return createJsonFileWithPath(kindQueryCountFilePath, { calculatedAt: new Date(stats.calculatedAt).getTime(), counts: mappedQueryCounts });
    }));
}

export interface IKindQueryCountStats {
    calculatedAt: Date | number;
    size: number;
}
export async function fetchKindQueryCountForApplications(kind: string, queryName: string, applications: string[]): Promise<IKindQueryCountStats> {
    const kindQueryCountFilePath: string = `${statsFolder}/${kind}/${queryName}.json`;
    const data: { calculatedAt: Date | number, counts: { [appName: string]: string[] } } = await fetchJsonFileFromPath(kindQueryCountFilePath);
    const ids: Set<string> = new Set();
    Object.keys(data.counts).forEach((appName: string) => {
        if (applications.includes(appName)) {
            data.counts[appName].forEach((id) => {
                ids.add(id);
            });
        }
    });
    return { size: ids.size, calculatedAt: new Date(data.calculatedAt).getTime() };
}

export async function processEntityCountStats(_applications: string[] | string, count: { [queryName: string]: { [appName: string]: Set<string> } }): Promise<{ [queryName: string]: number }> {
    let applications: string[] = [];
    if (_applications == "*") {
        applications = (await gcdsInstance.queryMany("Application", [])).map((it: any) => it.name);
    }
    else {
        applications = <string[]>_applications;
    }

    /*
    const returnData: IEntityStatsOutput = {
        kind: stats.kind,
        count: 0,
        histograms: null
    };
    returnData.count = countIds.size;
    */

    const returnData: { [queryName: string]: number } = {};
    const countsObject: { [queryName: string]: Set<string> } = {};
    Object.keys(count).forEach((queryName) => {
        returnData[queryName] = 0;
        countsObject[queryName] = new Set();
    });
    // sort count
    const countIds = new Set();

    Object.keys(count).map((queryName: string) => {
        Object.keys(count[queryName]).forEach((appName: string) => {
            console.log("applications.includes(appName)", applications.indexOf(appName) > -1, applications, appName);
            if (applications.includes(appName)) {
                console.log("stats.count[appName]", count[appName]);
                count[queryName][appName].forEach((id: string) => {
                    countsObject[queryName].add(id);
                });
            }
        });
    });

    Object.keys(countsObject).forEach((queryName) => {
        returnData[queryName] = countsObject[queryName].size;
    });

    return returnData;
    // return countIds.size;
}
/*
function processEntityHistogram(_applications: string[] | string, count: {[appName: string]: Set<string>}) {
    const outHists: IHistogramCollectionOutput = {};
    // sort hists
    //                                 histogramCollector[queryName][binType][dateFieldName][indexHit].count[entityApplication].add(entityId);
console.log("ABBBBOUTTUTUT TO HIISISTTTT");
    Object.keys(stats.histograms).forEach((queryName: string) => {
        outHists[queryName] = {};
        Object.keys(stats.histograms[queryName]).forEach((dataFieldName: string) => {
            stats.histograms[queryName][dataFieldName] = {};
            Object.keys(stats.histograms[queryName][dataFieldName]).forEach((bin: EBinType) => {
                console.log(queryName, dataFieldName, bin);
                outHists[queryName][dataFieldName][bin] = [];
                stats.histograms[queryName][dataFieldName][bin].forEach((binApp: IDateBinApplication) => {
                    const a: IDateBinOutput = {
                        date: binApp.date,
                        count: 0
                    };
                    const idSet = new Set();
                    Object.keys(binApp.count).forEach((appName: string) => {
                        if (applications.includes(appName)) {
                            binApp.count[appName].forEach((id: string) => {
                                idSet.add(id);
                            });
                        }
                    });
                    a.count = idSet.size;
                    console.log("GOT TO AAAAA");
                    outHists[queryName][dataFieldName][bin].push(a);
                });
            });
        });
    });

    returnData.histograms = outHists;

    return returnData;
}
*/

/*
export interface IStatsHistogram {
    histogram: IDateBin[];
    calculatedAt: Date;
    kind: string;
    binType: EBinType;
    dateFieldName: string;
}

        const quoteCreatedDate = await fetchHistogram(kind, "quoteCreatedDate", EBinType.Month, ["imaliaDaycare"]);
        console.log("quoteCreatedDate histogram", quoteCreatedDate, (Date.now() - hStart) / 1000);

export interface IHistogramOutput {
    calculatedAt: Date;
    bins: IDateBin[];
}
*/

export async function fetchHistograms(applications: string[]): Promise<IStatsHistogram[]> {
    const info = fetchJobs()[0];
    /*
            wantedBins: [EBinType.Day, EBinType.Month, EBinType.Week],
            dateFieldNames: ["quoteCreatedDate", "approvedDate", "startDate", "endDate"],
    */
    const bins = info.wantedBins;
    const dateFieldNames = info.dateFieldNames;
    const kind = info.kind;
    const histograms: IStatsHistogram[] = [];
    await Promise.all(bins.map((bin: EBinType) => {
        return Promise.all(dateFieldNames.map(async (fieldName: string) => {
            const data = await fetchHistogram(kind, fieldName, bin, applications);
            const hist: IStatsHistogram = {
                calculatedAt: new Date(data.calculatedAt).getTime(),
                kind: kind,
                binType: bin,
                dateFieldName: fieldName,
                histogram: <any>data.bins
            };
            histograms.push(hist);
        }));
    }));
    return histograms;
}


/*
AAAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGAAAAAAAAAAAAAAAAAAAAAIIIIIIIIIIIIIIIIIINNNNNNNNNNNNNNNNNNN
*/

export async function collectStatsForKind2(job: IStatsJobType2): Promise<IEntityStatsApplication> {
    // unpack
    const kind = job.kind;
    const datePinField = job.datePinField;
    const dateFieldNames = job.dateFieldNames;
    const wantedBins = job.wantedBins;
    const queries = job.queries.compoundQueryFilters;
    // get all system applications
    const appNames: string[] = (await gcdsInstance.queryMany("Application", [])).map((it: any) => it.name);
    console.log("ALL SYSTEM APPS");
    // need to find number of years min+max date
    const maxDateResults = (await gcdsInstance.queryManyLimited(kind, [], 1, undefined, undefined, undefined, { [datePinField]: { descending: false } }));
    const minDateResults = (await gcdsInstance.queryManyLimited(kind, [], 1, undefined, undefined, undefined, { [datePinField]: { descending: true } }));
    const minDate: Date = minDateResults.results[0] ? new Date(minDateResults.results[0][datePinField]) : addYearsToDate(new Date(), - 2);
    const maxDate: Date = maxDateResults.results[0] ? new Date(maxDateResults.results[0][datePinField]) : addYearsToDate(new Date(), 1);

    // normalise the dates to start at 0 hours
    const normalisedMinDate = minDate;
    normalisedMinDate.setHours(0);
    normalisedMinDate.setMinutes(0);
    normalisedMinDate.setSeconds(0);
    normalisedMinDate.setMilliseconds(0);
    normalisedMinDate.setDate(1);

    const normalisedMaxDate = maxDate;
    normalisedMaxDate.setHours(0);
    normalisedMaxDate.setMinutes(0);
    normalisedMaxDate.setSeconds(0);
    normalisedMaxDate.setMilliseconds(0);
    normalisedMaxDate.setFullYear(normalisedMaxDate.getFullYear() + 1);

    // get day at end of month
    // normalisedMaxDate.setMonth(12); // normalisedMaxDate.getMonth() + 1
    // normalisedMaxDate.setDate(1);  // -1 will result in the day before the last day of the previous month

    // build container for stats
    const histogramCollector: IHistogramCollectionApplication = {};
    dateFieldNames.forEach((dateFieldName: string) => {
        histogramCollector[dateFieldName] = {};
        wantedBins.forEach((bin: EBinType) => {
            // create bins
            const binsForBinType: IDateBinApplication[] = [{ date: normalisedMinDate, count: {} }];
            appNames.forEach((appName: string) => {
                binsForBinType[0].count[appName] = new Set();
            });
            // iterate in steps of BinSize
            let startDateClone = new Date(normalisedMinDate.toISOString());

            while (startDateClone <= normalisedMaxDate) {
                /*if (bin == EBinType.Week || bin == EBinType.Month) {
                    console.log("startDateClone", bin, startDateClone);
                }*/
                const nextDate: Date = incrementDateByBinUnit(startDateClone, bin);
                /*if (bin == EBinType.Week || bin == EBinType.Month) {
                    console.log("nextDate", bin, nextDate);
                }*/
                const countObj: { [appName: string]: Set<string> } = {};
                appNames.forEach((appName: string) => {
                    countObj[appName] = new Set();
                });
                binsForBinType.push({ date: nextDate, count: countObj });
                startDateClone = nextDate;
            }
            histogramCollector[dateFieldName][bin] = binsForBinType;

        });
    });



    // build return object
    const returnData: IEntityStatsApplication = {
        kind: kind,
        count: {},
        calculatedAt: new Date().getTime(),
        histograms: null
    };

    Object.keys(queries).forEach((queryName: string) => {
        returnData.count[queryName] = {};
        appNames.forEach((appName: string) => {
            returnData.count[queryName][appName] = new Set();
        });
    });


    Object.keys(queries).forEach((queryName: string) => {
        const filters: IGDSFilter[][] = queries[queryName];
    });
    // iterate the data only once
    let finished: boolean = false;
    let cursor: string = null;

    // while we have no still paged over all the data.... continue paging
    while (finished == false) {
        // get page
        const pageOfData = await gcdsInstance.queryManyLimited(kind, [], batchSize, cursor);
        // update finished for next iteration
        finished = pageOfData.allFinished;
        // update cursor for next page
        cursor = pageOfData.cursor;
        // iterate current batch
        await Promise.all(pageOfData.results.map(async (result: any) => {
            /*if (result.hasOwnProperty("PRICE_ALGORITHM_VERSION")) {
                if (result["PRICE_ALGORITHM_VERSION"] == "vX.X.X") {
                    return;
                }
            }*/
            let entityApplications: string[] = [];
            let entityId: string = null;
            if (kind == "QuotePolicy") {
                entityApplications = [result[job.queries.permissionFilter.propertyName]];
                // entityApplications = [result.systemApplication];
                entityId = result.quoteId;
            }
            else if (kind == "Customer") {
                // fetch quotes
                // const quotes = await gcdsInstance.queryMany("QuotePolicy", []);
                // const allApplicationsForQuote = quotes.map((quoteInfo: IQuotePolicy) => quoteInfo.systemApplication);
                entityApplications = result[job.queries.permissionFilter.propertyName];
                entityId = result.id;
            }

            // iterate the queries
            await Promise.all(Object.keys(queries).map(async (queryName: string) => { // here!
                // ignore if does not satisfy constraint
                let satisfiesOne: boolean = false;
                queries[queryName].forEach((filters: IGDSFilter[]) => {
                    const satisfiesFilter = localIGDSFilterComputer(filters, result);
                    if (satisfiesFilter == true) {
                        satisfiesOne = true;
                    }
                });

                if (!satisfiesOne) return; // skip

                // increment the count
                entityApplications.forEach((appName: string) => {
                    returnData.count[queryName][appName].add(entityId);
                });

            })); // here

            // iterate the wanted bins
            return Promise.all(wantedBins.map(async (binType: EBinType) => { // outer
                // iterate dateFieldNames
                return Promise.all(dateFieldNames.map(async (dateFieldName: string) => {
                    // deal with this dateField
                    const dateInQuestion: Date = new Date(result[dateFieldName]);

                    // ignore invalid dates
                    if (!isValidDate(dateInQuestion)) {
                        console.log("SKIPPING INVALID DATE", dateFieldName, binType);
                        return;
                    }


                    // reference to histogram collector
                    const relevantHistogramCollector = histogramCollector[dateFieldName][binType];
                    let indexHit: number = -1;
                    // scan relevant histogram collector for correct index
                    const someResult = relevantHistogramCollector.some((bin: IDateBinApplication, index: number) => {
                        const isLastIndex = (index + 1) == relevantHistogramCollector.length;
                        if (!isLastIndex) {
                            if (dateInQuestion < relevantHistogramCollector[index + 1].date && dateInQuestion > bin.date) {
                                indexHit = index;
                                return true;
                            }
                        } else {
                            if (dateInQuestion > bin.date) {
                                indexHit = index;
                                return true;
                            } // otherwise outside collectable date range might be invalid.
                        }
                        return false;
                    });
                    // increment the correct bin count
                    if (indexHit != -1) {
                        entityApplications.forEach((entityApplication: string) => {
                            histogramCollector[dateFieldName][binType][indexHit].count[entityApplication].add(entityId);
                        });
                    }
                    else {
                        console.log("SKIPPING NO MATCH", dateFieldName, binType);
                    }
                }));
            })); // outer

        }));
    }
    returnData.histograms = histogramCollector;
    return returnData;
}
