import { gcdsInstance, IGDSKey } from "@vaultspace/gdatastore-v1";
import { IApplicationFunnel } from "@vaultspace/neon-shared-models";
import { firestore } from "@vaultspace/firebase-server";

const applicationKind: string = "Application";
const applicationFunnelKind = "ApplicationFunnel";

export async function fetchApplication(applicationName: string) {
    return await gcdsInstance.getOne({kind: applicationKind, name: applicationName});
}

// export async function fetchApplicationFunnel(applicationName: string, funnelName: string): Promise<IApplicationFunnel> {
//     const key: IGDSKey = {kind: applicationFunnelKind, name: funnelName, ancestorKind: applicationKind, ancestorName: applicationName};
//     return gcdsInstance.getOne(key);
// }

export async function fetchApplicationFunnel(applicationName: string, sourceClass?: string): Promise<IApplicationFunnel> {
    let applicationFunnel: any;

    await firestore.collection("Application").doc(applicationName).collection("ApplicationFunnel").where("sourceClass", "==", sourceClass || "dx-scale").get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            applicationFunnel = doc.data();
        });
    });

    return applicationFunnel;
}