import * as deepDiff from "deep-diff";
function clone(obj: any) {
    return JSON.parse(JSON.stringify(obj));
}
export class DeepDiff { // i dont trust what its doing to my references
    public diff(_new: any, _old: any) {
        return deepDiff.diff(clone(_new), clone(_old));
    }
    public apply(_new: any, diffs: any) {
        const _newClone = clone(_new);
        diffs.forEach((it: any) => {
            deepDiff.applyChange(_newClone, _new, it);
        });
        return clone(_newClone);
    }
}