import { IEmailContentOptions } from "../models";
import { IEmailContentOptionsBody, IEmailContentOptionsSubject, IEmailContent } from "../models/index";

export const replaceFieldsBody = (bodyTemplate: string, emailData: IEmailContentOptionsBody, emailOptions?: {[key: string]: any}): string => {

    let newBody = bodyTemplate;

    Object.entries(emailData).forEach(([adKey, adVal]) => {
        Object.entries(adVal).forEach(([key, value]) => {
            // get only the first name
            if (key == "firstName") {
                value = value.split(" ")[0];
            }

            const regularExpression = new RegExp(`:::${String(adKey)}.${String(key)}:::`, "g");
            newBody = newBody.replace(regularExpression, String(value));
        });
    });

    if (emailOptions) {
        Object.entries(emailOptions).forEach(([key, value]) => {
            const regularExpression = new RegExp(`:::Options.${String(key)}:::`, "g");
            newBody = newBody.replace(regularExpression, String(value));
        });
    }


    return newBody;
};

export const replaceFieldsSubject = (subjectTemplate: string, emailData: IEmailContentOptionsSubject): string => {

    let newSubject: string = subjectTemplate;

    Object.entries(emailData).forEach(([key, value]) => {
        const regularExpression = new RegExp(`:::${String(key)}:::`, "g");
        newSubject = newSubject.replace(regularExpression, String(value));
    });

    return newSubject;
};

export const replaceFields = (emailTemplate: IEmailContent, emailData: IEmailContentOptions, emailOptions?: {[key: string]: any}): IEmailContent => {
    return {
        body: replaceFieldsBody(emailTemplate.body, emailData.body, emailOptions),
        subject: replaceFieldsSubject(emailTemplate.subject, emailData.subject)
    };
};