import { gcdsInstance, TGFSKey } from "@vaultspace/gdatastore-v1";
import { availableAppName, availableEmailType, IEmailContent } from "../models";
import { applicationKind } from "../configs/kinds";



export function createEmailContentEntity(appName: availableAppName, emailType: availableEmailType, data: IEmailContent) {
    const key: TGFSKey = [applicationKind, appName, "EmailContent", emailType];
    return gcdsInstance.createOne(key, data, {merge: true});
}