import { imaliaDayCareDebtCollectorFeeAndTelephoneLegalAdviceProduct } from "../../providers/email/imaliaDaycare";
import { ICustomer, IQuotePolicy, IContact } from "@vaultspace/neon-shared-models";
import { ICreateFdcScheduleArguments } from "@vaultspace/pdf/src/models";


function getFullCost(quotePolicy: IQuotePolicy): number {
   return Object.keys(quotePolicy.paymentPlan.written.total.tax).reduce((acc, nameOfTotal) => {
        acc += parseFloat(quotePolicy.paymentPlan.written.total.tax[nameOfTotal]);
        return acc;
    }, 0)
 +
Object.keys(quotePolicy.paymentPlan.written.total.party).reduce((acc, nameOfTotal) => {
        acc += parseFloat(quotePolicy.paymentPlan.written.total.party[nameOfTotal]);
        return acc;
    }, 0);
}

export function fdcScheduleBuilder(customer: ICustomer, quotePolicy: IQuotePolicy, contact: IContact): ICreateFdcScheduleArguments {
    const fdcSchedule: ICreateFdcScheduleArguments = {
        policyNumber: quotePolicy._name,
        name: customer.formalName,
        address: customer.principalPlaceOfBusiness.formatted_address || `${customer.principalPlaceOfBusiness.street}, ${customer.principalPlaceOfBusiness.line2}, ${customer.region}, ${customer.principalPlaceOfBusiness.postCode}`,
        birthday: new Date(contact.dob).toDateString(),
        email: contact.email,
        contactNumber: contact.phone,
        outOfHomeCare: customer.providesOutOfHomeCare,
        fdcService: customer.registeredFamilyDayCareProvider,
        inceptionDate: new Date(quotePolicy.startDate).toString(), // @Vlad: or quoteCreatedDate;
        expiryDate: new Date(quotePolicy.endDate).toString(),
        // you can find limit and excess in the paymentPlan in quotePolicy
        publicLiability: quotePolicy.paymentPlan.limit.generalLiability, //  quotePolicy.publicLiabilityAmount.amount.toString(), // this is right
        productsLiability: quotePolicy.paymentPlan.limit.generalLiability, // quotePolicy.publicLiabilityAmount.amount.toString()
        professionalIndemnityCover: quotePolicy.paymentPlan.limit.generalLiability, // quotePolicy.publicLiabilityAmount.amount.toString(), // @Vlad: quotePolicy.publicLiabilityAmount.amount
        excessPersonalAccident: quotePolicy.paymentPlan.excess.personalAccident,
        excessGeneralLiability: quotePolicy.paymentPlan.excess.generalLiability,
        limitBrokerFee: quotePolicy.paymentPlan.limit.brokerFee,
        limitDebtCollectorFeeAndTelephoneLegalAdvice: quotePolicy.paymentPlan.limit.debtCollectorFeeAndTelephoneLegalAdvice,
        limitGeneralLiability: quotePolicy.paymentPlan.limit.generalLiability,
        limitPersonalAccident: quotePolicy.paymentPlan.limit.personalAccident,
        contractTAndCChanges: quotePolicy.contractTAndCChanges || null,
        interestedParties: quotePolicy.additionalParties ? quotePolicy.additionalParties.join(", ") : null,
        // QuotePolicy.paymentPlan.excess.generalLiability,
        /*
            brokerFee: "N/A"
            debtCollectorFeeAndTelephoneLegalAdvice: "NIL"
            generalLiability: "NIL"
            personalAccident: "NIL"
        */
        // this is wrong end
        // we have no endoresements so "" for now
        // endorsements: quotePolicy.endorsements, // @Vlad: can't find the source; please help
        reliefEducators: (Array.isArray(quotePolicy.reliefEducators)) ? quotePolicy.reliefEducators.join(", ") : null, // "N/A" || "N/A"
        contractNumber: quotePolicy._name,
        telephoneLegalAdvice: imaliaDayCareDebtCollectorFeeAndTelephoneLegalAdviceProduct.productName, // smart concatination needed imaliaDayCareDebtCollectorFeeAndTelephoneLegalAdviceProduct,
        // is this pre or post tax? paymentPlan
        annualWrittenPremium: getFullCost(quotePolicy).toFixed(2), // quotePolicy.paymentPlan.preTaxGWPWritten.total, // @Vlad: can't find the source; please help
        type: quotePolicy.paymentPlanType as "Monthly" | "Annual",
        wantsTelephoneAndLegalAdvice: quotePolicy.requireLegalAdviceAndDebtCollection,
        // endorsements: quotePolicy.endorsements || (Array.isArray(quotePolicy.reliefEducator)) ? quotePolicy.reliefEducator.join(", ") : "N/A" || "N/A",
    };

    return fdcSchedule;
}
