import { ICustomer, IQuotePolicy } from "@vaultspace/neon-shared-models";
import { ICreateCoCArguments } from "@vaultspace/pdf/src/models";

export function cocBuilder(customer: ICustomer, quotePolicy: IQuotePolicy): ICreateCoCArguments {
    const coc: ICreateCoCArguments = {
        fullName: customer.formalName,
        outOfHomeCare: customer.providesOutOfHomeCare,
        interestedParties: (Array.isArray(quotePolicy.additionalParties)) ? quotePolicy.additionalParties.join(", ") : "N/A",
        inceptionDate: new Date(quotePolicy.startDate).toString(),
        expiryDate: new Date(quotePolicy.endDate).toString(),
        limitBrokerFee: quotePolicy.paymentPlan.limit.generalLiability,
        limitDebtCollectorFeeAndTelephoneLegalAdvice: quotePolicy.paymentPlan.limit.debtCollectorFeeAndTelephoneLegalAdvice,
        limitGeneralLiability: quotePolicy.paymentPlan.limit.generalLiability,
        limitPersonalAccident: quotePolicy.paymentPlan.limit.personalAccident,
        dateOfSigning: new Date(quotePolicy.agreedWhen || quotePolicy.approvedDate).toString(), // agreedWhen
        address: customer.principalPlaceOfBusiness.formatted_address || `${customer.principalPlaceOfBusiness.street}, ${customer.principalPlaceOfBusiness.line2}, ${customer.region}, ${customer.principalPlaceOfBusiness.postCode}`
    };

    return coc;
}