import { IContact } from "@vaultspace/neon-shared-models";
import { formatDate_3 } from "../../date-time";

export function contact2SimpleBuilderArguments(contact: IContact): any {

    return {
        birthday: formatDate_3(new Date(contact.dob)),
        email: contact.email,
        contactNumber: contact.phone
    };
}