import { ICustomer } from "@vaultspace/neon-shared-models";
export function customer2SimpleBuilderArguments(customer: ICustomer): any {

    return {
        fullName: customer.formalName,
        outOfHomeCare: customer.providesOutOfHomeCare,
        address: `${customer.principalPlaceOfBusiness.street}, ${customer.principalPlaceOfBusiness.line2}, ${customer.region}, ${customer.principalPlaceOfBusiness.postCode}`,
        name: customer.formalName,
        fdcService: customer.registeredFamilyDayCareProvider
    };
}