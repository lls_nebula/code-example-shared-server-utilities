import { IQuotePolicy } from "@vaultspace/neon-shared-models";
import { formatDate_3 } from "../../date-time";

export function quotePolicy2SimpleBuilderArguments(quotePolicy: IQuotePolicy): any {

    return {
        interestedParties: (Array.isArray(quotePolicy.additionalParties)) ? quotePolicy.additionalParties.join(", ") : "N/A",
        inceptionDate: formatDate_3(new Date(quotePolicy.startDate)),
        expiryDate: formatDate_3(new Date(quotePolicy.endDate)),
        limitBrokerFee: quotePolicy.paymentPlan.limit.generalLiability,
        limitDebtCollectorFeeAndTelephoneLegalAdvice: quotePolicy.paymentPlan.limit.debtCollectorFeeAndTelephoneLegalAdvice,
        limitGeneralLiability: quotePolicy.paymentPlan.limit.generalLiability.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"),
        limitPersonalAccident: quotePolicy.paymentPlan.limit.personalAccident.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"),
        dateOfSigning: formatDate_3(new Date(quotePolicy.agreedWhen || quotePolicy.approvedDate)),
        policyNumber: quotePolicy._name,
        publicLiability: quotePolicy.publicLiabilityAmount.amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"),
        productsLiability: quotePolicy.publicLiabilityAmount.amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"),
        professionalIndemnityCover: quotePolicy.publicLiabilityAmount.amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"),
        excessPersonalAccident: quotePolicy.paymentPlan.excess.personalAccident.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"),
        excessGeneralLiability: quotePolicy.paymentPlan.excess.generalLiability.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"),
        endorsements: quotePolicy.endorsements || (Array.isArray(quotePolicy.reliefEducators)) ? quotePolicy.reliefEducators.join(", ") : "N/A" || "N/A",
        contractNumber: quotePolicy._name,
        annualWrittenPremium: quotePolicy.paymentPlan.preTaxGWPWritten.total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"),
        type: quotePolicy.paymentPlanType as "Monthly" | "Annual",
        wantsTelephoneAndLegalAdvice: quotePolicy.requireLegalAdviceAndDebtCollection,
    };
}