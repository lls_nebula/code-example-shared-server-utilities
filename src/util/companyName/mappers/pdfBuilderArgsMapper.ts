import { IQuotePolicy, ICustomer, IContact } from "@vaultspace/neon-shared-models";
import { quotePolicy2SimpleBuilderArguments } from "./quotePolicyMapper";
import { customer2SimpleBuilderArguments } from "./customerMapper";
import { contact2SimpleBuilderArguments } from "./contactMapper";

export function entities2SimpleBuilderArguments(quotePolicy: IQuotePolicy, customer: ICustomer, contact: IContact): any {
    return {
        ...quotePolicy2SimpleBuilderArguments(quotePolicy),
        ...customer2SimpleBuilderArguments(customer),
        ...contact2SimpleBuilderArguments(contact)
    };
}