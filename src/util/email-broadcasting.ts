import { VSMailer, ISendMail } from "@vaultspace/emailer";

import { IApplicationRole, IEmployee, ENotificationPermissionTypes, IRole, INotificationPermission } from "src/util/companyName/mappers/node_modules/@vaultspace/neon-shared-models";
import { gcdsInstance } from "../gcds-instance";
import { IGDSFilter, IGDSKey } from "@vaultspace/gdatastore-v1";
import { mailerResponseType } from "@vaultspace/emailer/dist/src/models";
import { NotificationBuilder } from "./notification-builder";

const notificationPermissionsProtoKind = "notificationPermissions";
const employeeKind = "Employee";
const roleKind = "Role";

async function fetchNotificationPermission(roleName: string): Promise<INotificationPermission> {
    const key: IGDSKey = { kind: roleKind, name: roleName };
    const role: IRole = await gcdsInstance.getOne(key);

    return role.notificationPermissionData;
}

function getUniqueEmails(arr: string[][]): string[] {
    const obj: {[key: string]: string} = {};

    for (const subArr of arr) {
        for (const email of subArr) {
            obj[email] = email;
        }
    }

    return Object.keys(obj);
}

async function listRoleNameForApplication(appName: string, kind: string): Promise<string[]> {
    try {
        const filters: IGDSFilter[] = [{
            comparator: "=",
            propertyName: "applicationName",
            value: appName,
        }];

        const appRoles = await gcdsInstance.queryMany(kind, filters) as Array<IApplicationRole>;

        return appRoles.map(item => item.role);

    } catch (err) { throw err; }
}

export async function getEmailsForBroadcastingByRole(role: string): Promise<string[]> {
    try {
        const filters: IGDSFilter[] = !!role ? [{
            comparator: "=",
            propertyName: "role",
            value: role
        }] : [];

        const employees: IEmployee[] = await gcdsInstance.queryMany(employeeKind, filters);

        return employees.map(item => item.email);

    } catch (err) {}
}

async function getEmailAddressesForRolesList(roles: Array<string>): Promise<string[]> {
    try {
        const promises: Array<Promise<Array<string>>> = [];

        roles.forEach(role => {
            promises.push(getEmailsForBroadcastingByRole(role));
        });

        const result = await Promise.all(promises);

        let emails: Array<string> = [];

        result.forEach(item => {
            emails = emails.concat(item);
        });

        return emails;

    } catch (err) { throw err; }
}

export async function getEmailsForBroadcastingByApp(appName: string): Promise<string[]> {
    try {
        const results = await Promise.all(Object.keys(ENotificationPermissionTypes).map(key => ENotificationPermissionTypes[key as any]).map(permissionType => {
            return listRoleNameForApplication(appName, `${notificationPermissionsProtoKind}_${permissionType}`).then((roles) => {
                return getEmailAddressesForRolesList(roles);
            }).catch(err => {
                throw err;
            });
        }));

        return getUniqueEmails(results);
    } catch (err) {
        throw err;
    }
}

export interface IBroadcastingDetail {
    emailList: string[];
    notificationType: string;
}
export async function getDetailsForBroadcastingByApp(appName: string): Promise<IBroadcastingDetail[]> {
    try {
        const result: IBroadcastingDetail[] = await Promise.all(Object.keys(ENotificationPermissionTypes).map(key => ENotificationPermissionTypes[key as any]).map((permissionType) => {
            return listRoleNameForApplication(
                appName,
                `${notificationPermissionsProtoKind}_${permissionType}`
            ).then(async (roles) => {
                return {
                    emailList: await getEmailAddressesForRolesList(roles),
                    notificationType: permissionType,
                };
            }).catch(err => {
                throw err;
            });
        }));

        return result;

    } catch (err) { throw err; }
}

export async function broadcastForApplication(appName: string, userID: string): Promise<mailerResponseType[][]> {
    const mailer = new VSMailer(userID);
    const detailList: IBroadcastingDetail[] = await getDetailsForBroadcastingByApp(appName);

    return await Promise.all(detailList.map(async (detail) => {
        const detailForEmail: ISendMail = await NotificationBuilder.buildNotification(detail.emailList, detail.notificationType);

        return mailer.sendMail(detailForEmail);
    }));
}
