const _MS_PER_DAY = 1000 * 60 * 60 * 24;

export function monthsElapsed_old(dt2: Date, dt1: Date): number {
    return Math.abs(Math.round(((dt2.getTime() - dt1.getTime()) / 1000) / 2419200));
}

export function monthsElapsed(dt1: Date, dt2: Date, roundUpFractionalMonths?: boolean, UTC?: boolean): number {
    let startDate: Date = dt1;
    let endDate: Date = dt2;

    if (dt1 > dt2) {
        startDate = dt2;
        endDate = dt1;
    }

    const startDateUTC = new Date(startDate.getTime() - startDate.getTimezoneOffset() * 60000);
    const endDatetUTC = new Date(endDate.getTime() - endDate.getTimezoneOffset() * 60000);

    const yearsDifference = endDate.getFullYear() - startDate.getFullYear();
    const monthsDifference = endDate.getMonth() - startDate.getMonth();
    const daysDifference = endDate.getDate() - startDate.getDate();

    // const yearsDifference = endDatetUTC.getUTCFullYear() - startDateUTC.getUTCFullYear();
    // const monthsDifference = endDatetUTC.getUTCMonth() - startDateUTC.getUTCMonth();
    // const daysDifference = endDatetUTC.getUTCDate() - startDateUTC.getUTCDate();

    let monthCorrection = 0;

    if (roundUpFractionalMonths === true && daysDifference >= 0) {
        monthCorrection = 1;
    } else if (roundUpFractionalMonths !== true && daysDifference < 0) {
        monthCorrection = -1;
    }

    if (monthsDifference == 0 && daysDifference < 22) {
        return 0;
    }

    return yearsDifference * 12 + monthsDifference + monthCorrection;
}

export function monthsElapsedUTC(dt1: Date, dt2: Date, roundUpFractionalMonths?: boolean, ): number {
    let startDate: Date = dt1;
    let endDate: Date = dt2;

    if (dt1 > dt2) {
        startDate = dt2;
        endDate = dt1;
    }

    const startDateUTC = new Date(startDate.getTime() - startDate.getTimezoneOffset() * 60000);
    const endDatetUTC = new Date(endDate.getTime() - endDate.getTimezoneOffset() * 60000);

    const yearsDifference = endDatetUTC.getUTCFullYear() - startDateUTC.getUTCFullYear();
    const monthsDifference = endDatetUTC.getUTCMonth() - startDateUTC.getUTCMonth();
    const daysDifference = endDatetUTC.getUTCDate() - startDateUTC.getUTCDate();

    let monthCorrection = 0;

    if (roundUpFractionalMonths === true && daysDifference >= 0) {
        monthCorrection = 1;
    } else if (roundUpFractionalMonths !== true && daysDifference < 0) {
        monthCorrection = -1;
    }

    if (monthsDifference == 0 && daysDifference < 22) {
        return 0;
    }

    return yearsDifference * 12 + monthsDifference + monthCorrection;
}

export function dateDiffInDays(dt1: Date, dt2: Date): number {

  // Discard the time and time-zone information.
  const utc1 = Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate());
  const utc2 = Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

export function formatDate_1(date: Date): string { // day[st/nd/rd/th] MonthName, FullYear
    const d = date.getDate();
    const m = date.toLocaleString("en-us", { month: "long" });
    const y = date.getFullYear();

    let dPostfix: string;

    if (d > 3 && d < 21) {
        dPostfix = "th";
    }

    switch (d % 10) {
        case 1:  dPostfix = "st"; break;
        case 2:  dPostfix = "nd"; break;
        case 3:  dPostfix = "rd"; break;
        default: dPostfix = "th"; break;
    }

    return `${d}${dPostfix} ${m}, ${y}`;
}

export function formatDate_2(date: Date): string { // DayName day[st/nd/rd/th] MonthName
    const d = date.getDate();
    const dName = date.toLocaleDateString("en-En", { weekday: "long" });
    const m = date.toLocaleString("en-En", { month: "long" });

    let dPostfix: string;

    if (d > 3 && d < 21) {
        dPostfix = "th";
    }

    switch (d % 10) {
        case 1:  dPostfix = "st"; break;
        case 2:  dPostfix = "nd"; break;
        case 3:  dPostfix = "rd"; break;
        default: dPostfix = "th"; break;
    }

    return `${dName} ${d}${dPostfix} ${m}`;
}

export function formatDate_3(date: Date): string { // dd-mm-YYYY
    return `${date.getUTCDate()}-${date.getUTCMonth() + 1}-${date.getFullYear()}`;
}