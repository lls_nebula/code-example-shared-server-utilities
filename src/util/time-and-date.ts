export function leapYear(year: number): boolean {
    return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}

export function addDaysToDate(someDate: Date, daysToAdd: number) {
    const dateClone: Date = new Date(someDate.getTime());
    dateClone.setUTCDate(someDate.getUTCDate() + daysToAdd);
    return dateClone;
}

export const roundCorrect = (num: number) => Math.round((num + 0.00001) * 100) / 100;

export function addYearToDate(someDate: Date) {
    const expireDate: Date = new Date(someDate);
    expireDate.setUTCFullYear(expireDate.getUTCFullYear() + 1);
    return expireDate;
}

export function addYearsToDate(someDate: Date, years: number = 1) {
    const expireDate: Date = new Date(someDate);
    expireDate.setUTCFullYear(expireDate.getUTCFullYear() + years);
    return expireDate;
}

export function addMonthsToDate(someDate: Date, months: number) {
    const dateClone: Date = new Date(someDate.getTime());
    dateClone.setUTCMonth(someDate.getUTCMonth() + months);
    return dateClone;
}

function round(v: number): number {
    return <any>(v >= 0 || -1) * Math.round(Math.abs(v));
}

export function subtractDaysFromDate(someDate: Date, days: number = 1): Date {
    const d: Date = new Date(someDate);
    d.setDate(d.getDate() - days);

    return d;
}


export function daysDifference(startDate: number | string | Date, endDate: number | string | Date): number {
    const delta = (new Date(startDate).getTime()) - (new Date(endDate).getTime());
    return (Math.floor(delta / (1000 * 60 * 60 * 24)));
}

export function getRandomInt(_min: number, _max: number) {
    const min = Math.ceil(_min);
    const max = Math.floor(_max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function randomDate(start: Date, end: Date) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

export function daysInMonth(inputDate: Date): number {
    const year = inputDate.getUTCFullYear();
    const month = inputDate.getUTCMonth();
    const cloneDate: Date = new Date(year, month + 1, 0);
    const daysInMonth: number = cloneDate.getUTCDate();
    return daysInMonth;
}

export function daysInMonthWithMonthYear(month: number, year: number): number {
    const cloneDate: Date = new Date(year, month + 1, 0);
    const daysInMonth: number = cloneDate.getDate();
    return daysInMonth;
}

export function targetLessThanEqualTo1CalendarMonthPlusStart(start: Date, target: Date) { // less than one calendar month
    const dateInOneMonth: Date = addMonthsToDate(start, 1);
    console.log(target, dateInOneMonth);
    return target <= dateInOneMonth;
}

export function targetGreaterThan1CalendarMonthPlusStart(start: Date, target: Date) { // less than one calendar month
    const dateInOneMonth: Date = addMonthsToDate(start, 1);
    return target > dateInOneMonth;
}

