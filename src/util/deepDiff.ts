import _ from "lodash";

interface IDiff {
    path: string;
    oldValue: any;
    newValue: any;
}

interface IDiffResult {
    different: IDiff[];
    missingFromFirst: string[];
    missingFromSecond: string[];
}

export function getDiff(obj: any, base: any): IDiffResult {

    const result: any = {
        different: [],
        missingFromFirst: [],
        missingFromSecond: []
    };

    _.reduce(obj, (result, value, key): IDiff => {
        if (base.hasOwnProperty(key)) {
            if (_.isEqual(value, base[key])) {
                return result;
            } else {
                if (typeof (obj[key]) != typeof ({}) || typeof (base[key]) != typeof ({})) {
                    // result.different.push(key);

                    result.different.push({
                        path: key,
                        oldValue: base[key],
                        newValue: obj[key]
                    });

                    return result;
                } else {
                    const deeper = getDiff(obj[key], base[key]);

                    // result.different = result.different.concat(_.map(deeper.different, (sub_path) => { return `${key}.${sub_path}`; }));
                    result.different = result.different.concat(_.map(deeper.different, (sub_path): IDiff => { return {
                        path: `${key}.${sub_path["path"]}`,
                        oldValue: sub_path["oldValue"],
                        newValue: sub_path["newValue"]
                    }; }));
                    result.missingFromSecond = result.missingFromSecond.concat(_.map(deeper.missingFromSecond, (sub_path) => { return `${key}.${sub_path}`; }));
                    result.missingFromFirst = result.missingFromFirst.concat(_.map(deeper.missingFromFirst, (sub_path) => { return `${key}.${sub_path}`; }));

                    return result;
                }
            }
        } else {
            result.missingFromSecond.push(key);

            return result;
        }
    }, result);

    _.reduce(base, function (result, value, key) {
        if (obj.hasOwnProperty(key)) {

            return result;
        } else {
            result.missingFromFirst.push(key);

            return result;
        }
    }, result);

    return result;
}