import { ISendMail, IBufferAttachment } from "@vaultspace/emailer";
import { IApplicationEmailContent } from "src/util/companyName/mappers/node_modules/@vaultspace/neon-shared-models";

export interface IEmailContentOptions {
    [key: string]: {
        [key: string]: string;
    };
}
export class NotificationBuilder {
    constructor() {

    }

    // TODO: kill it
    private static buildContent(applicationEmailContent: IApplicationEmailContent, additionalData?: IEmailContentOptions): string {
        // entity format: "name--->text"
        const seporator: string = "--->";

        applicationEmailContent.ofInputEntities.forEach((ent: string) => {
            const splittedString: string[] = ent.split(seporator);
            const regularExpression = new RegExp(`:::${splittedString[0]}:::`, "g");
            applicationEmailContent.textBlob = applicationEmailContent.textBlob.replace(regularExpression, splittedString[1]);
        });

        /* additionalData format:
        {
            Options: {
                password: "something", stripeRetries: "something"
            },
            Contact: {
                name: "something"
            }...
        }
        */
        if (additionalData) {
            Object.entries(additionalData).forEach(([adKey, adVal]) => {
                Object.entries(adVal).forEach(([key, value]) => {
                    const regularExpression = new RegExp(`:::${adKey}.${key}:::`, "g");
                    applicationEmailContent.textBlob = applicationEmailContent.textBlob.replace(regularExpression, value);
                });
            });
        }

        return applicationEmailContent.textBlob;
    }

    private static buildReceivePaymentDisputeNotification(
        to: string[],
        attachments?: IBufferAttachment[],
        bcc?: string[],
        cc?: string[]
    ): ISendMail {
        return {
            to: to,
            subject: `Your payment was disputed`,
            body: `ReceivePaymentDisputeNotification was sent`,
            attachments: attachments || [],
            bcc: bcc || [],
            cc: cc || []
        };
    }

    private static buildReceivePaymentFailureNotification(
        to: string[],
        attachments?: IBufferAttachment[],
        bcc?: string[],
        cc?: string[]
    ): ISendMail {
        return {
            to: to,
            subject: `Payment failed`,
            body: `ReceivePaymentFailureNotification was sent`,
            attachments: attachments || [],
            bcc: bcc || [],
            cc: cc || []
        };
    }

    private static buildReceiveRefundFailureNotification(
        to: string[],
        attachments?: IBufferAttachment[],
        bcc?: string[],
        cc?: string[]
    ): ISendMail {
        return {
            to: to,
            subject: `Refund failed`,
            body: `ReceiveRefundFailureNotification was sent`,
            attachments: attachments || [],
            bcc: bcc || [],
            cc: cc || []
        };
    }

    private static buildReceiveReferralNotification(
        to: string[],
        attachments?: IBufferAttachment[],
        bcc?: string[],
        cc?: string[]
    ): ISendMail {
        return {
            to: to,
            subject: `Referral`,
            body: `ReceiveReferralNotification was sent`,
            attachments: attachments || [],
            bcc: bcc || [],
            cc: cc || []
        };
    }

    public static buildEmailNotification(
        to: string[],
        applicationEmailContent: IApplicationEmailContent,
        additionalData?: IEmailContentOptions,
        attachments?: IBufferAttachment[],
        bcc?: string[],
        cc?: string[],
    ): ISendMail {

        return {
            to: to,
            subject: applicationEmailContent.name,
            body: this.buildContent(applicationEmailContent, additionalData),
            attachments: attachments || [],
            bcc: bcc || [],
            cc: cc || []
        };
    }

    public static buildNotification(
        to: string[],
        notificationType: string,
        applicationEmailContent?: IApplicationEmailContent,
        additionalData?: IEmailContentOptions,
    ): ISendMail {
        switch (notificationType) {
            case `canReceivePaymentDisputeNotification`: return this.buildReceivePaymentDisputeNotification(to);
            case `canReceivePaymentFailureNotification`: return this.buildReceivePaymentFailureNotification(to);
            case `canReceiveRefundFailureNotification`: return this.buildReceiveRefundFailureNotification(to);
            case `canReceiveReferralNotification`: return this.buildReceiveReferralNotification(to);

            default: return this.buildEmailNotification(to, applicationEmailContent, additionalData);
        }
    }
}
