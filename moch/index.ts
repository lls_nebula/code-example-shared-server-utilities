import { availableAppName } from "../src/models";
import { IEmployee, ICustomer, IContact, IApplication, IQuotePolicy, IApplicationFunnel } from "@vaultspace/neon-shared-models";


const now = new Date;
const testQuoteId = "test-quote-id-" + Math.random();
const testCustomerId = "test-customer-id-" + Math.random();
const tesEmployeeId = "test-employee-id-" + Math.random();
const testCustomerName = "Private Snafu";
const testAppName: availableAppName = "applicationName";

enum EConsumerType {
    Company = "Company",
    SoleTrader = "SoleTrader",
    Consumer = "Consumer",
    UnInsurable = "UnInsurable"
}
enum EConsumerStatus {
    Active = "Active",
    Inactive = "Inactive",
    Provisional = "Provisional"
}
enum EApplicationFunnelSourceType {
    dxForm = "dx-form",
    dxChatbot = "dx-chatbot",
    incongnitoForm = "cognito-form",
    dxTsv = "dx-tsv",
    dxCsv = "dx-csv"
}
enum EApplicationFunnelSourceClass {
    dxSpreadsheet = "dx-spreadsheet",
    dxZapier = "dx-zapier",
    dxScale = "dx-scale"
}

export const cocMoch = {
    fullName: testCustomerName, // customer.formalName
    outOfHomeCare: true, // customer.providesOutOfHomeCare
    interestedParty: 20000, // quotePolicy.additionalParties ???
    inceptionDate: "2019-03-08", // quotePolicy.startDate or quoteCreatedDate ???
    expiryDate: "2020-03-08", // quotePolicy.endDate
    limit: 20, // application.limit ?
    dateOfSigning: "2019-03-07", // quotePolicy.startDate
    address: "13, 44-4, Mezentseva, Tula, Russia", // customer.principalPlaceOfBusiness
};

export const fdcScheduleMoch = {
    policyNumber: "987-987-987", // quotePolicy.quoteId
    name: testCustomerName, // customer.formalName
    address: "54, 45, some, some", // customer.principalPlaceOfBusiness
    birthday: "2019-01-01", // customer.birthdate ???
    email: "steinerleelogan@gmail.com", // contact.email
    contactNumber: "+79509061440", // contact.phone
    outOfHomeCare: true, // customer.providesOutOfHomeCare
    fdcService: "some service", // ???
    inceptionDate: "2019-03-08", // quotePolicy.startDate or quoteCreatedDate ???
    expiryDate: "2020-03-08", // quotePolicy.endDate
    publicLiability: "20,000,000", // quotePolicy.publicLiabilityAmount
    productsLiability: "20,000,000", // quotePolicy.publicLiabilityAmount ???
    professionalIndemnityCover: "20,000,000", // ???
    excess: "20,000,000", // quotePolicy.excess
    endorsements: "some endorsements", // quotePolicy.endorsements???
    contractNumber: "321-321-321", // quotePolicy.quoteId
    telephoneLegalAdvice: "some advice", // exported from applicationNameDebtCollectorFeeAndTelephoneLegalAdviceProduct
    annualWrittenPremium: "some premium" // ???
};

export const fintechScheduleMoch = {
    schedule: {
        certNo: "123!@#3456&9",
        insured: "very valuable asset",
        address: "2, 67, Pobedy",
        abn: "123!@#3456&9",
        activities: "activities are active",
        inceptionDate: "2019-02-05",
        expiryDate: "2020-02-05",
    },

    plan: {
        plan: "Bronze",
        clauses1234678: 1000,
        clauses5: 2500,
        clauses: {
            clause1: {
                forTheEvent: {
                    a: 500,
                    b: 50,
                    d: 50,
                    e: 50,
                    total: 500,
                },
                forThePeriod: {
                    a: 500,
                    b: 50,
                    d: 50,
                    e: 50,
                    total: 500,
                },
            },
            clause2: {
                forTheEvent: {
                    a: 500,
                    b: 50,
                    c: 50,
                    d: 50,
                    e: 50,
                    f: 50,
                    total: 500,
                },
                forThePeriod: {
                    a: 500,
                    b: 50,
                    c: 50,
                    d: 50,
                    e: 50,
                    f: 50,
                    total: 500,
                },
            },
            clause3: {
                forTheEvent: {
                    a: 500,
                    b: 50,
                    c: 50,
                    d: 50,
                    e: 50,
                    f: 50,
                    total: 500,
                },
                forThePeriod: {
                    a: 500,
                    b: 50,
                    c: 50,
                    d: 50,
                    e: 50,
                    f: 50,
                    total: 500,
                },
            },
            clause4: {
                forTheEvent: {
                    a: 500,
                    b: 50,
                    c: 50,
                    d: 50,
                    e: 50,
                    f: 50,
                    total: 500,
                },
                forThePeriod: {
                    a: 500,
                    b: 50,
                    c: 50,
                    d: 50,
                    e: 50,
                    f: 50,
                    total: 500,
                },
            },
            clause5: {
                forTheEvent: {
                    a: 500,
                    b: 50,
                    c: 50,
                    d: 50,
                    e: 50,
                    f: 50,
                    total: 500,
                },
                forThePeriod: {
                    a: 500,
                    b: 50,
                    c: 50,
                    d: 50,
                    e: 50,
                    f: 50,
                    total: 500,
                },
            },
            clause6: {
                forTheEvent: {
                    total: 500,
                },
                forThePeriod: {
                    total: 500,
                },
            },
            clause7: {
                forTheEvent: {
                    a: 500,
                    b: 50,
                    c: 50,
                    d: 50,
                    e: 50,
                    f: 50,
                    total: 500,
                },
                forThePeriod: {
                    a: 500,
                    b: 50,
                    c: 50,
                    d: 50,
                    e: 50,
                    f: 50,
                    total: 500,
                },
            },
            clause8: {
                forTheEvent: {
                    total: 500,
                },
                forThePeriod: {
                    total: 500,
                },
            },
        },
        deductible: 1000,
        policyDocument: "and what are their names?",
        endorsements: "some data",
        premium: 10000,
        gst: 10000,
        stampDuty: 1000,
        esl_fsl: 1000,
        total: 9999999,
        date: "2019-02-05",
    },

    endorsements: "some data",

    witness: {
        at: "Tula",
        by: "Me",
    }
};


export const testCustomer: ICustomer = {
    id: testCustomerId,
    type: EConsumerType.Consumer,
    exactEntityType: "exactEntityType",
    status: EConsumerStatus.Active,
    createdAt: now.getTime(),
    firebaseUserId: "13",
};

export const testEmployee: IEmployee = {
    userId: "test@test.test",
    role: "admin",
    email: "test@test.test"
};

export const testContact: IContact = {
    customerId: testCustomerId,
    name: testCustomerName,
    email: "caxap_8787@mail.ru",
};

export const testApp: IApplication = {
    name: testAppName,
    code: "string",
    creatorUserId: testCustomerId,
    disabled: false,
    currentPricingEngineVersion: "string",
};

export const testAppFunnel: IApplicationFunnel = {
    name: "string",
    sourceClass: EApplicationFunnelSourceClass.dxSpreadsheet,
    sourceType: EApplicationFunnelSourceType.dxTsv,
    customerEndPointUrl: "customer/url",
};


export const testQuote = {
    quoteId: testQuoteId,
    customerId:  <availableAppName>testCustomerId,
    systemApplication: testAppName,
    quoteCreatedDate: now.getTime(),
    customerDetailsSnapshot: <any> {},
    approvedStatus: <any>"provisional",
    backDated: false,
    endDate: "12/1/2020",
    markedForCancellationDate: "12/1/2020",
    startDate: "1/1/2020",
    quoteNeedsApproval: true,
    quoteValidUntil: now.getTime(),
    policy: false,
    agreed: false,
    quoteExpired: false,
    signed: false,
    PRICE_ALGORITHM_VERSION: "v7.0.0",
    claim: false,
    publicLiabilityAmount: {
        amount: 20
    },
    paymentPlan: {
        limit: {
            generalLiability: "20000",
            personalAccident: "11111"
        },
        excess: {
            personalAccident: "11111"
        },
        preTaxGWPWritten: {
            total: "4444444"
        }
    },
};
