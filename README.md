# PDF GENERATOR

# Description

PDF generator for Schedules and Policies

# Pre-reqs
To build and run this app locally you will need a few things:
- Install [Node.js](https://nodejs.org/en/)

# Getting started
- Clone the repository
```
git clone git@bitbucket.org:realtimedataprotection/pdf.git
```
- Install dependencies
```
cd finscan
npm install
```
- Build the project
```
npm run build
```

- Run the tests
```
npm run test
```

- Compile the docs
```
npm run docs
```



