import { it, describe } from "mocha";
import chai = require("chai");
const expect = chai.expect;
import chaiAsPromised = require("chai-as-promised");
import { updateEntityCollectionWithDelta } from "../src/providers/price-engine";
import { gcdsInstance } from "@vaultspace/gdatastore-v1";
import { ICustomer, IQuotePolicy, IContact } from "@vaultspace/neon-shared-models";
import { quotePolicyConversion } from "../src/providers/events";

describe("updateEntityCollectionWithDelta", () => {
    it("should apply diff", async () => {
        // get random quote
        const randomQuotes = await gcdsInstance.queryManyLimited("QuotePolicy", [{propertyName: "policy", comparator: "=", value: true}], 1);
        const randomQuote: IQuotePolicy = randomQuotes.results[0];
        // get random customer
        const customer: ICustomer = await gcdsInstance.getOne({kind: "Customer", name: randomQuote.customerId});
        const contact: IContact = await gcdsInstance.getOne({kind: "Contact", name: customer.primaryContactId});
        // skip if invalid
        if (!customer) return;
        // fix changes
        customer.providesOutOfHomeCare = false;
        randomQuote.requireLegalAdviceAndDebtCollection = false;
        // test deltas
        const testDelta = {
            "Customer.providesOutOfHomeCare": true,
            "QuotePolicy.requireLegalAdviceAndDebtCollection": true
        };
        // print before
        console.log("BEFORE CHANGE");
        console.log("QuotePolicy", JSON.stringify(randomQuote, null, 4));
        console.log("Customer", JSON.stringify(customer, null, 4));
        console.log("-----------------------------------------------------------------------");
        // apply change
        console.log("APPLYING DELTA", testDelta);
        console.log("-----------------------------------------------------------------------");
        const modifiedContext = updateEntityCollectionWithDelta(randomQuote, customer, contact, testDelta);
        console.log("modifiedContext", JSON.stringify(modifiedContext, null, 4));
    });
});