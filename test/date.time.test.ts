
import { expect } from "chai";
import { it, describe } from "mocha";
import { monthsElapsed, monthsElapsedUTC } from "../src/util/date-time";

describe("date-time", () => {
    beforeEach(async () => {

    });

    it("Should return months of risk for the following interval: dt1 = 1/1/2019 dt2 = 3/2/2019 // 3", async () => {
        const monthsOfRisk: number = monthsElapsed(new Date("1/1/2019"), new Date("3/2/2019"), true);

        expect(monthsOfRisk).to.equal(3);
    });

    it("Should return months of risk for the following interval: dt1 = 1/1/2019 dt2 = 2/1/2019 // 2", async () => {
        const monthsOfRisk: number = monthsElapsed(new Date("1/1/2019"), new Date("2/1/2019"), true);

        expect(monthsOfRisk).to.equal(2);
    });

    it("Should return months of risk for the following interval: dt1 = 1/1/2019 dt2 = 1/31/2019 // 1", async () => {
        const monthsOfRisk: number = monthsElapsed(new Date("1/1/2019"), new Date("1/31/2019"), true);

        expect(monthsOfRisk).to.equal(1);
    });

    it("Should return months of risk for the following interval: dt1 = 1/1/2019 dt2 = 1/23/2019 // 1", async () => {
        const monthsOfRisk: number = monthsElapsed(new Date("1/1/2019"), new Date("1/23/2019"), true);

        expect(monthsOfRisk).to.equal(1);
    });

    it("Should return months of risk for the following interval: dt1 = 1/1/2019 dt2 = 1/22/2019 // 0", async () => {
        const monthsOfRisk: number = monthsElapsed(new Date("1/1/2019"), new Date("1/22/2019"), true);

        expect(monthsOfRisk).to.equal(0);
    });

    it("Should return months of risk for the following interval: dt1 = 1/1/2019 dt2 = 1/1/2019 // 0", async () => {
        const monthsOfRisk: number = monthsElapsed(new Date("1/1/2019"), new Date("1/1/2019"), true);

        expect(monthsOfRisk).to.equal(0);
    });

    it("Should return months of risk for the following interval: dt1 = 1/1/2019 dt2 = 3/2/2019 // 3 (UTC)", async () => {
        const monthsOfRisk: number = monthsElapsedUTC(new Date("1/1/2019"), new Date("3/2/2019"), true);

        expect(monthsOfRisk).to.equal(3);
    });

    it("Should return months of risk for the following interval: dt1 = 1/1/2019 dt2 = 2/1/2019 // 2 (UTC)", async () => {
        const monthsOfRisk: number = monthsElapsedUTC(new Date("1/1/2019"), new Date("2/1/2019"), true);

        expect(monthsOfRisk).to.equal(2);
    });

    it("Should return months of risk for the following interval: dt1 = 1/1/2019 dt2 = 1/31/2019 // 1 (UTC)", async () => {
        const monthsOfRisk: number = monthsElapsedUTC(new Date("1/1/2019"), new Date("1/31/2019"), true);

        expect(monthsOfRisk).to.equal(1);
    });

    it("Should return months of risk for the following interval: dt1 = 1/1/2019 dt2 = 1/23/2019 // 1 (UTC)", async () => {
        const monthsOfRisk: number = monthsElapsedUTC(new Date("1/1/2019"), new Date("1/23/2019"), true);

        expect(monthsOfRisk).to.equal(1);
    });

    it("Should return months of risk for the following interval: dt1 = 1/1/2019 dt2 = 1/22/2019 // 0 (UTC)", async () => {
        const monthsOfRisk: number = monthsElapsedUTC(new Date("1/1/2019"), new Date("1/22/2019"), true);

        expect(monthsOfRisk).to.equal(0);
    });

    it("Should return months of risk for the following interval: dt1 = 1/1/2019 dt2 = 1/1/2019 // 0 (UTC)", async () => {
        const monthsOfRisk: number = monthsElapsedUTC(new Date("1/1/2019"), new Date("1/1/2019"), true);

        expect(monthsOfRisk).to.equal(0);
    });
});