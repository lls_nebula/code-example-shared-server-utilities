import { emailContentBuilder } from "../src/providers/email/emailContentBuilder";

import { broadcastForApplication, getEmailsForBroadcastingByApp } from "../src/util/email-broadcasting";
import { mailerResponseType } from "@vaultspace/emailer/dist/src/models";
import { it, describe } from "mocha";

import chai = require("chai");
const expect = chai.expect;
import chaiAsPromised = require("chai-as-promised");

// Load chai-as-promised support
chai.use(chaiAsPromised);

function randomString() {
  let text = "";
  const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (let i = 0; i < 50; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text;
}

describe("Email broadcasting test", () => {
  beforeEach(async () => {
    setTimeout(() => { console.log(`wait...`); }, 3000);
  });

  it("Make a broadcast", async () => {
    // TODO: should be updated in accordance with the new email-engine

    const emailListForBroadcasting: string[] = await getEmailsForBroadcastingByApp("imaliaDaycare");

    console.log("###########################################################################################");
    console.log(emailListForBroadcasting);
    console.log("###########################################################################################");

    expect(!!emailListForBroadcasting.length).to.be.true;

    return Promise.resolve(1);
  });
});
