import {  eval_model, sanitize_price_model_output_for_consumer_pretax, eval_application_model, sanitize_price_model_output_for_consumer_tax, compute_final_revenue, eval_quote_model_charge } from "../src/providers/price-engine";
import fs from "fs";
import { expect } from "chai";
import { it, describe } from "mocha";
import { ERegion, EPaymentPlan, IQuotePolicy, ICustomer } from "@vaultspace/neon-shared-models";
import { EFieldType, IFieldMap, ISpreadSheetInput, ISpreadSheetOutput } from "@vaultspace/neon-shared-models";
import { gcdsInstance } from "../src/gcds-instance";

const inputs_correct: ISpreadSheetInput[] = [
    // state
    {
        sheet: "v0.0.11", cell: "B4", fromInput: "Customer.region", fieldMap: {
            type: EFieldType.numberEnum,
            numberEnumMap: {
                [ERegion.AustraliaCapitalTerritory]: 1,
                [ERegion.NewSouthWales]: 2,
                [ERegion.NorthernTerritory]: 3,
                [ERegion.Queensland]: 4,
                [ERegion.SouthAustralia]: 5,
                [ERegion.Tasmania]: 6,
                [ERegion.Victoria]: 3,
                [ERegion.WesternAustralia]: 3
            }
        }
    },
    {
        sheet: "v0.0.11", cell: "C4", fromInput: "QuotePolicy.requireLegalAdviceAndDebtCollection", fieldMap:
        {
            type: EFieldType.numberEnum, numberEnumMap: {
                true: 2,
                false: 1
            }
        }
    },
    {
        sheet: "v0.0.11", cell: "D4", fromInput: "QuotePolicy.publicLiabilityAmount.amount", fieldMap: {
            type: EFieldType.numberEnum,
            numberEnumMap: {
                10000000: 1,
                20000000: 2
            }
        }
    },
    {
        sheet: "v0.0.11", cell: "E4", fromInput: "Customer.providesOutOfHomeCare", fieldMap: {
            type: EFieldType.numberEnum,
            numberEnumMap: {
                true: 2,
                false: 1
            }
        }
    },
    {
        sheet: "v0.0.11", cell: "F4", fromInput: "QuotePolicy.paymentPlanType", fieldMap: {
            type: EFieldType.numberEnum,
            numberEnumMap: {
                [EPaymentPlan.Singular]: 1,
                [EPaymentPlan.Monthly]: 2
            }
        }
    },
    // days of risk
    { sheet: "v0.0.11", cell: "F19", fromInput: "daysOfRisk" }
];

describe("pricing engine", () => {
 // 20190529 NeonImalia_HomeCare_Pricing Model  V0.0.14.xlsx
 it("Should read DAYCARE spreadsheet data v0.0.15", async () => {
    // entity mock
    const context: any = {
        Customer: {
            region: ERegion.Tasmania,
            providesOutOfHomeCare: false
        },
        QuotePolicy: { // publicLiabilityAmount.amount
            requireLegalAdviceAndDebtCollection: false,
            publicLiabilityAmount: { currency: "AUD", amount: 20000000 },
            paymentPlanType: EPaymentPlan.Monthly
        },
        daysOfRisk: 100
    };
    // State	Legalyes2	PL Limit	2outhome	1 annual
    // 5	1	2	2	1

    const inputs: ISpreadSheetInput[] = JSON.parse('[{"cell":"B4","fieldMap":{"numberEnumMap":{"Australia Capital Territory(ACT)":1,"New South Wales":2,"Northern Territory":3,"Queensland":4,"South Australia":5,"Tasmania":6,"Victoria":3,"Western Australia":3},"type":"numberEnum"},"fromInput":"Customer.region","sheet":"v0.0.14"},{"cell":"E4","fieldMap":{"numberEnumMap":{"false":1,"true":2},"type":"numberEnum"},"fromInput":"Customer.providesOutOfHomeCare","sheet":"v0.0.14"},{"cell":"C4","fieldMap":{"numberEnumMap":{"false":1,"true":2},"type":"numberEnum"},"fromInput":"QuotePolicy.requireLegalAdviceAndDebtCollection","sheet":"v0.0.14"},{"cell":"D4","fieldMap":{"numberEnumMap":{"10000000":1,"20000000":2},"type":"numberEnum"},"fromInput":"QuotePolicy.publicLiabilityAmount.amount","sheet":"v0.0.14"},{"cell":"F4","fieldMap":{"numberEnumMap":{"Monthly":2,"Singular":1},"type":"numberEnum"},"fromInput":"QuotePolicy.paymentPlanType","sheet":"v0.0.14"},{"cell":"F19","fromInput":"daysOfRisk","sheet":"v0.0.14"}]');
    const outputs: ISpreadSheetOutput[] = <any> JSON.parse('[{"toOutput":"preTaxGWP.total","cell":"G59","sheet":"v0.0.14"},{"toOutput":"charge.brokerFee.tax.NRT","cell":"E52","sheet":"v0.0.14"},{"toOutput":"refund.brokerFee.tax.NRT","cell":"L52","sheet":"v0.0.14"},{"toOutput":"charge.total.tax.NRT","cell":"G52","sheet":"v0.0.14"},{"toOutput":"refund.total.tax.NRT","cell":"N52","sheet":"v0.0.14"},{"toOutput":"charge.brokerFee.tax.GST","cell":"E61","sheet":"v0.0.14"},{"toOutput":"refund.brokerFee.tax.GST","cell":"L61","sheet":"v0.0.14"},{"toOutput":"charge.total.tax.GST","cell":"G61","sheet":"v0.0.14"},{"toOutput":"refund.total.tax.GST","cell":"N61","sheet":"v0.0.14"},{"toOutput":"charge.brokerFee.party.Imalia","cell":"E53","sheet":"v0.0.14"},{"toOutput":"refund.brokerFee.party.Imalia","cell":"L53","sheet":"v0.0.14"},{"toOutput":"charge.total.party.Imalia","cell":"G53","sheet":"v0.0.14"},{"toOutput":"refund.total.party.Imalia","cell":"N53","sheet":"v0.0.14"},{"toOutput":"charge.brokerFee.party.DX-Evolution","cell":"E55","sheet":"v0.0.14"},{"toOutput":"refund.brokerFee.party.DX-Evolution","cell":"L55","sheet":"v0.0.14"},{"toOutput":"charge.total.party.DX-Evolution","cell":"G55","sheet":"v0.0.14"},{"toOutput":"refund.total.party.DX-Evolution","cell":"N55","sheet":"v0.0.14"},{"toOutput":"charge.brokerFee.party.Stripe","cell":"E56","sheet":"v0.0.14"},{"toOutput":"refund.brokerFee.party.Stripe","cell":"L56","sheet":"v0.0.14"},{"toOutput":"charge.total.party.Stripe","cell":"G56","sheet":"v0.0.14"},{"toOutput":"refund.total.party.Stripe","cell":"N56","sheet":"v0.0.14"},{"toOutput":"preTaxGWP.brokerFee","cell":"E59","sheet":"v0.0.14"},{"toOutput":"charge.debtCollectorFeeAndTelephoneLegalAdvice.tax.NRT","cell":"F52","sheet":"v0.0.14"},{"toOutput":"refund.debtCollectorFeeAndTelephoneLegalAdvice.tax.NRT","cell":"M52","sheet":"v0.0.14"},{"toOutput":"charge.debtCollectorFeeAndTelephoneLegalAdvice.tax.GST","cell":"F61","sheet":"v0.0.14"},{"toOutput":"refund.debtCollectorFeeAndTelephoneLegalAdvice.tax.GST","cell":"M61","sheet":"v0.0.14"},{"toOutput":"charge.debtCollectorFeeAndTelephoneLegalAdvice.party.Imalia","cell":"F53","sheet":"v0.0.14"},{"toOutput":"refund.debtCollectorFeeAndTelephoneLegalAdvice.party.Imalia","cell":"M53","sheet":"v0.0.14"},{"toOutput":"charge.debtCollectorFeeAndTelephoneLegalAdvice.party.DX-Evolution","cell":"F55","sheet":"v0.0.14"},{"toOutput":"refund.debtCollectorFeeAndTelephoneLegalAdvice.party.DX-Evolution","cell":"M55","sheet":"v0.0.14"},{"toOutput":"charge.debtCollectorFeeAndTelephoneLegalAdvice.party.Stripe","cell":"F56","sheet":"v0.0.14"},{"toOutput":"refund.debtCollectorFeeAndTelephoneLegalAdvice.party.Stripe","cell":"M56","sheet":"v0.0.14"},{"toOutput":"preTaxGWP.debtCollectorFeeAndTelephoneLegalAdvice","cell":"F59","sheet":"v0.0.14"},{"toOutput":"charge.generalLiability.tax.NRT","cell":"C52","sheet":"v0.0.14"},{"toOutput":"refund.generalLiability.tax.NRT","cell":"J52","sheet":"v0.0.14"},{"toOutput":"charge.generalLiability.tax.GST","cell":"C61","sheet":"v0.0.14"},{"toOutput":"refund.generalLiability.tax.GST","cell":"J61","sheet":"v0.0.14"},{"toOutput":"charge.generalLiability.tax.StampDuty","cell":"C62","sheet":"v0.0.14"},{"toOutput":"refund.generalLiability.tax.StampDuty","cell":"J62","sheet":"v0.0.14"},{"toOutput":"charge.total.tax.StampDuty","cell":"G62","sheet":"v0.0.14"},{"toOutput":"refund.total.tax.StampDuty","cell":"N62","sheet":"v0.0.14"},{"toOutput":"charge.generalLiability.party.Imalia","cell":"C53","sheet":"v0.0.14"},{"toOutput":"refund.generalLiability.party.Imalia","cell":"J53","sheet":"v0.0.14"},{"toOutput":"charge.generalLiability.party.JLT","cell":"C54","sheet":"v0.0.14"},{"toOutput":"refund.generalLiability.party.JLT","cell":"J54","sheet":"v0.0.14"},{"toOutput":"charge.total.party.JLT","cell":"G54","sheet":"v0.0.14"},{"toOutput":"refund.total.party.JLT","cell":"N54","sheet":"v0.0.14"},{"toOutput":"charge.generalLiability.party.DX-Evolution","cell":"C55","sheet":"v0.0.14"},{"toOutput":"refund.generalLiability.party.DX-Evolution","cell":"J55","sheet":"v0.0.14"},{"toOutput":"charge.generalLiability.party.Stripe","cell":"C56","sheet":"v0.0.14"},{"toOutput":"refund.generalLiability.party.Stripe","cell":"J56","sheet":"v0.0.14"},{"toOutput":"charge.generalLiability.party.Neon","cell":"C58","sheet":"v0.0.14"},{"toOutput":"refund.generalLiability.party.Neon","cell":"J58","sheet":"v0.0.14"},{"toOutput":"charge.total.party.Neon","cell":"G58","sheet":"v0.0.14"},{"toOutput":"refund.total.party.Neon","cell":"N58","sheet":"v0.0.14"},{"toOutput":"limit.generalLiability","cell":"C74","sheet":"v0.0.14"},{"toOutput":"excess.generalLiability","cell":"C73","sheet":"v0.0.14"},{"toOutput":"commission.generalLiability","cell":"C72","sheet":"v0.0.14"},{"toOutput":"preTaxGWP.generalLiability","cell":"C59","sheet":"v0.0.14"},{"toOutput":"charge.personalAccident.tax.NRT","cell":"D52","sheet":"v0.0.14"},{"toOutput":"refund.personalAccident.tax.NRT","cell":"K52","sheet":"v0.0.14"},{"toOutput":"charge.personalAccident.tax.GST","cell":"D61","sheet":"v0.0.14"},{"toOutput":"refund.personalAccident.tax.GST","cell":"K61","sheet":"v0.0.14"},{"toOutput":"charge.personalAccident.tax.StampDuty","cell":"D62","sheet":"v0.0.14"},{"toOutput":"refund.personalAccident.tax.StampDuty","cell":"K62","sheet":"v0.0.14"},{"toOutput":"charge.personalAccident.party.Imalia","cell":"D53","sheet":"v0.0.14"},{"toOutput":"refund.personalAccident.party.Imalia","cell":"K53","sheet":"v0.0.14"},{"toOutput":"charge.personalAccident.party.JLT","cell":"D54","sheet":"v0.0.14"},{"toOutput":"refund.personalAccident.party.JLT","cell":"K54","sheet":"v0.0.14"},{"toOutput":"charge.personalAccident.party.DX-Evolution","cell":"D55","sheet":"v0.0.14"},{"toOutput":"refund.personalAccident.party.DX-Evolution","cell":"K55","sheet":"v0.0.14"},{"toOutput":"charge.personalAccident.party.Stripe","cell":"D56","sheet":"v0.0.14"},{"toOutput":"refund.personalAccident.party.Stripe","cell":"K56","sheet":"v0.0.14"},{"toOutput":"charge.personalAccident.party.MS-Amlin","cell":"D57","sheet":"v0.0.14"},{"toOutput":"refund.personalAccident.party.MS-Amlin","cell":"K57","sheet":"v0.0.14"},{"toOutput":"charge.total.party.MS-Amlin","cell":"G57","sheet":"v0.0.14"},{"toOutput":"refund.total.party.MS-Amlin","cell":"N57","sheet":"v0.0.14"},{"toOutput":"limit.personalAccident","cell":"D74","sheet":"v0.0.14"},{"toOutput":"excess.personalAccident","cell":"D73","sheet":"v0.0.14"},{"toOutput":"commission.personalAccident","cell":"D72","sheet":"v0.0.14"},{"toOutput":"preTaxGWP.personalAccident","cell":"D59","sheet":"v0.0.14"}]');
    const modelFileData: Buffer = fs.readFileSync("./test/data/pricing-engine/20190529 NeonImalia_HomeCare_Pricing Model  V0.0.15.xlsx");
    console.log(modelFileData);
    const start = Date.now();
    const results = eval_model(modelFileData, inputs, outputs, context);
    console.log("inputs", inputs);
    console.log("outputs", outputs);
    console.log("Done in ", (Date.now() - start), " millissconds");
    console.log("RESULT:");

    console.log("##################################");

    console.log(JSON.stringify(results, null, 4));
    console.log("---------------------------");
    const preTax = sanitize_price_model_output_for_consumer_pretax(results);
    console.log(JSON.stringify(preTax, null, 4));
    console.log("'''''''''''''''''''''''''''''''''''''''''");
    const postTax = sanitize_price_model_output_for_consumer_tax(results);
    console.log(JSON.stringify(postTax, null, 4));

    const finalRevenue = compute_final_revenue(results);
    console.log("finalRevenue", finalRevenue);
    // try utils
});
/*
    it("Should read DAYCARE spreadsheet data", async () => {
        // entity mock
        const context: any = {
            Customer: {
                region: ERegion.SouthAustralia,
                providesOutOfHomeCare: false
            },
            QuotePolicy: {
                requireLegalAdviceAndDebtCollection: true,
                publicLiabilityAmount: { currency: "AUD", amount: 20000000 },
                paymentPlanType: EPaymentPlan.Singular
            },
            daysOfRisk: 100
        };
        // State	Legalyes2	PL Limit	2outhome	1 annual
        // 5	1	2	2	1

        const inputs: ISpreadSheetInput[] = [
            // state
            {
                sheet: "v0.0.11", cell: "B4", fromInput: "Customer.region", fieldMap: {
                    type: EFieldType.numberEnum,
                    numberEnumMap: {
                        [ERegion.AustraliaCapitalTerritory]: 1,
                        [ERegion.NewSouthWales]: 2,
                        [ERegion.NorthernTerritory]: 3,
                        [ERegion.Queensland]: 4,
                        [ERegion.SouthAustralia]: 5,
                        [ERegion.Tasmania]: 6,
                        [ERegion.Victoria]: 3,
                        [ERegion.WesternAustralia]: 3
                    }
                }
            },
            {
                sheet: "v0.0.11", cell: "C4", fromInput: "QuotePolicy.requireLegalAdviceAndDebtCollection", fieldMap:
                {
                    type: EFieldType.numberEnum, numberEnumMap: {
                        true: 2,
                        false: 1
                    }
                }
            },
            {
                sheet: "v0.0.11", cell: "D4", fromInput: "QuotePolicy.publicLiabilityAmount.amount", fieldMap: {
                    type: EFieldType.numberEnum,
                    numberEnumMap: {
                        10000000: 1,
                        20000000: 2
                    }
                }
            },
            {
                sheet: "v0.0.11", cell: "E4", fromInput: "Customer.providesOutOfHomeCare", fieldMap: {
                    type: EFieldType.numberEnum,
                    numberEnumMap: {
                        true: 2,
                        false: 1
                    }
                }
            },
            {
                sheet: "v0.0.11", cell: "F4", fromInput: "QuotePolicy.paymentPlanType", fieldMap: {
                    type: EFieldType.numberEnum,
                    numberEnumMap: {
                        [EPaymentPlan.Singular]: 1,
                        [EPaymentPlan.Monthly]: 2
                    }
                }
            },
            // days of risk
            { sheet: "v0.0.11", cell: "F19", fromInput: "daysOfRisk" }
        ];

        const outputs: ISpreadSheetOutput[] = [
            // nrt 52
            { sheet: "v0.0.11", cell: "C52", toOutput: "charge.generalLiability.nrt" },
            { sheet: "v0.0.11", cell: "D52", toOutput: "charge.personalAccident.nrt" },
            { sheet: "v0.0.11", cell: "E52", toOutput: "charge.brokerFee.nrt" },
            { sheet: "v0.0.11", cell: "F52", toOutput: "charge.debtCollectorFeeAndTelephoneLegalAdvice.nrt" },

            // imalia 53

            { sheet: "v0.0.11", cell: "C53", toOutput: "charge.generalLiability.imalia" },
            { sheet: "v0.0.11", cell: "D53", toOutput: "charge.personalAccident.imalia" },
            { sheet: "v0.0.11", cell: "E53", toOutput: "charge.brokerFee.imalia" },
            { sheet: "v0.0.11", cell: "F53", toOutput: "charge.debtCollectorFeeAndTelephoneLegalAdvice.imalia" },

            // jlt 54

            { sheet: "v0.0.11", cell: "C54", toOutput: "charge.generalLiability.jlt" },
            { sheet: "v0.0.11", cell: "D54", toOutput: "charge.personalAccident.jlt" },
            { sheet: "v0.0.11", cell: "E54", toOutput: "charge.brokerFee.jlt" },
            { sheet: "v0.0.11", cell: "F54", toOutput: "charge.debtCollectorFeeAndTelephoneLegalAdvice.jlt" },

            // a365 55

            { sheet: "v0.0.11", cell: "C55", toOutput: "charge.generalLiability.dx" },
            { sheet: "v0.0.11", cell: "D55", toOutput: "charge.personalAccident.dx" },
            { sheet: "v0.0.11", cell: "E55", toOutput: "charge.brokerFee.dx" },
            { sheet: "v0.0.11", cell: "F55", toOutput: "charge.debtCollectorFeeAndTelephoneLegalAdvice.dx" },

            // stripe 56

            { sheet: "v0.0.11", cell: "C56", toOutput: "charge.generalLiability.stripe" },
            { sheet: "v0.0.11", cell: "D56", toOutput: "charge.personalAccident.stripe" },
            { sheet: "v0.0.11", cell: "E56", toOutput: "charge.brokerFee.stripe" },
            { sheet: "v0.0.11", cell: "F56", toOutput: "charge.debtCollectorFeeAndTelephoneLegalAdvice.stripe" },

            // msamlin 57

            { sheet: "v0.0.11", cell: "C57", toOutput: "charge.generalLiability.msamlin" },
            { sheet: "v0.0.11", cell: "D57", toOutput: "charge.personalAccident.msamlin" },
            { sheet: "v0.0.11", cell: "E57", toOutput: "charge.brokerFee.msamlin" },
            { sheet: "v0.0.11", cell: "F57", toOutput: "charge.debtCollectorFeeAndTelephoneLegalAdvice.msamlin" },

            // neon 58

            { sheet: "v0.0.11", cell: "C58", toOutput: "charge.generalLiability.neon" },
            { sheet: "v0.0.11", cell: "D58", toOutput: "charge.personalAccident.neon" },
            { sheet: "v0.0.11", cell: "E58", toOutput: "charge.brokerFee.neon" },
            { sheet: "v0.0.11", cell: "F58", toOutput: "charge.debtCollectorFeeAndTelephoneLegalAdvice.neon" },

            // gwp 59

            { sheet: "v0.0.11", cell: "C59", toOutput: "charge.generalLiability.gwp" },
            { sheet: "v0.0.11", cell: "D59", toOutput: "charge.personalAccident.gwp" },
            { sheet: "v0.0.11", cell: "E59", toOutput: "charge.brokerFee.gwp" },
            { sheet: "v0.0.11", cell: "F59", toOutput: "charge.debtCollectorFeeAndTelephoneLegalAdvice.gwp" },

            // esl 60

            { sheet: "v0.0.11", cell: "C60", toOutput: "charge.generalLiability.esl" },
            { sheet: "v0.0.11", cell: "D60", toOutput: "charge.personalAccident.esl" },
            { sheet: "v0.0.11", cell: "E60", toOutput: "charge.brokerFee.esl" },
            { sheet: "v0.0.11", cell: "F60", toOutput: "charge.debtCollectorFeeAndTelephoneLegalAdvice.esl" },

            // gst 61

            { sheet: "v0.0.11", cell: "C61", toOutput: "charge.generalLiability.gst" },
            { sheet: "v0.0.11", cell: "D61", toOutput: "charge.personalAccident.gst" },
            { sheet: "v0.0.11", cell: "E61", toOutput: "charge.brokerFee.gst" },
            { sheet: "v0.0.11", cell: "F61", toOutput: "charge.debtCollectorFeeAndTelephoneLegalAdvice.gst" },

            // stamp 62

            { sheet: "v0.0.11", cell: "C62", toOutput: "charge.generalLiability.stamp" },
            { sheet: "v0.0.11", cell: "D62", toOutput: "charge.personalAccident.stamp" },
            { sheet: "v0.0.11", cell: "E62", toOutput: "charge.brokerFee.stamp" },
            { sheet: "v0.0.11", cell: "F62", toOutput: "charge.debtCollectorFeeAndTelephoneLegalAdvice.stamp" },

            // total 63

            { sheet: "v0.0.11", cell: "C63", toOutput: "charge.generalLiability.total" },
            { sheet: "v0.0.11", cell: "D63", toOutput: "charge.personalAccident.total" },
            { sheet: "v0.0.11", cell: "E63", toOutput: "charge.brokerFee.total" },
            { sheet: "v0.0.11", cell: "F63", toOutput: "charge.debtCollectorFeeAndTelephoneLegalAdvice.total" },

            // total

            { sheet: "v0.0.11", cell: "G52", toOutput: "charge.total.nrt" },
            { sheet: "v0.0.11", cell: "G53", toOutput: "charge.total.imalia" },
            { sheet: "v0.0.11", cell: "G54", toOutput: "charge.total.jlt" },
            { sheet: "v0.0.11", cell: "G55", toOutput: "charge.total.dx" },
            { sheet: "v0.0.11", cell: "G56", toOutput: "charge.total.stripe" },
            { sheet: "v0.0.11", cell: "G57", toOutput: "charge.total.msamlin" },
            { sheet: "v0.0.11", cell: "G58", toOutput: "charge.total.neon" },
            { sheet: "v0.0.11", cell: "G59", toOutput: "charge.total.gwp" },
            { sheet: "v0.0.11", cell: "G60", toOutput: "charge.total.esl" },
            { sheet: "v0.0.11", cell: "G61", toOutput: "charge.total.gst" },
            { sheet: "v0.0.11", cell: "G62", toOutput: "charge.total.stamp" },
            { sheet: "v0.0.11", cell: "G63", toOutput: "charge.total.total" },

            // REFUNDS:

            // nrt 52
            { sheet: "v0.0.11", cell: "J52", toOutput: "refund.generalLiability.nrt" },
            { sheet: "v0.0.11", cell: "K52", toOutput: "refund.personalAccident.nrt" },
            { sheet: "v0.0.11", cell: "L52", toOutput: "refund.brokerFee.nrt" },
            { sheet: "v0.0.11", cell: "M52", toOutput: "refund.debtCollectorFeeAndTelephoneLegalAdvice.nrt" },

            // imalia 53

            { sheet: "v0.0.11", cell: "J53", toOutput: "refund.generalLiability.imalia" },
            { sheet: "v0.0.11", cell: "K53", toOutput: "refund.personalAccident.imalia" },
            { sheet: "v0.0.11", cell: "L53", toOutput: "refund.brokerFee.imalia" },
            { sheet: "v0.0.11", cell: "M53", toOutput: "refund.debtCollectorFeeAndTelephoneLegalAdvice.imalia" },

            // jlt 54

            { sheet: "v0.0.11", cell: "J54", toOutput: "refund.generalLiability.jlt" },
            { sheet: "v0.0.11", cell: "K54", toOutput: "refund.personalAccident.jlt" },
            { sheet: "v0.0.11", cell: "L54", toOutput: "refund.brokerFee.jlt" },
            { sheet: "v0.0.11", cell: "M54", toOutput: "refund.debtCollectorFeeAndTelephoneLegalAdvice.jlt" },

            // a365 55

            { sheet: "v0.0.11", cell: "J55", toOutput: "refund.generalLiability.dx" },
            { sheet: "v0.0.11", cell: "K55", toOutput: "refund.personalAccident.dx" },
            { sheet: "v0.0.11", cell: "L55", toOutput: "refund.brokerFee.dx" },
            { sheet: "v0.0.11", cell: "M55", toOutput: "refund.debtCollectorFeeAndTelephoneLegalAdvice.dx" },

            // stripe 56

            { sheet: "v0.0.11", cell: "J56", toOutput: "refund.generalLiability.stripe" },
            { sheet: "v0.0.11", cell: "K56", toOutput: "refund.personalAccident.stripe" },
            { sheet: "v0.0.11", cell: "L56", toOutput: "refund.brokerFee.stripe" },
            { sheet: "v0.0.11", cell: "M56", toOutput: "refund.debtCollectorFeeAndTelephoneLegalAdvice.stripe" },

            // msamlin 57

            { sheet: "v0.0.11", cell: "J57", toOutput: "refund.generalLiability.msamlin" },
            { sheet: "v0.0.11", cell: "K57", toOutput: "refund.personalAccident.msamlin" },
            { sheet: "v0.0.11", cell: "L57", toOutput: "refund.brokerFee.msamlin" },
            { sheet: "v0.0.11", cell: "M57", toOutput: "refund.debtCollectorFeeAndTelephoneLegalAdvice.msamlin" },

            // neon 58

            { sheet: "v0.0.11", cell: "J58", toOutput: "refund.generalLiability.neon" },
            { sheet: "v0.0.11", cell: "K58", toOutput: "refund.personalAccident.neon" },
            { sheet: "v0.0.11", cell: "L58", toOutput: "refund.brokerFee.neon" },
            { sheet: "v0.0.11", cell: "M58", toOutput: "refund.debtCollectorFeeAndTelephoneLegalAdvice.neon" },

            // gwp 59

            { sheet: "v0.0.11", cell: "J59", toOutput: "refund.generalLiability.gwp" },
            { sheet: "v0.0.11", cell: "K59", toOutput: "refund.personalAccident.gwp" },
            { sheet: "v0.0.11", cell: "L59", toOutput: "refund.brokerFee.gwp" },
            { sheet: "v0.0.11", cell: "M59", toOutput: "refund.debtCollectorFeeAndTelephoneLegalAdvice.gwp" },

            // esl 60

            { sheet: "v0.0.11", cell: "J60", toOutput: "refund.generalLiability.esl" },
            { sheet: "v0.0.11", cell: "K60", toOutput: "refund.personalAccident.esl" },
            { sheet: "v0.0.11", cell: "L60", toOutput: "refund.brokerFee.esl" },
            { sheet: "v0.0.11", cell: "M60", toOutput: "refund.debtCollectorFeeAndTelephoneLegalAdvice.esl" },

            // gst 61

            { sheet: "v0.0.11", cell: "J61", toOutput: "refund.generalLiability.gst" },
            { sheet: "v0.0.11", cell: "K61", toOutput: "refund.personalAccident.gst" },
            { sheet: "v0.0.11", cell: "L61", toOutput: "refund.brokerFee.gst" },
            { sheet: "v0.0.11", cell: "M61", toOutput: "refund.debtCollectorFeeAndTelephoneLegalAdvice.gst" },

            // stamp 62

            { sheet: "v0.0.11", cell: "J62", toOutput: "refund.generalLiability.stamp" },
            { sheet: "v0.0.11", cell: "K62", toOutput: "refund.personalAccident.stamp" },
            { sheet: "v0.0.11", cell: "L62", toOutput: "refund.brokerFee.stamp" },
            { sheet: "v0.0.11", cell: "M62", toOutput: "refund.debtCollectorFeeAndTelephoneLegalAdvice.stamp" },

            // total 63

            { sheet: "v0.0.11", cell: "J63", toOutput: "refund.generalLiability.total" },
            { sheet: "v0.0.11", cell: "K63", toOutput: "refund.personalAccident.total" },
            { sheet: "v0.0.11", cell: "L63", toOutput: "refund.brokerFee.total" },
            { sheet: "v0.0.11", cell: "M63", toOutput: "refund.debtCollectorFeeAndTelephoneLegalAdvice.total" },

            { sheet: "v0.0.11", cell: "N52", toOutput: "refund.total.nrt" },
            { sheet: "v0.0.11", cell: "N53", toOutput: "refund.total.imalia" },
            { sheet: "v0.0.11", cell: "N54", toOutput: "refund.total.jlt" },
            { sheet: "v0.0.11", cell: "N55", toOutput: "refund.total.dx" },
            { sheet: "v0.0.11", cell: "N56", toOutput: "refund.total.stripe" },
            { sheet: "v0.0.11", cell: "N57", toOutput: "refund.total.msamlin" },
            { sheet: "v0.0.11", cell: "N58", toOutput: "refund.total.neon" },
            { sheet: "v0.0.11", cell: "N59", toOutput: "refund.total.gwp" },
            { sheet: "v0.0.11", cell: "N60", toOutput: "refund.total.esl" },
            { sheet: "v0.0.11", cell: "N61", toOutput: "refund.total.gst" },
            { sheet: "v0.0.11", cell: "N62", toOutput: "refund.total.stamp" },
            { sheet: "v0.0.11", cell: "N63", toOutput: "refund.total.total" },


            // 270.00000000	15.00000000	50.40000000	50.40000000	385.80000000
            // pre tax

            { sheet: "v0.0.11", cell: "C30", toOutput: "preTaxGWP.generalLiability" },
            { sheet: "v0.0.11", cell: "D30", toOutput: "preTaxGWP.personalAccident" },
            { sheet: "v0.0.11", cell: "E30", toOutput: "preTaxGWP.brokerFee" },
            { sheet: "v0.0.11", cell: "F30", toOutput: "preTaxGWP.debtCollectorFeeAndTelephoneLegalAdvice" },
            { sheet: "v0.0.11", cell: "G30", toOutput: "preTaxGWP.total" },




        ];

        const modelFileData: Buffer = fs.readFileSync("./test/data/pricing-engine/NeonImalia_HomeCare_Pricing Model _V0.0.11.xlsx");
        console.log(modelFileData);
        const start = Date.now();
        const results = eval_model(modelFileData, inputs, outputs, context);
        console.log("Done in ", (Date.now() - start), " millissconds");
        console.log("RESULT:");
        console.log(JSON.stringify(results, null, 4));
    });
*/

// https://firebasestorage.googleapis.com/v0/b/a365-neon-development.appspot.com/o/pricingModels%2F?alt=media&token=3772d9e8-5c8c-41a7-92d6-9c3a4abd9680
    it("Should run eval_quote_model_charge for a found policy context", async() => {
        const randomPolicy: IQuotePolicy = (await gcdsInstance.queryManyLimited("QuotePolicy", [
            {propertyName: "policy", comparator: <any> "=", value: true},
            {propertyName: "endDate", comparator: <any> ">", value: Date.now()}
        ], 1)).results[0];
        const customer: ICustomer = await gcdsInstance.getOne(["Customer", randomPolicy.customerId]);
        console.log(randomPolicy, customer);
 // 20190617 NeonImalia_FamilyDayCare_Pricing Model V0.0.19.xlsx
        const peResults = await eval_application_model("imaliaDaycare", "20190617 NeonImalia_FamilyDayCare_Pricing Model  V0.0.19.xlsx", "2", {
            QuotePolicy: randomPolicy,
            Customer: customer,
            isManualRating: 0
        });

        console.log("--------------------------------");
        console.log(JSON.stringify(peResults, null, 4));
    });
});