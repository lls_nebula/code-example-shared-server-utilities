import { expect } from "chai";
import { it, describe } from "mocha";
import { contextComputedField_computer, applicationUpdatedInner } from "../src/providers/events";
import { IApplicationProduct, IApplication } from "@vaultspace/neon-shared-models";
import { gcdsInstance } from "../src/gcds-instance";


describe("contextComputer", () => {
    it("Should run all jobs", async() => {
       const computedValue = await contextComputedField_computer("a.hello", {a: {}});
       console.log(computedValue);
    });

    it("siis", async () => {
        const applicationProductInfo: IApplication = await gcdsInstance.getOne(["Application", "applicationName"]);
        const applicationProductInfoMorphed: IApplication = JSON.parse(JSON.stringify(applicationProductInfo));
        applicationProductInfoMorphed.streamVariables["somethingnew"] = "1234";
        await applicationUpdatedInner("applicationName", applicationProductInfoMorphed, applicationProductInfo);
    });
});

