import { generateAttachmentsForFdcPersonal } from "./../src/providers/email/attachmentsGenerator/imaliaDaycare/imaliaDaycarePersonal";

import { expect } from "chai";
import { it, describe } from "mocha";
import { ICustomer, IContact, ERegion, EConsumerStatus, EConsumerType, IQuotePolicy, IApplication, IApplicationFunnel } from "@vaultspace/neon-shared-models";
import { IGDSKey, DatastoreKey, gcdsInstance } from "@vaultspace/gdatastore-v1";
import { testQuote } from "../moch/index";
import { quotePolicyKind, fetchQuote } from "../src/providers/quote-policy";
import { v4 as uuid } from "uuid";
import { fetchApplicationFunnel } from "../src";

const customer: ICustomer = {
    id: uuid(),
    type: EConsumerType.Consumer,
    exactEntityType: null,
    status: EConsumerStatus.Inactive,
    createdAt: new Date().getTime(),
    // additionalParties: ["20", "000", "000"],
    principalPlaceOfBusiness: {
        street: "street",
        line2: "line2 content",
        postCode: "30000"
    },
    region: ERegion.AustraliaCapitalTerritory
};

const contact: IContact = {
    customerId: customer.id,
    name: "Lee Logan Steiner",
    email: "steinerleelogan@gmail.com"
};

const quoteKey: IGDSKey = {name: testQuote.quoteId, kind: quotePolicyKind};
const deleteKey: DatastoreKey = {
    kind: quotePolicyKind,
    name: testQuote.quoteId,
    path: [quotePolicyKind, testQuote.quoteId],
};

describe("generateA", () => {
    beforeEach(async () => {

    });

    it("generateAttachmentsForFdcPersonal", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result = await generateAttachmentsForFdcPersonal({
            emailType: "firstInvoiceEmail",
            notificationType: "personal",
            Customer: customer,
            Contact: contact,
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: {
                password: "eDdwqe44dsf3f321s31s_221_000000"
            }
        });

        console.log(`&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&`);
        console.log(result);
        console.log(`&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&`);

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });

        expect(3).to.equal(3);
    });
});