import { eval_model, eval_quote_model_bespoke, sanitize_price_model_output_for_consumer_pretax, eval_application_model, sanitize_price_model_output_for_consumer_tax, compute_final_revenue, eval_quote_model_charge, IPaymentPlanOutput } from "../src/providers/price-engine";
import fs from "fs";
import { expect } from "chai";
import { it, describe } from "mocha";
import { ERegion, EPaymentPlan, IQuotePolicy, ICustomer } from "@vaultspace/neon-shared-models";
import { EFieldType, IFieldMap, ISpreadSheetInput, ISpreadSheetOutput } from "@vaultspace/neon-shared-models";
import { gcdsInstance } from "../src/gcds-instance";
import { contextComputedField_computer } from "../src/providers/events";

const modelConfig: any = {
  "inputs": [
    {
      "sheet": "FDC PE",
      "cell": "B4",
      "fromInput": "Customer.region",
      "fieldMap": {
        "numberEnumMap": {
          "Australia Capital Territory(ACT)": 1,
          "New South Wales": 2,
          "Northern Territory": 3,
          "Queensland": 4,
          "South Australia": 5,
          "Tasmania": 6,
          "Victoria": 3,
          "Western Australia": 3
        },
        "type": "numberEnum"
      }
    },
    {
      "sheet": "FDC PE",
      "cell": "E4",
      "fromInput": "Customer.providesOutOfHomeCare",
      "fieldMap": {
        "numberEnumMap": {
          "false": 1,
          "true": 2
        },
        "type": "numberEnum"
      }
    },
    {
      "sheet": "FDC PE",
      "cell": "C4",
      "fromInput": "QuotePolicy.requireLegalAdviceAndDebtCollection",
      "fieldMap": {
        "numberEnumMap": {
          "false": 1,
          "true": 2
        },
        "type": "numberEnum"
      }
    },
    {
      "sheet": "FDC PE",
      "cell": "D4",
      "fromInput": "QuotePolicy.publicLiabilityAmount.amount",
      "fieldMap": {
        "numberEnumMap": {
          "10000000": 1,
          "20000000": 2
        },
        "type": "numberEnum"
      }
    },
    {
      "sheet": "FDC PE",
      "cell": "F4",
      "fromInput": "QuotePolicy.paymentPlanType",
      "fieldMap": {
        "numberEnumMap": {
          "Monthly": 2,
          "Singular": 1
        },
        "type": "numberEnum"
      }
    },
    {
      "sheet": "FDC PE",
      "cell": "F20",
      "fromInput": "daysOfRisk"
    },
    {
      "sheet": "FDC PE",
      "cell": "F21",
      "fromInput": "monthsOfRisk"
    },
    {
      "sheet": "FDC PE",
      "cell": "G4",
      "fromInput": "isManualRating"
    },
    {
      "sheet": "FDC PE",
      "cell": "I4",
      "fromInput": "manualPremiumLiability"
    },
    {
      "sheet": "FDC PE",
      "cell": "J4",
      "fromInput": "manualPremiumPersonalAccident"
    },
    {
      "sheet": "FDC PE",
      "cell": "K4",
      "fromInput": "manualPremiumBrokerFee"
    },
    {
      "sheet": "FDC PE",
      "cell": "L4",
      "fromInput": "manualPremiumLegalAddOn"
    },
    {
      "sheet": "FDC PE",
      "cell": "M4",
      "fromInput": "manualCommissionLiability"
    },
    {
      "sheet": "FDC PE",
      "cell": "O4",
      "fromInput": "manualLimitLiability"
    },
    {
      "sheet": "FDC PE",
      "cell": "P4",
      "fromInput": "manualLimitPersonalAccident"
    },
    {
      "sheet": "FDC PE",
      "cell": "Q4",
      "fromInput": "manualExcessLiability"
    },
    {
      "sheet": "FDC PE",
      "cell": "R4",
      "fromInput": "manualExcessPersonalAccident"
    },
    {
      "sheet": "FDC PE",
      "cell": "N4",
      "fromInput": "manualCommissionPersonalAccident"
    }
  ],
  "outputs": [
    {
      "sheet": "FDC PE",
      "cell": "G59",
      "toOutput": "preTaxGWP.total"
    },
    {
      "sheet": "FDC PE",
      "cell": "E72",
      "toOutput": "commission.brokerFee"
    },
    {
      "sheet": "FDC PE",
      "cell": "E52",
      "toOutput": "charge.brokerFee.tax.NRT"
    },
    {
      "sheet": "FDC PE",
      "cell": "L52",
      "toOutput": "refund.brokerFee.tax.NRT"
    },
    {
      "sheet": "FDC PE",
      "cell": "G52",
      "toOutput": "charge.total.tax.NRT"
    },
    {
      "sheet": "FDC PE",
      "cell": "N52",
      "toOutput": "refund.total.tax.NRT"
    },
    {
      "sheet": "FDC PE",
      "cell": "E61",
      "toOutput": "charge.brokerFee.tax.GST"
    },
    {
      "sheet": "FDC PE",
      "cell": "L61",
      "toOutput": "refund.brokerFee.tax.GST"
    },
    {
      "sheet": "FDC PE",
      "cell": "G61",
      "toOutput": "charge.total.tax.GST"
    },
    {
      "sheet": "FDC PE",
      "cell": "N61",
      "toOutput": "refund.total.tax.GST"
    },
    {
      "sheet": "FDC PE",
      "cell": "E53",
      "toOutput": "charge.brokerFee.party.Imalia"
    },
    {
      "sheet": "FDC PE",
      "cell": "L53",
      "toOutput": "refund.brokerFee.party.Imalia"
    },
    {
      "sheet": "FDC PE",
      "cell": "G53",
      "toOutput": "charge.total.party.Imalia"
    },
    {
      "sheet": "FDC PE",
      "cell": "N53",
      "toOutput": "refund.total.party.Imalia"
    },
    {
      "sheet": "FDC PE",
      "cell": "E55",
      "toOutput": "charge.brokerFee.party.DX-Evolution"
    },
    {
      "sheet": "FDC PE",
      "cell": "L55",
      "toOutput": "refund.brokerFee.party.DX-Evolution"
    },
    {
      "sheet": "FDC PE",
      "cell": "G55",
      "toOutput": "charge.total.party.DX-Evolution"
    },
    {
      "sheet": "FDC PE",
      "cell": "N55",
      "toOutput": "refund.total.party.DX-Evolution"
    },
    {
      "sheet": "FDC PE",
      "cell": "E56",
      "toOutput": "charge.brokerFee.party.Stripe"
    },
    {
      "sheet": "FDC PE",
      "cell": "L56",
      "toOutput": "refund.brokerFee.party.Stripe"
    },
    {
      "sheet": "FDC PE",
      "cell": "G56",
      "toOutput": "charge.total.party.Stripe"
    },
    {
      "sheet": "FDC PE",
      "cell": "N56",
      "toOutput": "refund.total.party.Stripe"
    },
    {
      "sheet": "FDC PE",
      "cell": "E59",
      "toOutput": "preTaxGWP.brokerFee"
    },
    {
      "sheet": "FDC PE",
      "cell": "F72",
      "toOutput": "commission.debtCollectorFeeAndTelephoneLegalAdvice"
    },
    {
      "sheet": "FDC PE",
      "cell": "F52",
      "toOutput": "charge.debtCollectorFeeAndTelephoneLegalAdvice.tax.NRT"
    },
    {
      "sheet": "FDC PE",
      "cell": "M52",
      "toOutput": "refund.debtCollectorFeeAndTelephoneLegalAdvice.tax.NRT"
    },
    {
      "sheet": "FDC PE",
      "cell": "F61",
      "toOutput": "charge.debtCollectorFeeAndTelephoneLegalAdvice.tax.GST"
    },
    {
      "sheet": "FDC PE",
      "cell": "M61",
      "toOutput": "refund.debtCollectorFeeAndTelephoneLegalAdvice.tax.GST"
    },
    {
      "sheet": "FDC PE",
      "cell": "F53",
      "toOutput": "charge.debtCollectorFeeAndTelephoneLegalAdvice.party.Imalia"
    },
    {
      "sheet": "FDC PE",
      "cell": "M53",
      "toOutput": "refund.debtCollectorFeeAndTelephoneLegalAdvice.party.Imalia"
    },
    {
      "sheet": "FDC PE",
      "cell": "F55",
      "toOutput": "charge.debtCollectorFeeAndTelephoneLegalAdvice.party.DX-Evolution"
    },
    {
      "sheet": "FDC PE",
      "cell": "M55",
      "toOutput": "refund.debtCollectorFeeAndTelephoneLegalAdvice.party.DX-Evolution"
    },
    {
      "sheet": "FDC PE",
      "cell": "F56",
      "toOutput": "charge.debtCollectorFeeAndTelephoneLegalAdvice.party.Stripe"
    },
    {
      "sheet": "FDC PE",
      "cell": "M56",
      "toOutput": "refund.debtCollectorFeeAndTelephoneLegalAdvice.party.Stripe"
    },
    {
      "sheet": "FDC PE",
      "cell": "F59",
      "toOutput": "preTaxGWP.debtCollectorFeeAndTelephoneLegalAdvice"
    },
    {
      "sheet": "FDC PE",
      "cell": "C72",
      "toOutput": "commission.generalLiability"
    },
    {
      "sheet": "FDC PE",
      "cell": "C52",
      "toOutput": "charge.generalLiability.tax.NRT"
    },
    {
      "sheet": "FDC PE",
      "cell": "J52",
      "toOutput": "refund.generalLiability.tax.NRT"
    },
    {
      "sheet": "FDC PE",
      "cell": "C61",
      "toOutput": "charge.generalLiability.tax.GST"
    },
    {
      "sheet": "FDC PE",
      "cell": "J61",
      "toOutput": "refund.generalLiability.tax.GST"
    },
    {
      "sheet": "FDC PE",
      "cell": "C62",
      "toOutput": "charge.generalLiability.tax.StampDuty"
    },
    {
      "sheet": "FDC PE",
      "cell": "J62",
      "toOutput": "refund.generalLiability.tax.StampDuty"
    },
    {
      "sheet": "FDC PE",
      "cell": "G62",
      "toOutput": "charge.total.tax.StampDuty"
    },
    {
      "sheet": "FDC PE",
      "cell": "N62",
      "toOutput": "refund.total.tax.StampDuty"
    },
    {
      "sheet": "FDC PE",
      "cell": "C53",
      "toOutput": "charge.generalLiability.party.Imalia"
    },
    {
      "sheet": "FDC PE",
      "cell": "J53",
      "toOutput": "refund.generalLiability.party.Imalia"
    },
    {
      "sheet": "FDC PE",
      "cell": "C54",
      "toOutput": "charge.generalLiability.party.JLT"
    },
    {
      "sheet": "FDC PE",
      "cell": "J54",
      "toOutput": "refund.generalLiability.party.JLT"
    },
    {
      "sheet": "FDC PE",
      "cell": "G54",
      "toOutput": "charge.total.party.JLT"
    },
    {
      "sheet": "FDC PE",
      "cell": "N54",
      "toOutput": "refund.total.party.JLT"
    },
    {
      "sheet": "FDC PE",
      "cell": "C55",
      "toOutput": "charge.generalLiability.party.DX-Evolution"
    },
    {
      "sheet": "FDC PE",
      "cell": "J55",
      "toOutput": "refund.generalLiability.party.DX-Evolution"
    },
    {
      "sheet": "FDC PE",
      "cell": "C56",
      "toOutput": "charge.generalLiability.party.Stripe"
    },
    {
      "sheet": "FDC PE",
      "cell": "J56",
      "toOutput": "refund.generalLiability.party.Stripe"
    },
    {
      "sheet": "FDC PE",
      "cell": "C58",
      "toOutput": "charge.generalLiability.party.Neon"
    },
    {
      "sheet": "FDC PE",
      "cell": "J58",
      "toOutput": "refund.generalLiability.party.Neon"
    },
    {
      "sheet": "FDC PE",
      "cell": "G58",
      "toOutput": "charge.total.party.Neon"
    },
    {
      "sheet": "FDC PE",
      "cell": "N58",
      "toOutput": "refund.total.party.Neon"
    },
    {
      "sheet": "FDC PE",
      "cell": "C74",
      "toOutput": "limit.generalLiability"
    },
    {
      "sheet": "FDC PE",
      "cell": "C73",
      "toOutput": "excess.generalLiability"
    },
    {
      "sheet": "FDC PE",
      "cell": "C59",
      "toOutput": "preTaxGWP.generalLiability"
    },
    {
      "sheet": "FDC PE",
      "cell": "D72",
      "toOutput": "commission.personalAccident"
    },
    {
      "sheet": "FDC PE",
      "cell": "D52",
      "toOutput": "charge.personalAccident.tax.NRT"
    },
    {
      "sheet": "FDC PE",
      "cell": "K52",
      "toOutput": "refund.personalAccident.tax.NRT"
    },
    {
      "sheet": "FDC PE",
      "cell": "D61",
      "toOutput": "charge.personalAccident.tax.GST"
    },
    {
      "sheet": "FDC PE",
      "cell": "K61",
      "toOutput": "refund.personalAccident.tax.GST"
    },
    {
      "sheet": "FDC PE",
      "cell": "D62",
      "toOutput": "charge.personalAccident.tax.StampDuty"
    },
    {
      "sheet": "FDC PE",
      "cell": "K62",
      "toOutput": "refund.personalAccident.tax.StampDuty"
    },
    {
      "sheet": "FDC PE",
      "cell": "D53",
      "toOutput": "charge.personalAccident.party.Imalia"
    },
    {
      "sheet": "FDC PE",
      "cell": "K53",
      "toOutput": "refund.personalAccident.party.Imalia"
    },
    {
      "sheet": "FDC PE",
      "cell": "D54",
      "toOutput": "charge.personalAccident.party.JLT"
    },
    {
      "sheet": "FDC PE",
      "cell": "K54",
      "toOutput": "refund.personalAccident.party.JLT"
    },
    {
      "sheet": "FDC PE",
      "cell": "D55",
      "toOutput": "charge.personalAccident.party.DX-Evolution"
    },
    {
      "sheet": "FDC PE",
      "cell": "K55",
      "toOutput": "refund.personalAccident.party.DX-Evolution"
    },
    {
      "sheet": "FDC PE",
      "cell": "D56",
      "toOutput": "charge.personalAccident.party.Stripe"
    },
    {
      "sheet": "FDC PE",
      "cell": "K56",
      "toOutput": "refund.personalAccident.party.Stripe"
    },
    {
      "sheet": "FDC PE",
      "cell": "D57",
      "toOutput": "charge.personalAccident.party.MS-Amlin"
    },
    {
      "sheet": "FDC PE",
      "cell": "K57",
      "toOutput": "refund.personalAccident.party.MS-Amlin"
    },
    {
      "sheet": "FDC PE",
      "cell": "G57",
      "toOutput": "charge.total.party.MS-Amlin"
    },
    {
      "sheet": "FDC PE",
      "cell": "N57",
      "toOutput": "refund.total.party.MS-Amlin"
    },
    {
      "sheet": "FDC PE",
      "cell": "D74",
      "toOutput": "limit.personalAccident"
    },
    {
      "sheet": "FDC PE",
      "cell": "D73",
      "toOutput": "excess.personalAccident"
    },
    {
      "sheet": "FDC PE",
      "cell": "D59",
      "toOutput": "preTaxGWP.personalAccident"
    }
  ],
  "reason": "Model mapping update 1561374390582"
};

function tryFloat(val: string) {
  const ret = parseFloat(val);
  if (isNaN(ret)) return val;
  return ret;
}
// store this in the db later
const configForManualForImaliaDayCare: any = {
  "paymentPlan.preTaxGWPWritten.generalLiability": "manualPremiumLiability",
  "paymentPlan.preTaxGWPWritten.personalAccident": "manualPremiumPersonalAccident",
  "paymentPlan.preTaxGWPWritten.brokerFee": "manualPremiumBrokerFee",
  "paymentPlan.preTaxGWPWritten.debtCollectorFeeAndTelephoneLegalAdvice": "manualPremiumLegalAddOn",
  "paymentPlan.limit.generalLiability": "manualLimitLiability",
  "paymentPlan.limit.personalAccident": "manualLimitPersonalAccident",
  "paymentPlan.excess.generalLiability": "manualExcessLiability",
  "paymentPlan.excess.personalAccident": "manualExcessPersonalAccident",
  "paymentPlan.commission.personalAccident": "manualCommissionPersonalAccident",
  "paymentPlan.commission.generalLiability": "manualCommissionLiability"
};
// import { contextComputedField_computer } from "../src/providers/price-engine/engine";

function calculateRestOfPaymentPlan(inputs: any, paymentPlan: IPaymentPlanOutput) {
  Object.keys(configForManualForImaliaDayCare).forEach((fieldPath: string) => {
    // compute fieldPath value from paymentPan
    const value = tryFloat(contextComputedField_computer(fieldPath, {paymentPlan: paymentPlan}));
    const key = configForManualForImaliaDayCare[fieldPath];
    console.log("fieldPath", key, value);
    inputs[key] = value;
  });
  return inputs;
}

function calculateRestOfPaymentPlan_clone(results: IPaymentPlanOutput, originalContext: any) {
  const cloneResults = JSON.parse(JSON.stringify(results));
  const cloneContext = JSON.parse(JSON.stringify(originalContext));
  const outputs = calculateRestOfPaymentPlan(cloneContext, cloneResults);
  return outputs;
}

describe("test for manual rating route", () => {

  const inputs: ISpreadSheetInput[] = modelConfig.inputs;
  const outputs: ISpreadSheetOutput[] = modelConfig.outputs;
  const modelFileData: Buffer = fs.readFileSync("./test/data/pricing-engine/20190617 NeonImalia_FamilyDayCare_Pricing Model  V0.0.19.xlsx");


  it("Should stay stable", async () => {
    const randomPolicy: IQuotePolicy = (await gcdsInstance.queryManyLimited("QuotePolicy", [
      { propertyName: "policy", comparator: <any>"=", value: true },
      { propertyName: "endDate", comparator: <any>">", value: Date.now() }
    ], 1)).results[0];
    const customer: ICustomer = await gcdsInstance.getOne(["Customer", randomPolicy.customerId]);

    if (!randomPolicy.paymentPlanType) return;


    function calculateStandardRating(): IPaymentPlanOutput {
      const standardContext: any = {
        QuotePolicy: randomPolicy,
        Customer: customer,
        isManualRating: 0,
        daysOfRisk: 0,
        monthsOfRisk: 0,
      };
      console.log("STANDARD RESULTS CONTEXT ----------------------------------------");
      console.log("STANDARD RESULTS CONTEXT ----------------------------------------");
      console.log("STANDARD RESULTS CONTEXT ----------------------------------------");
      console.log("STANDARD RESULTS CONTEXT ----------------------------------------");
      console.log(JSON.stringify(standardContext, null, 4));
      return eval_model(modelFileData, inputs, outputs, standardContext);
    }

    // should calculate stardard
    console.log("STANDARD RESULTS ----------------------------------------");
    console.log("STANDARD RESULTS ----------------------------------------");
    console.log("STANDARD RESULTS ----------------------------------------");
    console.log("STANDARD RESULTS ----------------------------------------");
    const standardRating = await calculateStandardRating();
    console.log(JSON.stringify(standardRating, null, 4));

    function calculateBespokeRating(standardRating: IPaymentPlanOutput, args: any): IPaymentPlanOutput {
      // should calculate bespoke
      const bespokeDefaultContext: any = {
        QuotePolicy: randomPolicy,
        Customer: customer,
        isManualRating: 1,
        daysOfRisk: 0,
        monthsOfRisk: 1,
      };
      // modify defaults based on user preferences
      const bespokeCorrectedContext =  calculateRestOfPaymentPlan_clone(standardRating, bespokeDefaultContext);
      Object.keys(args).forEach((argName: string) => {
        bespokeCorrectedContext[argName] = args[argName];
      });
      console.log("BESPOKE RESULTS CONTEXT ------------------------------------------");
      console.log("BESPOKE RESULTS CONTEXT ------------------------------------------");
      console.log("BESPOKE RESULTS CONTEXT ------------------------------------------");
      console.log("BESPOKE RESULTS CONTEXT ------------------------------------------");
      console.log("correctBespokeContext", JSON.stringify(bespokeCorrectedContext, null, 4));
      return eval_model(modelFileData, inputs, outputs, bespokeCorrectedContext);
    }

    console.log("BESPOKE RESULTS ------------------------------------------");
    console.log("BESPOKE RESULTS ------------------------------------------");
    console.log("BESPOKE RESULTS ------------------------------------------");
    console.log("BESPOKE RESULTS ------------------------------------------");
    console.log(JSON.stringify(await calculateBespokeRating(standardRating, {manualExcessLiability: 100000000000}), null, 4));



  });

  it("should test api route util", async() => {
    const randomPolicy: IQuotePolicy = (await gcdsInstance.queryManyLimited("QuotePolicy", [
      { propertyName: "policy", comparator: <any>"=", value: true },
      { propertyName: "endDate", comparator: <any>">", value: Date.now() }
    ], 1)).results[0];
    const a = await eval_quote_model_bespoke(randomPolicy._name, {"Customer.providesOutOfHomeCare": true}, {manualExcessLiability: 10000000000000000000000000000});
    console.log("-------------------------------------------");
    console.log(JSON.stringify(a, null, 4));
  });
});










// JUNK IGNORE


  /*

          manualPremiumLiability (NRT + ESL + STAMP + Stripe + imalia + neon + ....)
          // GWP (imlia +neon + .... + NRT)
          // paymentPlan.preTaxGWPWritten.generalLiability manualPremiumLiability
          // paymentPlan.preTaxGWPWritten.personalAccident manualPremiumPersonalAccident
          // paymentPlan.preTaxGWPWritten.brokerFee manualPremiumBrokerFee
          // paymentPlan.preTaxGWPWritten.debtCollectorFeeAndTelephoneLegalAdvice manualPremiumLegalAddOn
          // paymentPlan.limit.generalLiability manualLimitLiability
          // paymentPlan.limit.personalAccident manualLimitPersonalAccident
          // paymentPlan.excess.generalLiability manualExcessLiability
          // paymentPlan.excess.personalAccident manualExcessPersonalAccident
          // paymentPlan.commission.personalAccident manualCommissionPersonalAccident
          // paymentPlan.commission.generalLiability manualCommissionLiability
  *//*
 if (inputs.manualPremiumLiability === undefined || isNaN(parseFloat(inputs.manualPremiumLiability))) {
  inputs.manualPremiumLiability = parseFloat(paymentPlan.preTaxGWPWritten.generalLiability); // calculatePaymentItem(paymentPlan.written.generalLiability); // 278;
}
if (inputs.manualPremiumPersonalAccident === undefined || isNaN(parseFloat(inputs.manualPremiumPersonalAccident))) {
  inputs.manualPremiumPersonalAccident = parseFloat(paymentPlan.preTaxGWPWritten.personalAccident);  // calculatePaymentItem(paymentPlan.written.personalAccident); // 15.00;
}
if (inputs.manualPremiumBrokerFee === undefined || isNaN(parseFloat(inputs.manualPremiumBrokerFee))) {
  inputs.manualPremiumBrokerFee = parseFloat(paymentPlan.preTaxGWPWritten.brokerFee); // calculatePaymentItem(paymentPlan.written.brokerFee); // 55.40;
}
if (inputs.manualPremiumLegalAddOn === undefined || isNaN(parseFloat(inputs.manualPremiumLegalAddOn))) {
  inputs.manualPremiumLegalAddOn = parseFloat(paymentPlan.preTaxGWPWritten.debtCollectorFeeAndTelephoneLegalAdvice); // calculatePaymentItem(paymentPlan.written.debtCollectorFeeAndTelephoneLegalAdvice); // 50.4;
}

if (inputs.manualLimitLiability === undefined || isNaN(parseFloat(inputs.manualLimitLiability))) {
  inputs.manualLimitLiability = parseFloat(paymentPlan.limit.generalLiability);
}
if (inputs.manualLimitPersonalAccident === undefined || isNaN(parseFloat(inputs.manualLimitPersonalAccident))) {
  inputs.manualLimitPersonalAccident = parseFloat(paymentPlan.limit.personalAccident);
}
if (inputs.manualExcessLiability === undefined || isNaN(parseFloat(inputs.manualExcessLiability))) {
  inputs.manualExcessLiability = parseFloat(paymentPlan.excess.generalLiability);
}
if (inputs.manualExcessPersonalAccident === undefined || isNaN(parseFloat(inputs.manualExcessPersonalAccident))) {
  inputs.manualExcessPersonalAccident = parseFloat(paymentPlan.excess.personalAccident);
}
if (inputs.manualCommissionPersonalAccident === undefined || isNaN(parseFloat(inputs.manualCommissionPersonalAccident))) {
  inputs.manualCommissionPersonalAccident = parseFloat(paymentPlan.commission.personalAccident);
}
if (inputs.manualCommissionLiability === undefined || isNaN(parseFloat(inputs.manualCommissionLiability))) {
  inputs.manualCommissionLiability = parseFloat(paymentPlan.commission.generalLiability);
}
*/
/*
  {
    "sheet": "FDC PE",
    "cell": "M4",
    "fromInput": "manualCommissionBrokerFee"
  },
  {
    "sheet": "FDC PE",
    "cell": "M4",
    "fromInput": "debtCollectorFeeAndTelephoneLegalAdvice"
  },
*/

// broker fee
/*if (inputs.manualCommissionBrokerFee === undefined || isNaN(parseFloat(inputs.manualCommissionBrokerFee))) {
  inputs.manualCommissionBrokerFee = parseFloat(paymentPlan.commission.brokerFee);
}
// legal add on
if (inputs.debtCollectorFeeAndTelephoneLegalAdvice === undefined || isNaN(parseFloat(inputs.debtCollectorFeeAndTelephoneLegalAdvice))) {
  inputs.debtCollectorFeeAndTelephoneLegalAdvice = parseFloat(paymentPlan.commission.debtCollectorFeeAndTelephoneLegalAdvice);
}*/

