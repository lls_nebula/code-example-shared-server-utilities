
import { expect } from "chai";
import { it, describe } from "mocha";
import { createAllEmailContent } from "../src";
import { gcdsInstance, TGFSKey, DatastoreKey } from "@vaultspace/gdatastore-v1";
import { applicationKind } from "../src/configs/kinds";
import { buildContentForFdcPersonalEmail, buildContentForFdcBroadcastingEmail } from "../src/providers/email/contentBuilder";

import {
    cocMoch,
    fdcScheduleMoch,
    testCustomer,
    testContact,
    testQuote,
    testApp,
    testAppFunnel,
    testEmployee,
        } from "../moch";
import { IEmailContent, IEmailContentBuilderArguments, availableEmailTypeObj, availableAppNameObj } from "../src/models";
import { Z_UNKNOWN } from "zlib";
import fs from "fs";
// const applicationKind = "ApplicationTest";

const generatePersonalTestBody = async (args: IEmailContentBuilderArguments): Promise<IEmailContent> => {
    const key: TGFSKey = [applicationKind, args.Application.name, "EmailContent", args.emailType];

    const template = await gcdsInstance.getOne(key) as IEmailContent;

    return {
        subject: template.subject,
        body: buildContentForFdcPersonalEmail(template.body, args)
    }
};

const generateNotificationTestBody = async (args: IEmailContentBuilderArguments): Promise<IEmailContent> => {
    const key: TGFSKey = [applicationKind, args.Application.name, "EmailContent", args.emailType];
    console.log(key);

    const template = await gcdsInstance.getOne(key) as IEmailContent;

    return buildContentForFdcBroadcastingEmail(template, args);
};

const deleteTestTemplates = () => {
    const deleteKeys: Array<DatastoreKey> = [];

    for (const appName in availableAppNameObj) {
        for (const emailType in availableEmailTypeObj) {
            deleteKeys.push({
                kind: applicationKind,
                name: emailType,
                path: [applicationKind, appName, "EmailContent", emailType],
            });
        }
    }

    return gcdsInstance.deleteMany(deleteKeys);
};

describe("Email body replacement", () => {
    it("Should replace email bodies correctly", async () => {

        await createAllEmailContent();

        const mochArgs = {
            notificationType: "personal",
            Customer: testCustomer,
            Employee: testEmployee,
            Contact: testContact,
            QuotePolicy: testQuote,
            Application: testApp,
            ApplicationFunnel: testAppFunnel,
            options: {
                coc: cocMoch,
                fdcSchedule: fdcScheduleMoch,
                invoice_url: "some/invoice/url",
                policyExpiryDate: "10.05.2019",
                password: "password",
                stripeRetries: 1,
                stripeRetriesDif: 3,
                markedForCancellationDate: "11.05.2019",
                // failedRefund: ["", "", "", {refundAmount: "222.2"}],
                // failedRefund: ["", "", "", [{attemptedRefund: true, refundAmount: "222.2"}]]
            },
        };

        const emailContentList: IEmailContent[] = await Promise.all([
            generatePersonalTestBody({ emailType: "failedPaymentEmailWithLittleStripeRetries", ...mochArgs } as unknown as IEmailContentBuilderArguments),
            generatePersonalTestBody({ emailType: "paymentCancellationEmail", ...mochArgs } as unknown as IEmailContentBuilderArguments),
            generatePersonalTestBody({ emailType: "failedPaymentEmailWithManyStripeRetries", ...mochArgs } as unknown as IEmailContentBuilderArguments),
            generatePersonalTestBody({ emailType: "inviteEmail", ...mochArgs } as unknown as IEmailContentBuilderArguments),
            generatePersonalTestBody({ emailType: "refundEmail", options: {
                refundAmount: "222.2"
            }, ...mochArgs } as unknown as IEmailContentBuilderArguments),
            generatePersonalTestBody({
                emailType: "refundEmail2",
                options: {
                    failedRefund: ["", "", "", {refundAmount: "222.2"}]
                },
                ...mochArgs } as unknown as IEmailContentBuilderArguments),
            generatePersonalTestBody({
                emailType: "failedRefundEmail",
                options: {
                    failedRefund: ["", "", "", [{attemptedRefund: true, refundAmount: "222.2"}]]
                },
                ...mochArgs
            } as unknown as IEmailContentBuilderArguments),
            generatePersonalTestBody({ emailType: "firstInvoiceEmail", ...mochArgs } as unknown as IEmailContentBuilderArguments),
            generatePersonalTestBody({ emailType: "invoiceEmail", ...mochArgs } as unknown as IEmailContentBuilderArguments),
            generatePersonalTestBody({ emailType: "finalInvoiceEmail", ...mochArgs } as unknown as IEmailContentBuilderArguments),
            generatePersonalTestBody({ emailType: "paymentDisputeEmail", ...mochArgs } as unknown as IEmailContentBuilderArguments),
            generatePersonalTestBody({ emailType: "referralEmail", ...mochArgs } as unknown as IEmailContentBuilderArguments),
            generatePersonalTestBody({ emailType: "referralEmail2", ...mochArgs } as unknown as IEmailContentBuilderArguments),

            generateNotificationTestBody({emailType: "referralNotification", ...mochArgs } as unknown as IEmailContentBuilderArguments),
            generateNotificationTestBody({emailType: "refundFailureNotification", ...mochArgs } as unknown as IEmailContentBuilderArguments),
            generateNotificationTestBody({emailType: "paymentFailureNotification", ...mochArgs } as unknown as IEmailContentBuilderArguments),
            generateNotificationTestBody({emailType: "paymentDisputeNotification", ...mochArgs } as unknown as IEmailContentBuilderArguments),
            generateNotificationTestBody({emailType: "paymentDiscrepancyNotification", ...mochArgs } as unknown as IEmailContentBuilderArguments)
        ]);

        let html: string = `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Email content</title>
        </head>
        <body style="background-color: #d5f3f3;">`;
        emailContentList.forEach(email => {
            html += `
                <div style="width: 1000px; margin: 22px auto; clear: both; padding: 22px; border: 1px solid black; background-color: white;">
                    <b>SUBJECT: </b>${email.subject}<br>
                    <b>BODY: </b>${email.body}<br>
                </div>
            `;
            console.log(`SUBJECT: ${email.subject}`);
        });
        html += `</body>
        </html>`;

        await fs.writeFile("./test/data/email.content.html", html, (err) => {
            console.error(err);
        });

        // await deleteTestTemplates();
    });
});