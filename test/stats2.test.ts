import { updateAllStatsForKindEntity,
     detectActivePolicyConversion,
     detectRenewalLess90Conversion,
     detectRenewalLess60Conversion,
     detectRenewalLess45Conversion,
     detectRenewalLess30Conversion,
     detectRenewalLess15Conversion,
     detectRenewalLess7Conversion,
     detectExpiredQuotesConversion,
     detectExpiredPolicyConversion,
     initStatsForKind,
 } from "../src/providers/stats";
 import { computeRefresh } from "../src/providers/events";
import { expect } from "chai";
import { it, describe } from "mocha";
import { IQuotePolicy } from "@vaultspace/neon-shared-models";
import { gcdsInstance } from "../src/gcds-instance";
import { firestore } from "@vaultspace/firebase-server";

describe("stats 2", () => {

   /* it("should increment from undefine", async() => {
        const namedQuery = "All";
        const appName = "imaliaDaycare";
        const kind = "QuotePolicy";
        // const inc = firebaseAdmin.firestore.FieldValue["increment"](1);
        const inc = 1;
        console.log("inc", JSON.stringify(inc));
        await firestore.collection("StatisticsRead").doc(appName).collection("Kind").doc(kind).update(
            {
                [namedQuery]: helperMethods.increment,
            }); // , {merge: true}
        });*/

    it("should init stats", async() => {
        await initStatsForKind("QuotePolicy");
        await initStatsForKind("Customer");
    });

    it("should refresh", async() => {
        await computeRefresh("imaliaDaycare");
        console.log("END COMPUTE REFRESH");
    });

    it("should run stats", async() => {
        const quotes = await firestore.collection("QuotePolicy").get();
        const ids: Array<any> = [];
        quotes.forEach((querySnap) => {
            console.log(querySnap.id);
            ids.push({i: querySnap.id, d: querySnap.data()});
        });
        const allSimpleDeltas = await Promise.all(ids.map(async (id) => {
            return await updateAllStatsForKindEntity("QuotePolicy", id.d);
        }));
        // const data: IQuotePolicy = await gcdsInstance.getOne(["QuotePolicy", "91b62001-66f2-425e-a32d-ad6225e52101"]);
        // await updateAllStatsForKindEntity("QuotePolicy", data, Date.now());

        console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#");
        console.log(JSON.stringify(allSimpleDeltas, null, 4));
        console.log("---------------------------------------------------");
    });

    it("should detect active policy conversion", async() => {
        await detectActivePolicyConversion();
    });

    it("should detect ExpiredQuotesConversion", async() => {
        await detectExpiredQuotesConversion();
    });

    it("should detect ExpiredPolicyConversion", async() => {
        await detectExpiredPolicyConversion();
    });

    it("should detect RenewalLess7Conversion", async() => {
        await detectRenewalLess7Conversion();
    });

    it("should detect RenewalLess15Conversion", async() => {
        await detectRenewalLess15Conversion();
    });


    it("should detect RenewalLess30Conversion", async() => {
        await detectRenewalLess30Conversion();
    });

    it("should detect RenewalLess45Conversion", async() => {
        await detectRenewalLess45Conversion();
    });

    it("should detect RenewalLess60Conversion", async() => {
        await detectRenewalLess60Conversion();
    });

    it("should detect RenewalLess90Conversion", async() => {
        await detectRenewalLess90Conversion();
    });
});