import { mailerResponseType } from "@vaultspace/emailer/dist/src/models";
import { attachDocuments, storeCommonDocument } from "../src/providers/pdf-storager";
import * as pdf from "@vaultspace/pdf";
import { bucket } from "@vaultspace/firebase-server";
import { gcdsInstance } from "../src/gcds-instance";
import { IGDSKey, DatastoreKey } from "@vaultspace/gdatastore-v1";
import { ICustomer, IResponse, IContact, EConsumerType, EConsumerStatus, IQuotePolicy, IApplication, IApplicationFunnel, ERegion } from "@vaultspace/neon-shared-models";
import { quotePolicyKind } from "../src/configs/kinds";
import { expect } from "chai";
import { it, describe } from "mocha";
import { cocMoch, fdcScheduleMoch, testQuote } from "../moch";
import { fetchQuote, buildEmailAndSendToUser, fetchApplicationFunnel, createAllEmailContent } from "../src";
import { v4 as uuid } from "uuid";
import { generateAttachmentsForFdcPersonal } from "../src/providers/email/attachmentsGenerator/imaliaDaycare/imaliaDaycarePersonal";

const customer: ICustomer = {
    id: uuid(),
    type: EConsumerType.Consumer,
    exactEntityType: null,
    status: EConsumerStatus.Inactive,
    createdAt: new Date().getTime(),
    // additionalParties: ["20", "000", "000"],
    principalPlaceOfBusiness: {
        street: "street",
        line2: "line2 content",
        postCode: "30000"
    },
    region: ERegion.AustraliaCapitalTerritory
};

const contact: IContact = {
    customerId: customer.id,
    name: "Lee Logan Steiner",
    email: "steinerleelogan@gmail.com"
    // email: "502isok@gmail.com"
};

const quoteKey: IGDSKey = {name: testQuote.quoteId, kind: quotePolicyKind};
const deleteKey: DatastoreKey = {
    kind: quotePolicyKind,
    name: testQuote.quoteId,
    path: [quotePolicyKind, testQuote.quoteId],
};


describe("Email engine", () => {
    beforeEach(async () => {
        await createAllEmailContent();
    });

    it("Should send inviteEmail", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "inviteEmail",
            notificationType: "personal",
            Customer: customer,
            Contact: contact,
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: {
                password: "eDdwqe44dsf3f321s31s_221_000000"
            }
        },
        [],
        [],
        [contact.email]
        ).then((res) => {
            console.log(`The mail was sent to ${contact.email}`);
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a user the inviteEmail", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });


        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send firstInvoiceEmail", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        await generateAttachmentsForFdcPersonal({
            emailType: "firstInvoiceEmail",
            notificationType: "personal",
            Application: application,
            ApplicationFunnel: applicationFunnel,
            Customer: customer,
            Contact: contact,
            QuotePolicy: quotePolicy,
            options: {
                invoice_url: "https://testdomain.com/invoice_url/some/url"
            },
        });

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "firstInvoiceEmail",
            notificationType: "personal",
            Customer: customer,
            Contact: contact,
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: {
                invoice_url: "https://testdomain.com/invoice_url/some/url"
            }
        },
        [],
        [],
        [contact.email]
        ).then((res) => {
            console.log(`The mail was sent to ${contact.email}`);
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a user the firstInvoiceEmail", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });


        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send finalInvoiceEmail", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "finalInvoiceEmail",
            notificationType: "personal",
            Customer: customer,
            Contact: contact,
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: {
                policyExpiryDate: new Date("12/12/2020").toUTCString(),
                invoice_url: "https://testdomain.com/invoice_url/some/url"
            }
        },
        [],
        [],
        [contact.email]
        ).then((res) => {
            console.log(`The mail was sent to ${contact.email}`);
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a user the finalInvoiceEmail", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });


        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send invoiceEmail", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "invoiceEmail",
            notificationType: "personal",
            Customer: customer,
            Contact: contact,
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: {
                // policyExpiryDate: new Date("12/12/2020").toUTCString(),
                invoice_url: "https://testdomain.com/invoice_url/some/url"
            }
        },
        [],
        [],
        [contact.email]
        ).then((res) => {
            console.log(`The mail was sent to ${contact.email}`);
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a user the invoiceEmail", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });


        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send failedPaymentEmailWithLittleStripeRetries", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "failedPaymentEmailWithLittleStripeRetries",
            notificationType: "personal",
            Customer: customer,
            Contact: contact,
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: {
                markedForCancellationDate: new Date("12/12/2020").toUTCString()
            }
        },
        [],
        [],
        [contact.email]
        ).then((res) => {
            console.log(`The mail was sent to ${contact.email}`);
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a user the failedPaymentEmailWithLittleStripeRetries", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });


        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send failedPaymentEmailWithManyStripeRetries", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "failedPaymentEmailWithManyStripeRetries",
            notificationType: "personal",
            Customer: customer,
            Contact: contact,
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: {
                markedForCancellationDate: new Date("12/12/2020").toUTCString()
            }
        },
        [],
        [],
        [contact.email]
        ).then((res) => {
            console.log(`The mail was sent to ${contact.email}`);
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a user the failedPaymentEmailWithManyStripeRetries", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });


        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send paymentCancellationEmail", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "paymentCancellationEmail",
            notificationType: "personal",
            Customer: customer,
            Contact: contact,
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: {
                policyExpiryDate: new Date("12/12/2020").toUTCString()
            }
        },
        [],
        [],
        [contact.email]
        ).then((res) => {
            console.log(`The mail was sent to ${contact.email}`);
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a user the paymentCancellationEmail", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });


        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send refundEmail", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "refundEmail",
            notificationType: "personal",
            Customer: customer,
            Contact: contact,
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: {
                // firebaseUserId: "552d53-2342c-234dd4-23442",
                failedRefund: ["", "", "", {refundAmount: "222.2"}]
            }
        },
        [],
        [],
        [contact.email]
        ).then((res) => {
            console.log(`The mail was sent to ${contact.email}`);
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a user the refundEmail", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });


        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send refundEmail2", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "refundEmail2",
            notificationType: "personal",
            Customer: customer,
            Contact: contact,
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: {
                failedRefund: ["", "", "", {refundAmount: "222.2"}]
            }
        },
        [],
        [],
        [contact.email]
        ).then((res) => {
            console.log(`The mail was sent to ${contact.email}`);
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a user the refundEmail2", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });


        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send failedRefundEmail", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "failedRefundEmail",
            notificationType: "personal",
            Customer: customer,
            Contact: contact,
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: {
                failedRefund: ["", "", "", {refundAmount: "222.2"}]
            }
        },
        [],
        [],
        [contact.email]
        ).then((res) => {
            console.log(`The mail was sent to ${contact.email}`);
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a user the failedRefundEmail", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });


        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send referralEmail", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "referralEmail",
            notificationType: "personal",
            Customer: customer,
            Contact: contact,
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: {}
        },
        [],
        [],
        [contact.email]
        ).then((res) => {
            console.log(`The mail was sent to ${contact.email}`);
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a user the referralEmail", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });

        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send referralEmail2", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "referralEmail2",
            notificationType: "personal",
            Customer: customer,
            Contact: contact,
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: {}
        },
        [],
        [],
        [contact.email]
        ).then((res) => {
            console.log(`The mail was sent to ${contact.email}`);
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a user the referralEmail2", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });

        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send paymentDisputeEmail", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "paymentDisputeEmail",
            notificationType: "personal",
            Customer: customer,
            Contact: contact,
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: {}
        },
        [],
        [],
        [contact.email]
        ).then((res) => {
            console.log(`The mail was sent to ${contact.email}`);
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a user the paymentDisputeEmail", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });

        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send the emali broadcast related to paymentDisputeNotification", async () => {
        const responseMoc: any = [
            "0885fb92-fc6e-4165-b683-53840af09ecf", // customerId
            "80887f68-5574-4121-a7e4-cc675bb70dd4", // policyId
            "ch_1EPn9yK7QGBd6pRJ1bIXHEps", // chargeId
            { // response
                created: 1326853478,
                livemode: false,
                id: "evt_00000000000000",
                type: "charge.dispute.created",
                object: "event",
                request: null,
                pending_webhooks: 1,
                api_version: "2018-07-27",
                data: {
                    "created": 1326853478,
                    "livemode": false,
                    "id": "evt_00000000000000",
                    "type": "charge.dispute.created",
                    "object": "event",
                    "request": null,
                    "pending_webhooks": 1,
                    "api_version": "2018-07-27",
                    "data": {
                        "id": "dp_00000000000000",
                        "object": "dispute",
                        "amount": 1000,
                        "balance_transaction": "txn_00000000000000",
                        "balance_transactions": [],
                        "charge": "ch_00000000000000",
                        "created": 1557758729,
                        "currency": "aud",
                        "evidence": {
                            "access_activity_log": null,
                            "billing_address": null,
                            "cancellation_policy": null,
                            "cancellation_policy_disclosure": null,
                            "cancellation_rebuttal": null,
                            "customer_communication": null,
                            "customer_email_address": null,
                            "customer_name": null,
                            "customer_purchase_ip": null,
                            "customer_signature": null,
                            "duplicate_charge_documentation": null,
                            "duplicate_charge_explanation": null,
                            "duplicate_charge_id": null,
                            "product_description": null,
                            "receipt": null,
                            "refund_policy": null,
                            "refund_policy_disclosure": null,
                            "refund_refusal_explanation": null,
                            "service_date": null,
                            "service_documentation": null,
                            "shipping_address": null,
                            "shipping_carrier": null,
                            "shipping_date": null,
                            "shipping_documentation": null,
                            "shipping_tracking_number": null,
                            "uncategorized_file": null,
                            "uncategorized_text": null
                        },
                        "evidence_details": {
                            "due_by": 1559433599,
                            "has_evidence": false,
                            "past_due": false,
                            "submission_count": 0
                        },
                        "is_charge_refundable": true,
                        "livemode": false,
                        "metadata": { },
                        "reason": "general",
                        "status": "needs_response"
                    }
                }
            }
        ];

        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "paymentDisputeNotification",
            notificationType: "broadcasting",
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: {
                chargeDispute: {
                    customerId: responseMoc[0],
                    policyId: responseMoc[1],
                    chargeId: responseMoc[2],
                    response: responseMoc[3]
                }
            }
        }).then((res) => {
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a broadcast related to paymentDisputeNotification", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });

        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send the emali broadcast related to refundFailureNotification", async () => {
        const responseMoc: any = [
            "0885fb92-fc6e-4165-b683-53840af09ecf", // customerId
            "80887f68-5574-4121-a7e4-cc675bb70dd4", // policyId
            "ch_1EPn9yK7QGBd6pRJ1bIXHEps", // chargeId
            { // response
                created: 1326853478,
                livemode: false,
                id: "evt_00000000000000",
                type: "charge.refund.updated",
                object: "event",
                request: null,
                pending_webhooks: 1,
                api_version: "2018-07-27",
                data: {
                    "created": 1326853478,
                    "livemode": false,
                    "id": "evt_00000000000000",
                    "type": "charge.refund.updated",
                    "object": "event",
                    "request": null,
                    "pending_webhooks": 1,
                    "api_version": "2018-07-27",
                    "data": {
                        "object": {
                            "id": "re_00000000000000",
                            "object": "refund",
                            "amount": 6111,
                            "balance_transaction": "txn_00000000000000",
                            "charge": "ch_00000000000000",
                            "created": 1552300238,
                            "currency": "aud",
                            "metadata": { },
                            "reason": null,
                            "receipt_number": null,
                            "source_transfer_reversal": null,
                            "status": "succeeded",
                            "transfer_reversal": null
                        },
                        "previous_attributes": {
                            "metadata": {
                                "order_id": "old_order_id"
                            }
                        }
                    }
                }
            }
        ];

        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "refundFailureNotification",
            notificationType: "broadcasting",
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: {
                failedRefund: {
                    customerId: responseMoc[0],
                    policyId: responseMoc[1],
                    chargeId: responseMoc[2],
                    response: responseMoc[3]
                }
            }
        }).then((res) => {
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a broadcast related to refundFailureNotification", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });

        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send the emali broadcast related to referralNotification", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "referralNotification",
            notificationType: "broadcasting",
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: { }
        }).then((res) => {
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a broadcast related to referralNotification", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });

        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send the emali broadcast related to paymentFailureNotification", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "paymentFailureNotification",
            notificationType: "broadcasting",
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: { }
        }).then((res) => {
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a broadcast related to paymentFailureNotification", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });

        expect(result).to.equal(true);
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    it("Should send the emali broadcast related to paymentDiscrepancyNotification", async () => {
        await gcdsInstance.createOne(quoteKey, testQuote);

        const quotePolicy: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        const application: IApplication = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        const applicationFunnel: IApplicationFunnel = await fetchApplicationFunnel(application.name);

        const result: boolean = await buildEmailAndSendToUser(
        "noreply",
        {
            emailType: "paymentDiscrepancyNotification",
            notificationType: "broadcasting",
            QuotePolicy: quotePolicy,
            Application: application,
            ApplicationFunnel: applicationFunnel,
            options: { }
        }).then((res) => {
            console.log(res);
            return res[0].success;
        }).catch((err: any) => {
            console.error("Failed to send a broadcast related to paymentDiscrepancyNotification", err);
            return null;
        });

        await gcdsInstance.deleteOne(deleteKey).then(() => {
            console.info("test QuotePolicy entity deleted...");
        });

        expect(result).to.equal(true);
    });
});