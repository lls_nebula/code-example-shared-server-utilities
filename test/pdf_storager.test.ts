import { attachDocuments, storeCommonDocument } from "../src/providers/pdf-storager";
import * as pdf from "@vaultspace/pdf";
import { bucket } from "@vaultspace/firebase-server";
import { gcdsInstance } from "../src/gcds-instance";
import { IGDSKey, DatastoreKey } from "@vaultspace/gdatastore-v1";
import { IQuotePolicy } from "@vaultspace/neon-shared-models";
import { quotePolicyKind } from "../src/configs/kinds";
import { expect } from "chai";
import { it, describe } from "mocha";
import { cocMoch, fdcScheduleMoch, testQuote } from "../moch";
import { fetchQuote } from "../src";



describe("PDF Storager", () => {


    it("Should attach necessery pdf documents to QuotePolicy entity", async () => {

        const quoteKey: IGDSKey = {name: testQuote.quoteId, kind: quotePolicyKind};
        const deleteKey: DatastoreKey = {
            kind: quotePolicyKind,
            name: testQuote.quoteId,
            path: [quotePolicyKind, testQuote.quoteId],
        };

        await gcdsInstance.createOne(quoteKey, testQuote);
        console.info("test quote created...");

        await Promise.all([
            pdf.createFdcCombinedLiabilityHtml().then((buf) => {
                return storeCommonDocument(testQuote.systemApplication, "fdcLiabilityPolicy", buf);
            }).then(() => {
                console.info("liability policy stored...");
            }),

            pdf.createFdcPersonalAccidentHtml().then((buf) => {
                return storeCommonDocument(testQuote.systemApplication, "fdcPersonalAccidentPolicy", buf);
            }).then(() => {
                console.info("personal accident policy stored...");
            }),
        ]);

        await attachDocuments(testQuote.quoteId, {
            coc: <any>cocMoch,
            schedule: <any>fdcScheduleMoch,
        });
        console.info("dynamic docs stored...");
        console.info("docs attached to QuotePolicy entity...");

        const quote: IQuotePolicy = await fetchQuote(testQuote.quoteId);
        console.info("test QuotePolicy fetched for comparison...");

        console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        console.log(quote.storedDocuments);
        console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");


        await Promise.all([
            gcdsInstance.deleteOne(deleteKey).then(() => {
                console.info("test QuotePolicy entity deleted...");
            }),

            bucket.getFiles({directory: `tests/pdfDocumentation`}).then(files => Promise.all([
                files[0].map(file => file.delete())
            ])).then(() => console.info("test docs deleted...")),
        ]);

        const expected = [
            `tests/pdfDocumentation/${testQuote.systemApplication}/personal/fdcLiabilityPolicy.pdf`,
            `tests/pdfDocumentation/${testQuote.systemApplication}/static/fdcPersonalAccidentPolicy.pdf`,
            `/tests/pdfDocumentation/${testQuote.systemApplication}/personal/customers/${testQuote.customerId}/quotes/${testQuote.quoteId}/FdcSchedule.pdf`,
            `/tests/pdfDocumentation/${testQuote.systemApplication}/personal/customers/${testQuote.customerId}/quotes/${testQuote.quoteId}/CoC.pdf`,
        ];

        expect(JSON.stringify(expected)).to.equal(JSON.stringify(quote.storedDocuments));

    });
});
