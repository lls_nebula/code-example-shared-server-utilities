import { it, describe } from "mocha";
import chai = require("chai");
const expect = chai.expect;
import chaiAsPromised = require("chai-as-promised");
import * as deepDiff from "deep-diff";
import { DeepDiff } from "../src/util/diff";

describe("diff engine", () => {
    // 20190529 NeonImalia_HomeCare_Pricing Model  V0.0.14.xlsx

    /*
diff { a: { type: 'deleted', data: 1 },
  b: { type: 'created', data: 2 },
  c: { type: 'created', data: { d: 1 } } }

    */
    it("calculate reversable diff", async () => {
        const _old = {a: 1, f: 1, z: "a"};
        const _new = {b: 2, c: {d: 1}, f: 2, z: "a"};

        const _newClone = JSON.parse(JSON.stringify(_new));
        const _oldClone = JSON.parse(JSON.stringify(_old));

        const diffs =  deepDiff.diff(_new, _old);


        console.log("apply difference");

        console.log("----------------------------");

        console.log("new", _new);
        console.log("old", _old);

        console.log("diff", JSON.stringify(diffs, null, 4));

        console.log("----------------------------");

        const result = _newClone;
        diffs.forEach(function (it) {
            deepDiff.applyChange(result, _new, it);
        });

        console.log("diffMapper.apply(_new, diff)", result);
        console.log("old", _old);
    });


    /*it("calculate reversable diff harder object", async () => {
        const _old = {l: {a: {a: 1, b: 2}, b: {a: 1, b: 1}},            a: 1, f: 1, z: "a", p: [12, 2, 3, 3, {zp1: 32323}]};
        const _new = {l: {a: {a: 1, b: 2}, b: {a: 1, b: 2}},            b: 2, c: {d: 1}, f: 2, z: "a", m: [1, 2, {a: 1}], o: {m: {z: [1, 2, 3, {1: 1}]}}};
        const diffMapper = new DeepDiffMapper();
        const diff =  diffMapper.map(_new, _old);


        console.log("apply difference");

        console.log("----------------------------");

        console.log("new", _new);
        console.log("old", _old);

        console.log("diff", JSON.stringify(diff, null, 4));

        console.log("----------------------------");

        console.log("diffMapper.apply(_new, diff)", diffMapper.apply(_new, diff));
        console.log("old", _old);
    });*/

    // DeepDiff

    it("calculate reversable diff harder object", async () => {
        const _old = {l: {a: {a: 1, b: 2}, b: {a: 1, b: 1}},            a: 1, f: 1, z: "a", p: [12, 2, 3, 3, {zp1: 32323}]};
        const _new = {l: {a: {a: 1, b: 2}, b: {a: 1, b: 2}},            b: 2, c: {d: 1}, f: 2, z: "a", m: [1, 2, {a: 1}], o: {m: {z: [1, 2, 3, {1: 1}]}}};
        const diffMapper = new DeepDiff();
        const diff =  diffMapper.diff(_new, _old);


        console.log("apply difference");

        console.log("----------------------------");

        console.log("new", _new);
        console.log("old", _old);

        console.log("diff", JSON.stringify(diff, null, 4));

        console.log("----------------------------");

        console.log("new", _new);
        console.log("diffMapper.apply(_new, diff)", diffMapper.apply(_new, diff));
        console.log("old", _old);
    });
});