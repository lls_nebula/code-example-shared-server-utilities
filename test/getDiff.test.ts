import { it, describe } from "mocha";
import { getDiff } from "../src/util/deepDiff";
import chai = require("chai");
const expect = chai.expect;
import chaiAsPromised = require("chai-as-promised");

describe("get diff logic", () => {
    it("check the difference", async () => {
        console.log(getDiff({
            same: 1,
            different: 2,
            missing_from_b: 3,
            missing_nested_from_b: {
                x: 1,
                y: 2
            },
            nested: {
                same: 1,
                different: 2,
                different2: 23,
                missed_from_b_2: 222,
                missing_from_b: 3
            }
        }, {
            same: 1,
            different: 99,
            missing_from_a: 3,
            missing_nested_from_a: {
                x: 1,
                y: 2
            },
            nested: {
                same: 1,
                different: 99,
                different2: 32,
                missing_from_a: 3
            }
        }));
    });
});