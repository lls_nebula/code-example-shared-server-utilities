import { expect } from "chai";
import { it, describe } from "mocha";
import { gcdsInstance } from "../src/gcds-instance";
import { firestore } from "@vaultspace/firebase-server";

describe("Buckets return paths looking like what? trailing slash", () => {
    it("Should replace email bodies correctly", async () => {
        const Applications = await gcdsInstance.queryMany("Application", []);
        const pe = [];
        const applicationsRef = firestore.collection("Application");
        const applications = await applicationsRef.get();
        applications.forEach(async (app) => {
            console.log("Application", JSON.stringify(app.data(), null, 4));
            const productsRef = applicationsRef.doc(app.id).collection("Products");
            const products = await productsRef.get();
            products.forEach((prod) => {
                console.log("Products", app.id, prod.id, JSON.stringify(prod.data(), null, 4));
            });
            const reportsRef = applicationsRef.doc(app.id).collection("Reports");
            const reports = await reportsRef.get();
            reports.forEach((prod) => {
                console.log("Reports", app.id, prod.id, JSON.stringify(prod.data(), null, 4));
            });
            const applicationPriceEngineModelRef = applicationsRef.doc(app.id).collection("ApplicationPriceEngineModel");
            const applicationPriceEngineModel = await applicationPriceEngineModelRef.get();
            applicationPriceEngineModel.forEach((prod) => {
                console.log("ApplicationPriceEngineModel", app.id, prod.id, JSON.stringify(prod.data(), null, 4));
            });

        });
    });
});
