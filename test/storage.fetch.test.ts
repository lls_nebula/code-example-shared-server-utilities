import { expect } from "chai";
import { it, describe } from "mocha";
import { fetchFilesFromDirectory } from "../src/providers/storage";
import {
    folderName,
    staticFolderName,
    personalFolderName,
        } from "../src/configs/pdf_storager_config";
import { fetchAttachmentsForPolicy } from "../src/providers/email/attachmentsGenerator";
import { IQuotePolicy } from "@vaultspace/neon-shared-models";
import { gcdsInstance } from "@vaultspace/emailer/dist/src/util/gcdatastore-service";
//             fetchFilesFromDirectory(`${folderName}/${quote.systemApplication}/${personalFolderName}/customers/${quote.customerId}/quotes/${quoteId}`),

describe("storage", () => {
    // "https://firebasestorage.googleapis.com/v0/b/a365-neon-development.appspot.com/o/pdfDocumentation%2FimaliaDaycare%2Fpersonal%2Fcustomers%2F1cf12e63-9e94-494f-b6a5-d48699c65d8c%2Fquotes%2F7062a39a-3700-49c2-ae4e-942f3419e408%2FImalia%20-%20FDC%20-%20Schedule%20-%20v1.0.0.pdf?alt=media&token=f180578e-d454-43c4-82f0-19fc7ae23b17"
    it("Should fetch with correct names", async() => {
        const a = await fetchFilesFromDirectory(`${folderName}/imaliaDaycare/${personalFolderName}/customers/1cf12e63-9e94-494f-b6a5-d48699c65d8c/quotes/7062a39a-3700-49c2-ae4e-942f3419e408`);
        console.log("a", a);

        const policy: IQuotePolicy = await gcdsInstance.getOne({kind: "QuotePolicy", name: "7062a39a-3700-49c2-ae4e-942f3419e408"});

        const b = await fetchAttachmentsForPolicy(policy);

        console.log("b", b);
    });
});