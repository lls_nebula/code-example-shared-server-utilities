import { createFileWithPath, fetchFileFromPath, createJsonFileWithPath, fetchJsonFileFromPath } from "../src/providers/storage";
import { expect } from "chai";
import { it, describe } from "mocha";


describe("Buckets", () => {
    it("Should create file and save file", async () => {
        const startTime = Date.now();
        const testContent = "This is a test";
        console.log("saving test file");
        const savedFile = await createJsonFileWithPath("/tests/testFile.json", testContent);
        console.log("retrieving a test file");
        const retrievedFileData = await fetchJsonFileFromPath("/tests/testFile.json");
        const delta = Date.now() - startTime;
        console.log("how long:", delta);
        expect(retrievedFileData).to.equal(testContent);
    });
});