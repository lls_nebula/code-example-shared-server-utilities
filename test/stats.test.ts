import { collectStatsForKind, fetchHistograms, saveKindQueryCounts, fetchKindQueryCountForApplications, saveHistograms, fetchHistogram } from "../src/providers/stats";
import { collectAndSaveAllStatsAuto } from "../src/providers/statsJob";
import { expect } from "chai";
import { it, describe } from "mocha";
import { EBinType, IEntityStats, IDateBin } from "../src/models/index";
import { fetchQueries } from "../src/configs/stats";


describe("Stats", () => {


    it("Should run all jobs", async() => {
        await collectAndSaveAllStatsAuto();
    });



    it("Should calculate stats", async () => {
        const queries = fetchQueries();
        const kind: string = "QuotePolicy";
        const stats = await collectStatsForKind(kind, "quoteCreatedDate", [EBinType.Day, EBinType.Month, EBinType.Week], ["quoteCreatedDate", "endDate"], queries);
        // console.log(stats);
        const saveEntityCounts = await saveKindQueryCounts(stats);
        await Promise.all(Object.keys(queries).map( async (queryName: string) => {
            const startTime = Date.now();
            const kindQueryCount = await fetchKindQueryCountForApplications(kind, queryName, ["imaliaDaycare"]);
            console.log(kind, queryName, kindQueryCount, (Date.now() - startTime) / 1000);
        }));

        const saveHistogramsResult = await saveHistograms(stats);

        let hStart = Date.now();
        const quoteCreatedDate = await fetchHistogram(kind, "quoteCreatedDate", EBinType.Month, ["imaliaDaycare"]);
        console.log("quoteCreatedDate histogram", quoteCreatedDate, (Date.now() - hStart) / 1000);

        hStart = Date.now();
        const endDate = await fetchHistogram(kind, "endDate", EBinType.Month, ["imaliaDaycare"]);
        console.log("endDate histogram", endDate, (Date.now() - hStart) / 1000);
        // console.log("stats", stats);
        /*expect(stats.count != 0).to.equal(true);
        // sanity check for histograms
        Object.keys(stats.histograms).forEach((binType: EBinType) => {
            Object.keys(stats.histograms[binType]).forEach((dateFieldName: string) => {
                const localCount = stats.histograms[binType][dateFieldName].reduce((acc, bin: IDateBin) => {
                    acc += bin.count;
                    return acc;
                }, 0);
                console.log(localCount, stats.count, localCount == stats.count);
                expect(localCount == stats.count).to.equal(true);
            });
        });
        console.log(JSON.stringify(stats, null, 4));
        */
    });

    it("Should give better histograms", async() => {
        const a = await fetchHistograms(["imaliaDaycare"]);
        console.log("a", JSON.stringify(a));
    });
/*
    it("Should calculate stats", async () => {
        const stats = await collectStatsForKind("QuotePolicy", "quoteCreatedDate", [EBinType.Day, EBinType.Month, EBinType.Week], ["quoteCreatedDate"], queries);
        // console.log(stats);
        console.log(JSON.stringify(await processEntityCountStats(["imaliaDaycare"], stats.count), null, 4));
        // console.log("stats", stats);
        /*expect(stats.count != 0).to.equal(true);
        // sanity check for histograms
        Object.keys(stats.histograms).forEach((binType: EBinType) => {
            Object.keys(stats.histograms[binType]).forEach((dateFieldName: string) => {
                const localCount = stats.histograms[binType][dateFieldName].reduce((acc, bin: IDateBin) => {
                    acc += bin.count;
                    return acc;
                }, 0);
                console.log(localCount, stats.count, localCount == stats.count);
                expect(localCount == stats.count).to.equal(true);
            });
        });
        console.log(JSON.stringify(stats, null, 4));

    });*/
/*
    it("Should save stats", async () => {
        const stats = await collectStatsForKind("QuotePolicy", "quoteCreatedDate", [EBinType.Day, EBinType.Month, EBinType.Week], ["quoteCreatedDate"]);
        await saveStats(stats);
    });


*/
    // saveStats
});